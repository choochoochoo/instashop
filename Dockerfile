FROM node:8.12.0-alpine
WORKDIR /usr/src/app
COPY package.json package-lock.json ./
RUN npm install
COPY . ./
COPY /server/config.production.json ./server/config.json
RUN npm run build-prod
WORKDIR /usr/src/app/server

RUN npm install
CMD node index.js
EXPOSE 9097
