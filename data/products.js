export const products = [
    {
        id: 1,
        title: 'Торт 1',
        icon: 'http://localhost:8080/cake1.jpg',
        price: 1000,
    },
    {
        id: 2,
        title: 'Торт 2',
        icon: 'http://localhost:8080/cake2.jpg',
        price: 1500,
    },
    {
        id: 3,
        title: 'Торт 3',
        icon: 'http://localhost:8080/cake3.jpg',
        price: 1100,
    },
]

