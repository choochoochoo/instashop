var mongoose = require('mongoose')
var User = require('../db/models/User.js')
var utils = require('../utils.js')

exports.createCategory = function (category) {
    return new Promise((resolve, reject) => {

        User.findById(category.userId, function (err, user) {
            if (err) {
                console.log(err)
                return reject(err);
            }

            category._id = mongoose.Types.ObjectId()

            user.categories.push({
                _id: category._id,
                title: category.title,
                products: [],
            })

            user.save(function (err, updatedUser) {
                if (err) {
                    console.log(err)
                    return reject(err);
                }

                resolve(category);
            });
        });
    })
}

exports.editCategory = function (categoryId, category) {
    return new Promise((resolve, reject) => {

        User.findById(category.userId, function (err, user) {
            if (err) {
                console.log(err)
                return reject(err);
            }

            const editedCategory = user.categories.find(item => item._id.toString() === categoryId);
            editedCategory.title = category.title

            const oldPosition = utils.getOldPosition(user.categories, categoryId)

            if(oldPosition !== category.position){
                utils.arrayMove(user.categories, oldPosition - 1, category.position - 1)
            }

            // отредактируем категории в продуктах
            user.categories.forEach(categoryItem => {
                categoryItem.products.forEach(product => {
                    let foundCategory = product.categories.find(item => item._id.toString() === categoryId)
                    if(foundCategory) {
                        foundCategory.title = category.title
                    }
                })
            })

            user.products.forEach(product => {
                let foundCategory = product.categories.find(item => item._id.toString() === categoryId)
                if(foundCategory) {
                    foundCategory.title = category.title
                }
            })

            user.save(function (err, updatedUser) {
                if (err) {
                    console.log(err)
                    return reject(err);
                }

                resolve({category: editedCategory, newPosition: category.position });
            });
        });
    })
}

exports.removeCategory = function (categoryId, userId) {
    return new Promise((resolve, reject) => {

        User.findById(userId, function (err, user) {
            if (err) {
                console.log(err)
                return reject(err);
            }


            const userCategories = user.categories.filter(item => item._id.toString() !== categoryId);
            user.categories = userCategories

            // почистим категории в продуктах
            user.categories.forEach(category => {
                category.products.forEach(product => {
                    product.categories = product.categories.filter(item => item._doc._id.toString() !== categoryId)
                })
            })

            user.products.forEach(product => {
                product.categories = product.categories.filter(item => item._doc._id.toString() !== categoryId)
            })

            user.save(function (err, updatedUser) {
                if (err) {
                    console.log(err)
                    return reject(err);
                }

                resolve({
                    _id: categoryId,
                    userId,
                });
            });


        });
    })
}