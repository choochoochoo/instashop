const userApi = require('./user.js')
const productApi = require('./product.js')
const sectionApi = require('./section.js')
const categoryApi = require('./category.js')
const orderApi = require('./order.js')

var config = require('../config.json');
var mongoose = require('mongoose')

const options = {
    autoIndex: false, // Don't build indexes
    reconnectTries: 30, // Retry up to 30 times
    reconnectInterval: 500, // Reconnect every 500ms
    poolSize: 10, // Maintain up to 10 socket connections
    // If not connected, return errors immediately rather than waiting for reconnect
    bufferMaxEntries: 0
}

const connectWithRetry = () => {
    console.log('MongoDB connection with retry')
    mongoose.connect(config.dbConnectionString, options).then(()=>{
        console.log('MongoDB is connected')
    }).catch(err=>{
        console.log('MongoDB connection unsuccessful, retry after 5 seconds.')
        setTimeout(connectWithRetry, 5000)
    })
}

connectWithRetry()

exports.editUser = userApi.editUser
exports.createUser = userApi.createUser
exports.checkUser = userApi.checkUser
exports.getUserById = userApi.getUserById
exports.getUser = userApi.getUser
exports.getUsers = userApi.getUsers
exports.setAccountName = userApi.setAccountName
exports.getUserByAccountOrId = userApi.getUserByAccountOrId
exports.getUserByInstagrammId = userApi.getUserByInstagrammId
exports.getUserByAccountName = userApi.getUserByAccountName

exports.createProduct = productApi.createProduct
exports.editProduct = productApi.editProduct
exports.removeProduct = productApi.removeProduct

exports.createSection = sectionApi.createSection
exports.editSection = sectionApi.editSection
exports.getSection = sectionApi.getSection
exports.removeSection = sectionApi.removeSection

exports.createCategory = categoryApi.createCategory
exports.editCategory = categoryApi.editCategory
exports.removeCategory = categoryApi.removeCategory

exports.createOrder = orderApi.createOrder
exports.editOrder = orderApi.editOrder
exports.getOrders = orderApi.getOrders
exports.getOrder = orderApi.getOrder
