var User = require('../db/models/User.js')
var Order = require('../db/models/Order.js')
var mongoose = require('mongoose')
const _ = require('lodash');
var utils = require('../utils.js');



exports.createOrder = function (orderData) {
    return new Promise((resolve, reject) => {
        User.findById(orderData.userId, function (err, user) {
            if (err) {
                console.log(err)
                return reject(err);
            }

            Order.count({userId: orderData.userId}, function (err, count) {
               if (err) {
                   console.log(err)
                   return reject(err);
               }

                var copyOrder = _.cloneDeep(orderData)
                copyOrder.number = count === 0 ? 1 : ( count + 1 )
                copyOrder._id = mongoose.Types.ObjectId()
                copyOrder.status = 'new'

                return new Order(copyOrder).save(function (err, newOrder) {
                    if (err) {
                        console.log(err)
                        return reject(err);
                    }

                    Order.count({userId: orderData.userId, status: 'new'}, function (err, newOrdersCount) {
                        if (err) {
                            console.log(err)
                            return reject(err);
                        }

                        user.newOrders = newOrdersCount

                        user.save(function (err, updatedUser) {
                            if (err) {
                                console.log(err)
                                return reject(err);
                            }

                            let productsStiring = ''

                            orderData.products.forEach((product, index) => {
                                productsStiring += `${index + 1}. ${product.title} - ${product.count} шт. (цена - ${product.price})<br/>`
                            })

                            const text =
                                `Phone: ${orderData.phone}<br/>
                                 Comment: ${orderData.comment}<br/>
                                 Items<br/>
                                  ${productsStiring}
                                 <br/>
                                 <a href="https://flicky.io/${user.accountName}/orders/${newOrder._id}">Link to orders details</a>`


                            const emailForNotification = user.orderEmail ? user.orderEmail : user.email

                            utils.sendEmail(emailForNotification, `New order №${newOrder.number}`, text)

                            resolve(newOrder);
                        });


                    });



                });

           })


        })
    })
}

exports.editOrder = function (orderId, orderData) {
    return new Promise((resolve, reject) => {
        Order.findById(orderId, function (err, order) {
            if (err) {
                console.log(err)
                return reject(err);
            }

            order.status = orderData.status

            order.save(function (err, updatedOrder) {
                if (err) {
                    console.log(err)
                    return reject(err);
                }

                // обновим здесь счетчик новых заказов
                User.findById(order.userId, function (err, user) {
                    if (err) {
                        console.log(err)
                        return reject(err);
                    }


                    Order.count({userId: order.userId, status: 'new'}, function (err, newOrdersCount) {
                        if (err) {
                            console.log(err)
                            return reject(err);
                        }

                        user.newOrders = newOrdersCount

                        user.save(function (err, updatedUser) {
                            if (err) {
                                console.log(err)
                                return reject(err);
                            }

                            resolve({updatedOrder, newOrdersCount});
                        });


                    });



                })



            });

        })
    })
}

exports.getOrders = function (userId, skip, limit) {
    return new Promise((resolve, reject) => {
            Order.count({userId: userId}, function (err, count) {
                if (err) {
                    console.log(err)
                    return reject(err);
                }

                return Order
                    .find({userId}).sort({ 'createDate': -1}).skip(skip).limit(limit)
                    .then(function (doc) {
                        resolve({
                            skip,
                            limit,
                            count,
                            items: doc,
                        })
                    })
            })
    })
}

exports.getOrder = function (orderId) {
    return Order
        .findOne({_id: orderId})
        .then(function (doc) {
            return Promise.resolve(doc)
        })
}
