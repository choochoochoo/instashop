var mongoose = require('mongoose')
var User = require('../db/models/User.js')
var utils = require('../utils.js')

exports.createProduct = function (productData, photo) {
    return new Promise((resolve, reject) => {

        User.findById(productData.userId, function (err, user) {
            if (err) {
                console.log(err)
                return reject(err);
            }

            var product = {
                _id: mongoose.Types.ObjectId(),
                photo,
                title: productData.title,
                description: productData.description,
                price: productData.price,
                position: productData.position,
            };


            const categories = JSON.parse(productData.categories)
            if(categories.length > 0) {
                product.categories = categories
                categories.forEach(item => {
                    const foundCategory = user.categories.find(category => category._id.toString() === item._id)
                    foundCategory.products.splice(0, 0, product)
                })
            }

            user.products.splice(0, 0, product)


            let positions = []
            if(productData.positions){
                positions = JSON.parse(productData.positions)
                if(positions.length > 0){

                    positions.forEach(positionItem => {
                        if(positionItem._id === 'all'){
                            utils.arrayMove(user.products, 0, positionItem.position - 1)
                        }else{
                            const foundCategory = user.categories.find(category => category._id.toString() === positionItem._id)
                            utils.arrayMove(foundCategory.products, 0, positionItem.position - 1)
                        }
                    })

                }
            }


            user.save(function (err, updatedUser) {
                if (err) {
                    console.log(err)
                    return reject(err);
                }

                resolve({newProduct: product, newPositions: positions});
            });
        });
    })
}

exports.editProduct = function (productId, productData, photo) {
    return new Promise((resolve, reject) => {

        User.findById(productData.userId, function (err, user) {
            if (err) {
                console.log(err)
                return reject(err);
            }

            const editProduct = user.products.find(item => item._doc._id.toString() === productId);

            if(photo){
                editProduct.photo = photo
            }

            editProduct.title = productData.title;
            editProduct.description = productData.description;
            editProduct.price = productData.price;

            utils.editCategoryDb(JSON.parse(productData.categories), editProduct, user, productId)

            user.categories.forEach(category => {
                const foundProduct = category.products.find(item => item._doc._id.toString() === productId);
                if(foundProduct){
                    foundProduct.photo = editProduct.photo
                    foundProduct.title = editProduct.title
                    foundProduct.description = editProduct.description
                    foundProduct.price = editProduct.price
                    foundProduct.categories = editProduct.categories
                }
            })

            let positions = []
            if(productData.positions){
                positions = JSON.parse(productData.positions)
                if(positions.length > 0){
                    utils.changePositionsDb(positions, user, editProduct, productId)
                }
            }

            user.save(function (err, updatedUser) {
                if (err) {
                    console.log(err)
                    return reject(err);
                }

                const editProduct = user.products.find(item => item._doc._id.toString() === productId);
                resolve({editedProduct: editProduct, newPositions: positions});
            });
        })
    });
}

exports.removeProduct = function (userId, productId) {
    return new Promise((resolve, reject) => {

        User.findById(userId, function (err, user) {
            if (err) {
                console.log(err)
                return reject(err);
            }

            const removedProduct = user.products.find(item => item._doc._id.toString() === productId)
            user.products = user.products.filter(item => item._doc._id.toString() !== productId)

            user.categories.forEach(category => {
                category.products = category.products.filter(item => item._doc._id.toString() !== productId)
            })

            user.save(function (err, updatedUser) {
                if (err) {
                    console.log(err)
                    return reject(err);
                }

                resolve({
                    productId,
                    userId: userId,
                    photoOriginal: removedProduct.photo.original,
                    photoSize1080: removedProduct.photo.size1080,
                });
            });
        })
    });
}
