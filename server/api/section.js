var mongoose = require('mongoose')
var User = require('../db/models/User.js')
var Section = require('../db/models/Section.js')
var utils = require('../utils.js')

exports.createSection = function (section) {
    return new Promise((resolve, reject) => {

        User.findById(section.userId, function (err, user) {
            if (err) {
                console.log(err)
                return reject(err);
            }

            section._id = mongoose.Types.ObjectId(),

                Section(section).save(function (err, newSection) {
                    if (err) {
                        console.log(err)
                        return reject(err);
                    }

                    if(newSection.type === 'contacts'){
                        user.contactsSection = newSection._id
                    }

                    user.sections.push({
                        _id: section._id,
                        title: newSection.title,
                        type: newSection.type,
                    })

                    user.save(function (err, updatedUser) {
                        if (err) {
                            console.log(err)
                            return reject(err);
                        }

                        resolve(newSection);
                    });
                });
        });
    })
}

exports.editSection = function (sectionId, section) {
    return new Promise((resolve, reject) => {

        User.findById(section.userId, function (err, user) {
            if (err) {
                console.log(err)
                return reject(err);
            }

            const editUserSection = user.sections.find(item => item._id.toString() === sectionId);
            editUserSection.title = section.title
            editUserSection.type = section.type

            user.save(function (err, updatedUser) {
                if (err) {
                    console.log(err)
                    return reject(err);
                }

                Section.findById(sectionId, function (err, editSection) {
                    if (err) {
                        console.log(err)
                        return reject(err);
                    }

                    editSection.title = section.title
                    editSection.elements = section.elements

                    editSection.save(function (err, updatedSection) {
                        if (err) {
                            console.log(err)
                            return reject(err);
                        }

                        resolve(updatedSection);
                    })
                })

            });


        });
    })
}

exports.getSection = function (id) {
    return Section
        .findOne({_id: id})
        .then(function (doc) {
            return Promise.resolve(doc)
        })
}

exports.removeSection = function (sectionId, userId) {
    return new Promise((resolve, reject) => {

        User.findById(userId, function (err, user) {
            if (err) {
                console.log(err)
                return reject(err);
            }


            const removeSection = user.sections.find(item => item._id.toString() === sectionId);

            const userSections = user.sections.filter(item => item._id.toString() !== sectionId);
            user.sections = userSections


            if(removeSection.type === 'contacts') {
                user.contactsSection = null
            }

            user.save(function (err, updatedUser) {
                if (err) {
                    console.log(err)
                    return reject(err);
                }

                Section.deleteOne({_id: sectionId}, function (err) {
                    if (err) {
                        console.log(err)
                        return reject(err);
                    }

                    resolve({
                        _id: sectionId,
                        type: removeSection.type,
                    });
                })

            });


        });
    })
}
