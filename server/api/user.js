var User = require('../db/models/User.js')
var mongoose = require('mongoose')
var utils = require('../utils.js')
var config = require('../config.json');


exports.editUser = function (userId, userData, photo) {
    return new Promise((resolve, reject) => {
        User.findById(userId, function (err, user) {
            if (err) {
                console.log(err)
                return reject(err);
            }

            user.email = userData.email;
            user.companyPhone = userData.companyPhone;
            if(photo){
                user.companyLogo = photo;
            }
            user.companyName = userData.companyName;
            user.companyDescription = userData.companyDescription;
            user.theme = userData.theme;
            user.language = userData.language;
            user.hasSeenSectionDemoPage = userData.hasSeenSectionDemoPage;
            user.hasSeenContactsDemoPage = userData.hasSeenContactsDemoPage;
            user.phonecallPhone = userData.phonecallPhone;
            user.phonecallSwitchOn = userData.phonecallSwitchOn;

            user.whatsappcallPhone = userData.whatsappcallPhone;
            user.whatsappcallText = userData.whatsappcallText;
            user.whatsappcallSwitchOn = userData.whatsappcallSwitchOn;

            user.hideAllCategorySwitchOn = userData.hideAllCategorySwitchOn;
            user.allCategoryStart = userData.allCategoryStart;
            user.allCategoryEnd = userData.allCategoryEnd;
            user.settingsOrdersSwitchOn = userData.settingsOrdersSwitchOn;
            user.orderFeedback = userData.orderFeedback;
            user.orderEmail = userData.orderEmail;
            user.categoriesProductsSwitchOn = userData.categoriesProductsSwitchOn;

            user.save(function (err, updatedUser) {
                if (err) {
                    console.log(err)
                    return reject(err);
                }

                resolve(updatedUser);
            });
        });
    })
}

exports.createUser = function (userData) {
    return new Promise((resolve, reject) => {


        return new User(userData).save(function (error, newUser) {
            if (error) {
                console.log(error)
                return reject(error);
            }

             utils.sendEmail(config.support_email,
                'Новая регистрация flicky.io',
                `id: ${newUser._id}, email: ${newUser.email}, <a href="https://flicky.io/${newUser._id}">Ссылка на аккаунт</a>`)


            resolve(newUser);
        })
    })
}

exports.checkUser = function (userData) {
    return User
        .findOne({email: userData.login})
        .then(function (doc) {

            if(!doc){
                return Promise.resolve({
                    code: 'login_not_found'
                })
            }

            if (doc.password == utils.hash(userData.password)) {
                return Promise.resolve({
                    user: doc,
                    code: 'ok'
                })
            } else {
                return Promise.resolve({
                    code: 'incorrect_password'
                })
            }
        })
}

exports.setAccountName = function (userId, accountName) {
    return new Promise((resolve, reject) => {
        User.findById(userId, function (err, user) {
            if (err) {
                console.log(err)
                return reject(err);
            }

            user.accountName = accountName;

            user.save(function (err, updatedUser) {
                if (err) {
                    console.log(err)
                    return reject(err);
                }

                resolve(updatedUser);
            });
        });
    })
}

const getUserById = function (id) {
    return User
        .findOne({_id: id})
        .then(function (doc) {
            return Promise.resolve(doc)
        })
}

exports.getUserById = getUserById

const getUser = function(id) {
    return User
        .findOne({accountName: id})
        .then(function (doc) {
            return Promise.resolve(doc)
        })
}

exports.getUser = getUser

exports.getUserByAccountOrId = function (id) {
    return new Promise((resolve, reject) => {
        getUser(id)
            .then(function (users) {
                if (users) {
                    return resolve(users)
                } else {
                    getUserById(id).then(function (user) {
                        return resolve(user)
                    })
                }
            })
            .catch(function (error) {
                return reject(error)
            })
    })
}

exports.getUsers = function (id) {
    return User
        .find()
        .then(function (doc) {
            return Promise.resolve(doc)
        })
}

exports.getUserByInstagrammId = function (id) {
    return User
        .findOne({instagrammId: id})
        .then(function (doc) {
            return Promise.resolve(doc)
        })
}


exports.getUserByAccountName = function (accountName) {
    return User
        .findOne({accountName: accountName})
        .then(function (doc) {
            return Promise.resolve(doc)
        })
}
