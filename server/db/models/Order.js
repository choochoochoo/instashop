var mongoose = require('mongoose')
var Schema = mongoose.Schema;


var Order = new mongoose.Schema({
    _id: {
        type: Schema.Types.ObjectId,
        required: true,
    },
    phone: {
        type: String,
    },
    comment: {
        type: String,
    },
    userId: {
        type: String,
    },
    createDate: {
        type: Date,
        default: Date.now
    },
    number: {
        type: String,
        required: true,
    },
    status: {
        type: String,
    },
    products: [
        {
            _id: {
                type: Schema.Types.ObjectId,
                required: true,
            },
            title: {
                type: String,
            },
            price: {
                type: String,
            },
            photo: {
                type: String
            },
            count: {
                type: Number
            },
        }
    ]
})

module.exports = mongoose.model('Order', Order)
