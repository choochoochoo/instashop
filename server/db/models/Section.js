var mongoose = require('mongoose')
var Schema = mongoose.Schema;

var Section = new mongoose.Schema({
    _id: {
        type: Schema.Types.ObjectId,
        required: true,
    },
    type: {
        type: String,
        required: true,
    },
    userId: {
        type: Schema.Types.ObjectId,
        required: true,
    },
    title: {
        type: String,
    },
    elements: {
        type: Object,
    },
})

module.exports = mongoose.model('Section', Section)
