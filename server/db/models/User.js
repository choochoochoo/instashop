var mongoose = require('mongoose')
var Schema = mongoose.Schema;

var photo = {
    original: {
        type: String,
    },
    size1080: {
        type: String,
    },
    crop: {
        top: {
            type: Number,
        },
        left: {
            type: Number,
        },
        width: {
            type: Number,
        },
        height: {
            type: Number,
        },
    },
};

var product = {
    _id: {
        type: Schema.Types.ObjectId,
        required: true,
    },
    photo: photo,
    title: {
        type: String,
        required: true,
    },
    description: {
        type: String,
    },
    price: {
        type: String,
    },
    createDate: {
        type: Date,
        default: Date.now
    },
    categories: [
        {
            _id: {
                type: Schema.Types.ObjectId,
                required: true,
            },
            title: {
                type: String,

            }
        }
    ]
}

var category = {
    _id: {
        type: Schema.Types.ObjectId,
        required: true,
    },
    title: {
        type: String,
    },
    products: [
        product
    ],

}

var User = new mongoose.Schema({
    _id: {
        type: Schema.Types.ObjectId,
        required: true,
    },
    accountName: {
        type: String,
        unique: true,
        required: true
    },
    name: {
        type: String,
    },
    password: {
        type: String,
     //   required: true
    },
    email: {
        type: String,
        unique: true,
      //  required: true
    },
    language: {
        type: String,
        required: true,
        default: 'en',
    },
    companyPhone: {
        type: String,
    },
    companyName: {
        type: String,
    },
    companyDescription: {
        type: String,
    },
    companyLogo: photo,
    theme: {
        type: String,
        default: 'cloud',
    },
    createDate: {
        type: Date,
        default: Date.now
    },
    contactsSection: {
        type: Schema.Types.ObjectId,
    },
    products: [
        product
    ],
    sections: [
        {
            _id: {
                type: Schema.Types.ObjectId,
                required: true,
            },
            title: {
                type: String,
            },
            type: {
                type: String,
                required: true,
            }
        }
    ],
    categories: [
        category
    ],
    hasSeenSectionDemoPage: {
        type: Boolean,
        default: false
    },
    hasSeenContactsDemoPage: {
        type: Boolean,
        default: false,
    },
    phonecallPhone: {
        type: String,
    },
    phonecallSwitchOn: {
        type: Boolean,
        default: false,
    },
    whatsappcallPhone: {
        type: String,
    },
    whatsappcallText: {
        type: String,
    },
    whatsappcallSwitchOn: {
        type: Boolean,
        default: false,
    },
    hideAllCategorySwitchOn: {
        type: Boolean,
        default: false,
    },
    allCategoryStart: {
        type: Boolean,
        default: true,
    },
    allCategoryEnd: {
        type: Boolean,
        default: false,
    },
    settingsOrdersSwitchOn: {
        type: Boolean,
        default: false,
    },
    orderFeedback: {
        type: String
    },
    orderEmail: {
        type: String
    },
    newOrders: {
        type: Number
    },
    categoriesProductsSwitchOn: {
        type: Boolean,
        default: true,
    },
    instagrammId: {
        type: String
    },
})

module.exports = mongoose.model('User', User)
