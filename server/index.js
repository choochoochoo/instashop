'use strict';

// ================================================
// imports
const express = require('express');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const fileUpload = require('express-fileupload');
const compression = require('compression');
var utils = require('./utils.js');
var config = require('./config.json');

var mongoose = require("mongoose")
var session = require('express-session')
var MongoStore = require('connect-mongo')(session);


var http = require('http');
var https = require('https');
var fs = require('fs');

// start
var cors = require('cors');
var app = express();
app.use(cors());
app.use(fileUpload());
app.use(bodyParser.json());

//app.use(compression);
app.use('/static', express.static(__dirname + '/public'))
 app.use('/img', express.static(__dirname + '/public/img'))
 app.use('/favicons', express.static(__dirname + '/public/favicons'))

var api = require('./api')


var users  = require('./routes/users');
var products  = require('./routes/products');
var orders  = require('./routes/orders');
var files  = require('./routes/files');
var sections  = require('./routes/sections');
var categories  = require('./routes/categories');
var test  = require('./routes/test');

app.use(session({
    secret: 'i need more shops',
    resave: true,
    saveUninitialized: false,
    cookie: {
        maxAge: 3600 * 24 * 356 * 1000
    },
    store: new MongoStore({
        url: config.dbConnectionString,
    })
}))

app.use(cookieParser())

//app.use(compression())

app.use('/api/users', users);
app.use('/api/products', products);
app.use('/api/orders', orders);
app.use('/api/files', files);
app.use('/api/sections', sections);
app.use('/api/categories', categories);
app.use('/test', test);

app.set("view engine", "ejs");

// app.get('/', function(req, res) {
//
//     const index = fs.readFileSync(__dirname + '/public/index.html')
//     const html = fs.readFileSync(__dirname + '/public/index.html')
//     // res.sendFile(index)
//     res.render('test', { html: html })
//
// });

const renderStub = (response) => {
    response.end(utils.getHtml('/views/index.ejs', {
        page_title: 'flicky.io',
        profile_title: 'flicky.io',
        profile_description: '',
        profile_image: '',
        profile_url: '/',
        canonical: '/',
        image_src: '',
    }));
}

app.get("/:userId/products/:productId", function(request, response){
    const userId = request.params.userId
    const productId = request.params.productId

    if(userId && productId){
        api.getUserByAccountOrId(userId)
            .then(function (user) {

                const foundProduct = user.products.find(item => item._id.toString() === productId)
                if(foundProduct){
                    const logo = foundProduct.photo ? foundProduct.photo.size1080 : null
                    const logoImage = `https://flicky.io/api/files/${logo}`
                    const url = `https://flicky.io/${user.accountName}/products/${productId}`
                    const canonical = `https://flicky.io/${user.accountName}`

                    response.end(utils.getHtml('/views/index.ejs', {
                        page_title: foundProduct.title,
                        profile_title: foundProduct.title,
                        profile_description: foundProduct.description,
                        profile_image: logoImage,
                        profile_url: url,
                        canonical: canonical,
                        image_src: logoImage,
                    }));
                }else{
                    renderStub(response)
                }



            })
            .catch(function (error) {
                console.log(error)
                response.status(500).send(error)
            })
    }else{
        renderStub(response)
    }
});

app.get("/register", function(request, response){
    renderStub(response)
});

app.get("/login", function(request, response){
    renderStub(response)
});


app.get("/:userId", function(request, response){
    const userId = request.params.userId

    if(userId){
        api.getUserByAccountOrId(userId)
            .then(function (user) {
                const logo = user.companyLogo ? user.companyLogo.size1080 : null
                const logoImage = `https://flicky.io/api/files/${logo}`
                const url = `https://flicky.io/${user.accountName}`

                response.end(utils.getHtml('/views/index.ejs', {
                    page_title: `${user.companyName} - ${user.companyDescription}`,
                    profile_title: user.companyName,
                    profile_description: user.companyDescription,
                    profile_image: logoImage,
                    profile_url: url,
                    canonical: url,
                    image_src: logoImage,
                }));
            })
            .catch(function (error) {
                console.log(error)
                response.status(500).send(error)
            })
    }else{
        renderStub(response)
    }
});



app.get("*", function(request, response){
    renderStub(response)
});

var options = {
    key: fs.readFileSync('certificate/private.pem'),
    cert: fs.readFileSync('certificate/flicky.cert')
};

const serverPortHttp = 9096;
const serverPortHttps = 9097;

console.log(`http server started on ${serverPortHttp}`);
console.log(`https server started on ${serverPortHttps}`);

console.log(`debug mode ${process.env.DEBUG}`);

// app.listen(serverPortHttp);


process.on('uncaughtException', function(err) {
    console.log('Caught exception: ' + err);

    utils.sendEmail(config.support_email, 'Крит. ошибка на flicky.io', `${err.message} /n ${err.stack}`)
});

if(process.env.DEBUG === 'true'){
    app.listen(serverPortHttp);
}else{
    http.createServer(function (req, res) {
        res.writeHead(301, { "Location": "https://" + req.headers['host'] + req.url });
        res.end();
    }).listen(serverPortHttp);


    https.createServer(options, app, (req, res) => {
        res.writeHead(200);
        res.end('hello world\n');
    }).listen(serverPortHttps);
}


