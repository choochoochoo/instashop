var express = require('express');
var router = express.Router();
var api = require('../api')



// router.get('/:id', function (req, res, next) {
//     api.getCateogry(req.params.id)
//         .then(function (section) {
//             res.send(section)
//         })
//         .catch(function (error) {
//             return next(error)
//         })
// });

router.post('/', function (req, res, next) {

    api.createCategory(req.body)
        .then((result)=>{
            res.send(result)
        })
        .catch(function (error) {
            return next(error)
        })

});

router.put('/:id', function (req, res, next) {

    api.editCategory(req.params.id, req.body)
        .then((result)=>{
            res.send(result)
        })
        .catch(function (error) {
            return next(error)
        })
});

router.delete('/:categoryId/:userId', function (req, res, next) {
    api.removeCategory(req.params.categoryId, req.params.userId)
        .then(function (result) {

            res.send(result)
        })
        .catch(function (err) {
            res.status(500).send(err)
        })
});


module.exports = router;