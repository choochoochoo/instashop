var express = require('express');
var router = express.Router();
var api = require('../api')
var config = require('../config.json');
var fileSystem = require('fs');
var path = require('path');

router.get('/:id', function (req, res, next) {

    var filePath = `${config.fileStorePath}${req.params.id}`;
    var stat = fileSystem.statSync(filePath);

    res.writeHead(200, {
        'Content-Type': 'image/jpeg;image/png',
        'Content-Length': stat.size,
        'Cache-Control': 'public, max-age=31557600'
    });

    var readStream = fileSystem.createReadStream(filePath);
    // We replaced all the event handlers with a simple call to readStream.pipe()
    readStream.pipe(res);
});


module.exports = router;

