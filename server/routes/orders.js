var express = require('express');
var router = express.Router();
var api = require('../api')


router.put('/:id', function (req, res, next) {
    api.editOrder(req.params.id, req.body)
        .then(function (result) {
            res.send(result)
        })
        .catch(function (err) {
            res.status(500).send(err)
        })
});

router.post('/', function (req, res, next) {
    api.createOrder(req.body)
        .then(function (result) {
            res.send(result)
        })
        .catch(function (err) {
            res.status(500).send(err)
        })
});

router.get('/user/:id/:skip/:limit', function (req, res, next) {
    api.getOrders(req.params.id, parseInt(req.params.skip), parseInt(req.params.limit))
        .then(function (users) {
            if (users) {
                res.send(users)
            } else {
                return next(error)
            }
        })
        .catch(function (error) {
            return next(error)
        })
});

router.get('/:id', function (req, res, next) {
    api.getOrder(req.params.id)
        .then(function (users) {
            if (users) {
                res.send(users)
            } else {
                return next(error)
            }
        })
        .catch(function (error) {
            return next(error)
        })
});

module.exports = router;

