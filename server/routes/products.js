var express = require('express');
var router = express.Router();
var api = require('../api')
var config = require('../config.json');
var fs = require('fs')

var imageUtils = require('./utils.js');
var resizeImageAndSave = imageUtils.resizeImageAndSave
var getCrop = imageUtils.getCrop
var getPixelCrop = imageUtils.getPixelCrop
var getImagesNames = imageUtils.getImagesNames
var readImage = imageUtils.readImage


router.post('/', function (req, res, next) {
    if (req.files){
        let photo = req.files.photo;

        const images = getImagesNames(photo.mimetype, req.body)

        photo.mv(images.originalNewPath)
            .then(() => resizeImageAndSave(photo.data , images.size1080NewPath, getPixelCrop(req.body)))
            .then(() => api.createProduct(req.body, {
                original: images.originalNew,
                size1080: images.size1080New,
                crop: getCrop(req.body),
            }))
            .then(result => res.send(result))
            .catch(error => {
                console.log(error)
                res.status(500).send(error)
            })

    } else {
        api.createProduct(req.body, null)
            .then(result => res.send(result))
            .catch(function (err) {
                console.log(err)
                res.status(500).send(err)
            })
    }
});


router.put('/:id', function (req, res, next) {
    if (req.files){
        let photo = req.files.photo;
        const images = getImagesNames(photo.mimetype, req.body)

        if(images.originalOldPath) {
            try{
                fs.unlinkSync(images.originalOldPath)
            }catch (e) {
                console.log(e)
            }
        }

        if(images.size1080OldPath) {
            try{
                fs.unlinkSync(images.size1080OldPath)
            }catch (e) {
                console.log(e)
            }
        }

        photo.mv(images.originalNewPath)
            .then(() => resizeImageAndSave(photo.data , images.size1080NewPath, getPixelCrop(req.body)))
            .then(() => api.editProduct(req.params.id, req.body, {
                original: images.originalNew,
                size1080: images.size1080New,
                crop: getCrop(req.body),
            }))
            .then(result => res.send(result))
            .catch(error => {
                console.log(error)
                res.status(500).send(error)
            })

    } else {

        if(req.body.photoMetaPixelCropX !== 'undefined') {
            const images = getImagesNames(null, req.body)

            if(images.size1080OldPath) {
                try{
                    fs.unlinkSync(images.size1080OldPath)
                }catch (e) {
                    console.log(e)
                }
            }

            readImage(images.originalOldPath)
                .then((data) => resizeImageAndSave(data, images.size1080NewPath, getPixelCrop(req.body)))
                .then(() => api.editProduct(req.params.id, req.body, {
                    original: images.originalOld,
                    size1080: images.size1080New,
                    crop: getCrop(req.body),
                }))
                .then(result => res.send(result))
                .catch(function (err) {
                    console.log(err)
                    res.status(500).send(err)
                })

        }else{
            api.editProduct(req.params.id, req.body, null)
                .then(result => res.send(result))
                .catch(function (err) {
                    console.log(err)
                    res.status(500).send(err)
                })
        }


    }
});

router.delete('/:userId/:productId', function (req, res, next) {
    api.removeProduct(req.params.userId, req.params.productId)
        .then(function (result) {

            try{
                fs.unlinkSync(`${config.fileStorePath}${result.photoOriginal}`)
            }catch (e) {
                console.log(e)
            }

            try{
                fs.unlinkSync(`${config.fileStorePath}${result.photoSize1080}`)
            }catch (e) {
                console.log(e)
            }

            res.send(result)
        })
        .catch(function (err) {
            res.status(500).send(err)
        })
});

module.exports = router;
