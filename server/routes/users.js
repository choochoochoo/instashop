
var axios = require('axios');
var express = require('express');
var router = express.Router();
var api = require('../api')
var config = require('../config.json');
var uuidv1 = require('uuid/v1');
var path = require('path')
var fs = require('fs')
var utils = require('../utils.js');
var query = require('querystring');
var _ = require('lodash');
var mongoose = require('mongoose')


var configInst = require('../instagramm/config.json');


var imageUtils = require('./utils.js');
var resizeImageAndSave = imageUtils.resizeImageAndSave
var getCrop = imageUtils.getCrop
var getPixelCrop = imageUtils.getPixelCrop
var getImagesNames = imageUtils.getImagesNames
var readImage = imageUtils.readImage


router.put('/:id', function (req, res, next) {
    if (req.files){
        let photo = req.files.photo;
        const images = getImagesNames(photo.mimetype, req.body)

        if(images.originalOldPath) {
            try{
                fs.unlinkSync(images.originalOldPath)
            }catch (e) {
                console.log(e)
            }
        }

        if(images.size1080OldPath) {
            try{
                fs.unlinkSync(images.size1080OldPath)
            }catch (e) {
                console.log(e)
            }
        }

        photo.mv(images.originalNewPath)
            .then(() => resizeImageAndSave(photo.data, images.size1080NewPath, getPixelCrop(req.body)))
            .then(() => api.editUser(req.params.id, req.body, {
                original: images.originalNew,
                size1080: images.size1080New,
                crop: getCrop(req.body),
            }))
            .then(result => res.send(result))
            .catch(error => {
                console.log(error)
                res.status(500).send(error)
            })

    } else {

        if(req.body.photoMetaPixelCropX !== 'undefined') {
            const images = getImagesNames(null, req.body)

            if(images.size1080OldPath) {
                try{
                    fs.unlinkSync(images.size1080OldPath)
                }catch (e) {
                    console.log(e)
                }
            }

            readImage(images.originalOldPath)
                .then((data) => resizeImageAndSave(data, images.size1080NewPath, getPixelCrop(req.body)))
                .then(() => api.editUser(req.params.id, req.body, {
                    original: images.originalOld,
                    size1080: images.size1080New,
                    crop: getCrop(req.body),
                }))
                .then(result => res.send(result))
                .catch(function (err) {
                    console.log(err)
                    res.status(500).send(err)
                })

        }else{
            api.editUser(req.params.id, req.body, null)
            .then(result => res.send(result))
            .catch(function (err) {
                console.log(err)
                res.status(500).send(err)
            })
        }


    }
});

router.get('/', function (req, res, next) {
    api.getUsers()
        .then(function (users) {
            if (users) {
                res.send(users)
            } else {
                return next(error)
            }
        })
        .catch(function (error) {
            return next(error)
        })
});

router.post('/getAuth', function (req, res, next) {
    if(req.session.user) {
        api.getUserById(req.session.user.id)
            .then(function (users) {
                if (users) {
                    res.send(users)
                } else {
                    return next(error)
                }
            })
            .catch(function (error) {
                return next(error)
            })
    }else{
        res.send({})
    }
});


router.get('/:id', function (req, res, next) {
      api.getUser(req.params.id)
        .then(function (users) {
            if (users) {
                res.send(users)
            } else {
                api.getUserById(req.params.id).then(function (user) {
                    res.send(user)
                })
            }
        })
        .catch(function (error) {
            return next(error)
        })
});



router.post('/', function (req, res, next) {

    const password = uuidv1().substring(0, 6);

    const userId = mongoose.Types.ObjectId()

    // var product1 = {
    //     _id: mongoose.Types.ObjectId("5c0f8cd07ccb0e0eb727bdb9"),
    //     title: 'Пример товара #1',
    //     description: 'Описание товара',
    //     price: '1 200 ₽',
    // }
    //
    // var product2 = {
    //     _id: mongoose.Types.ObjectId("5c0f8cd07ccb0e0eb727bdba"),
    //     title: 'Пример товара #2',
    //     description: 'Описание товара',
    //     price: '700 ₽',
    // }
    //
    // var product3 = {
    //     _id: mongoose.Types.ObjectId("5c0f8cd07ccb0e0eb727bdbb"),
    //     title: 'Пример товара #3',
    //     description: 'Описание товара',
    //     price: '500 ₽',
    // }
    //
    // const products = []
    //
    // products.push(product1)
    // products.push(product2)
    // products.push(product3)

    api.createUser({
        _id: userId,
        accountName: userId,
        email: req.body.email,
        password: utils.hash(password),
        language: req.body.language,
        // products,
    })
        .then(function (result) {

            console.log(password)

            const text =
                `Login: ${req.body.email}<br/>
                 Password: ${password}<br/>
                 <br/>
                 <a href="https://flicky.io/login">Link to your account</a>
                 <br/>
                 <br/> In case you have any questions feel free to contact us via email:  
                 <a href="mailto:contact@flicky.io" target="_blank">contact@flicky.io</a>
                `


            utils.sendEmail(req.body.email, 'Registration on flicky.io', text).
                then(function () {

                    req.session.user = {id: result._id, name: result.email}

                    res.send({
                        user: result,
                        code: 'ok'
                    })
                }).catch(function (error) {
                    res.send({
                        user: result,
                        code: 'ok'
                    })
                })

        })
        .catch(function (err) {
            if (err.code == 11000) {
                res.send({
                    user: null,
                    code: 'login_exist'
                })
            }else{
                res.send({
                    user: null,
                    code: 'general'
                })
            }
        })
});



router.post('/login', function (req, res, next) {
    api.checkUser(req.body)
        .then(function (result) {
            if (result.user) {
                req.session.user = {id: result.user._id, name: result.user.email}
                res.send(result)
            } else {
                res.send(result)
            }
        })
        .catch(function (error) {
            return next(error)
        })
});

const saveUser = (req, res, instagrammUser, companyLogo) => {

    const userId = mongoose.Types.ObjectId()

    api.getUserByAccountName(instagrammUser.username).then(foundUser => {
        api.createUser({
            _id: userId,
            accountName: foundUser ? userId : instagrammUser.username,
            language: 'ru',
            instagrammId: instagrammUser.id,
            companyName: instagrammUser.full_name,
            companyDescription: instagrammUser.bio,
            companyLogo: companyLogo,
        }).then(function (newUser) {
            req.session.user = {id: newUser._id, name: newUser.instagrammId}
            res.redirect(`/${newUser.accountName}`);
        }).catch(error => {
            const message = error.message
            console.log(message)
            res.write(message);
            res.end()
        })
    }).catch(error => {
        const message = error.message
        console.log(message)
        res.write(message);
        res.end()
    })


}

router.get('/login/instagramm/', function (req, res, next) {

    //res.writeHead(200, { "Content-Type": "text/plain" });

    const code = req.query.code

    if(!code){
        res.write('no code');
        res.end()
    }else{

        var param = {
            client_id: configInst.CLIENT_ID,
            client_secret: configInst.CLIENT_SECRET,
            redirect_uri: configInst.REDIRECT_URI,
            grant_type: 'authorization_code',
            code: code,
        }

        const data = query.stringify(param);

        axios.post('https://api.instagram.com/oauth/access_token', data, {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Content-Length': data.length,
            }
        }).then(result => {

            const instagrammUser = result.data.user

            api.getUserByInstagrammId(instagrammUser.id).then(foundUser => {
                if(foundUser){
                    req.session.user = {id: foundUser._id, name: foundUser.instagrammId}
                    res.redirect(`/${foundUser.accountName}`);
                }else{

                    axios.get(instagrammUser.profile_picture, {
                        responseType: 'arraybuffer'
                    })
                    .then(result => {

                        const fileName = uuidv1();
                        const fileNameWithExt = `${fileName}.jpeg`
                        const imagePath = `${config.fileStorePath}${fileNameWithExt}`;

                        imageUtils.saveImage(result.data, imagePath).then(()=> {

                            saveUser(req, res, instagrammUser, {
                                original : fileNameWithExt,
                                size1080 : fileNameWithExt,
                                crop : {
                                    left : 0,
                                    top : 0,
                                    width : 150,
                                    height : 150
                                }
                            })

                        }).catch(error => {
                            console.log(error.message)
                            saveUser(req, res, instagrammUser, null)
                        })
                    }).catch(error => {
                        console.log(error.message)
                        saveUser(req, res, instagrammUser, null)
                    })




                }
            })



            //
            // res.write(JSON.stringify(result.data));
            // res.end()
        }).catch(error => {

            let message = _.get(error, 'response.data.error_message', null)

            if(message === null){
                message = _.get(error, 'response.data', null)
            }

            if(message === null){
                message = error.message
            }

            console.log(message)
            res.write(message);
            res.end()
        })
    }
});

router.post('/logout', function (req, res, next) {
    if (req.session.user) {
        delete req.session.user;
        res.redirect('/')
    }
});

router.put('/setAccountName/:id', function (req, res, next) {
    api.setAccountName(req.params.id, req.body.accountName)
        .then(function (result) {

            res.send({
                user: result,
                code: 'ok'
            })

        })
        .catch(function (err) {
            if (err.code === 11000) {
                res.send({
                    user: null,
                    code: 'account_name_exist'
                })
            }else{
                res.send({
                    user: null,
                    code: 'general'
                })
            }


        })
});

module.exports = router;

