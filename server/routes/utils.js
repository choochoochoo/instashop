var config = require('../config.json');
var uuidv1 = require('uuid/v1');
var fs = require('fs')
var mime = require('mime-types')
var path = require('path')
var Jimp = require('jimp');


exports.resizeImageAndSave = function resizeImageAndSave(buffer, name, crop) {
    return new Promise((resolve, reject) => {
        return Jimp.read(buffer)
            .then(newPhoto => {
                newPhoto
                    .crop(crop.left, crop.top, crop.width, crop.height)
                    .write(name);

                resolve()
            })
            .catch(err => {
                reject(error)
            });
    })
}

exports.saveImage = function resizeImageAndSave(buffer, name) {
    return new Promise((resolve, reject) => {
        fs.writeFile(name, buffer, { flag : 'w' }, (error) => {
                if(error) {
                    reject(error)
                }

                resolve()
        })
    })
}

exports.removeImage = function removeImage(file) {
    return new Promise((resolve, reject) => {
        fs.unlink(`${config.fileStorePath}${file}`, (error) => {
            if(error) {
                reject(error)
            }

            resolve()
        })
    })
}

exports.readImage = function readImage(file) {
    return new Promise((resolve, reject) => {
        fs.readFile(file, (error, data) => {
            if(error) {
                reject(error)
            }

            resolve(data)
        })
    })
}

exports.getCrop = function getCrop(body){

    function round(num) {
        return Number((num).toFixed(4))
    }

    return {
        left: round(parseFloat(body.photoMetaCropX)),
        top: round(parseFloat(body.photoMetaCropY)),
        width: round(parseFloat(body.photoMetaCropWidth)),
        height: round(parseFloat(body.photoMetaCropHeight)),
    };
}

exports.getPixelCrop = function getPixelCrop(body){
    return {
        left: parseFloat(body.photoMetaPixelCropX),
        top: parseFloat(body.photoMetaPixelCropY),
        width: parseFloat(body.photoMetaPixelCropWidth),
        height: parseFloat(body.photoMetaPixelCropHeight),
    };
}

exports.getImagesNames = function getImagesNames(mimetype, body){

    let originalOldPath = null;
    let originalOld = null;
    let size1080OldPath = null;
    let size1080Old = null;

    if(body.photoOriginal){
        originalOld = body.photoOriginal;
        size1080Old = body.photoSize1080;
        originalOldPath = `${config.fileStorePath}${originalOld}`;
        size1080OldPath = `${config.fileStorePath}${size1080Old}`;
    }

    const fileName = uuidv1();
    let fileExtension = null;

    if(mimetype){
        fileExtension = mime.extension(mimetype);
    }else{
        fileExtension = path.extname(originalOldPath).replace('.', '')
    }



    const originalNew = `${fileName}.${fileExtension}`;
    const originalNewPath = `${config.fileStorePath}${originalNew}`;

    const fileNameOfResizeImage = uuidv1();
    const size1080New = `${fileNameOfResizeImage}.${fileExtension}`;
    const size1080NewPath = `${config.fileStorePath}${size1080New}`;

    return {
        originalOldPath,
        originalOld,
        size1080OldPath,
        size1080Old,

        originalNewPath,
        originalNew,
        size1080NewPath,
        size1080New,
    }
}