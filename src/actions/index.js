import * as types from '../constants/ActionTypes.js';

export const userGetAuthStart = (user) => ({ type: types.USER_GET_AUTH_START, user });
export const userGetAuthSuccess = (user) => ({ type: types.USER_GET_AUTH_SUCCESS, user });
export const userGetAuthError = (error) => ({ type: types.USER_GET_AUTH_ERROR, error });

export const userLoginStart = (user) => ({ type: types.USER_LOGIN_START, user });
export const userLoginSuccess = (user) => ({ type: types.USER_LOGIN_SUCCESS, user });
export const userLoginNotSuccess = (code) => ({ type: types.USER_LOGIN_NOT_SUCCESS, code });
export const userLoginError = (error) => ({ type: types.USER_LOGIN_ERROR, error });

export const userLogoutStart = (user) => ({ type: types.USER_LOGOUT_START, user });
export const userLogoutSuccess = (user) => ({ type: types.USER_LOGOUT_SUCCESS, user });
export const userLogoutError = (error) => ({ type: types.USER_LOGOUT_ERROR, error });

export const userGetStart = () => ({ type: types.USER_GET_START });
export const userGetSuccess = (user) => ({ type: types.USER_GET_SUCCESS, user });
export const userGetError = (error) => ({ type: types.USER_GET_ERROR, error });

export const userCreateStart = (user) => ({ type: types.USER_CREATE_START, user });
export const userCreateSuccess = (user) => ({ type: types.USER_CREATE_SUCCESS, user });
export const userCreateNotSuccess = (code) => ({ type: types.USER_CREATE_NOT_SUCCESS, code });
export const userCreateError = (error) => ({ type: types.USER_CREATE_ERROR, error });

export const userChangeAccountNameStart = (user) => ({ type: types.USER_CHANGE_ACCOUNT_NAME_START, user });
export const userChangeAccountNameSuccess = (user) => ({ type: types.USER_CHANGE_ACCOUNT_NAME_SUCCESS, user });
export const userChangeAccountNameNotSuccess = (code) => ({ type: types.USER_CHANGE_ACCOUNT_NAME_NOT_SUCCESS, code });
export const userChangeAccountNameError = (code) => ({ type: types.USER_CHANGE_ACCOUNT_NAME_ERROR, code });

export const userEditStart = (user) => ({ type: types.USER_EDIT_START, user });
export const userEditSuccess = (user) => ({ type: types.USER_EDIT_SUCCESS, user });
export const userEditError = (error) => ({ type: types.USER_EDIT_ERROR, error });

export const productCreateStart = (product) => ({ type: types.PRODUCT_CREATE_START, product });
export const productCreateSuccess = (product) => ({ type: types.PRODUCT_CREATE_SUCCESS, product });
export const productCreateError = (error) => ({ type: types.PRODUCT_CREATE_ERROR, error });

export const productEditStart = (product) => ({ type: types.PRODUCT_EDIT_START, product });
export const productEditSuccess = (product) => ({ type: types.PRODUCT_EDIT_SUCCESS, product });
export const productEditError = (error) => ({ type: types.PRODUCT_EDIT_ERROR, error });

export const productRemoveStart = (product) => ({ type: types.PRODUCT_REMOVE_START, product });
export const productRemoveSuccess = (product) => ({ type: types.PRODUCT_REMOVE_SUCCESS, product });
export const productRemoveError = (error) => ({ type: types.PRODUCT_REMOVE_ERROR, error });

export const orderCreateStart = (order) => ({ type: types.ORDER_CREATE_START, order });
export const orderCreateSuccess = (order) => ({ type: types.ORDER_CREATE_SUCCESS, order });
export const orderCreateError = (error) => ({ type: types.ORDER_CREATE_ERROR, error });

export const orderEditStart = (order) => ({ type: types.ORDER_EDIT_START, order });
export const orderEditSuccess = (order) => ({ type: types.ORDER_EDIT_SUCCESS, order });
export const orderEditError = (error) => ({ type: types.ORDER_EDIT_ERROR, error });

export const ordersGetStart = (orders) => ({ type: types.ORDERS_GET_START, orders });
export const ordersGetSuccess = (orders) => ({ type: types.ORDERS_GET_SUCCESS, orders });
export const ordersGetError = (error) => ({ type: types.ORDERS_GET_ERROR, error });

export const orderGetStart = (order) => ({ type: types.ORDER_GET_START, order });
export const orderGetSuccess = (order) => ({ type: types.ORDER_GET_SUCCESS, order });
export const orderGetError = (error) => ({ type: types.ORDER_GET_ERROR, error });

export const showProgress = () => ({ type: types.SHOW_PROGRESS });
export const hideProgress = () => ({ type: types.HIDE_PROGRESS });

export const showCallbackDialog = () => ({ type: types.SHOW_CALLBACK_DIALOG });
export const hideCallbackDialog = () => ({ type: types.HIDE_CALLBACK_DIALOG });

export const showCreateProductDialog = (productId, photoId) => ({ type: types.SHOW_CREATE_PRODUCT_DIALOG, productId, photoId });
export const hideCreateProductDialog = () => ({ type: types.HIDE_CREATE_PRODUCT_DIALOG});

export const changeLanguage = (language) => ({ type: types.CHANGE_LANGUAGE, language});

export const openMenu = () => ({ type: types.OPEN_MENU});
export const closeMenu = () => ({ type: types.CLOSE_MENU});

export const viewModeEnabled = () => ({ type: types.VIEW_MODE_ENABLED});
export const viewModeDisabled = () => ({ type: types.VIEW_MODE_DISABLED});

export const changeTheme = (theme) => ({ type: types.CHANGE_THEME, theme});






export const setCrop = (crop) => ({ type: types.SET_CROP, crop});

export const setSrcQRCode = (qrcode) => ({ type: types.SET_SRC_QR_CODE, qrcode});

export const sectionCreateStart = (section) => ({ type: types.SECTION_CREATE_START, section });
export const sectionCreateSuccess = (section) => ({ type: types.SECTION_CREATE_SUCCESS, section });
export const sectionCreateError = (error) => ({ type: types.SECTION_CREATE_ERROR, error });

export const sectionEditStart = (section) => ({ type: types.SECTION_EDIT_START, section });
export const sectionEditSuccess = (section) => ({ type: types.SECTION_EDIT_SUCCESS, section });
export const sectionEditError = (error) => ({ type: types.SECTION_EDIT_ERROR, error });

export const sectionGetStart = (section) => ({ type: types.SECTION_GET_START, section });
export const sectionGetSuccess = (section) => ({ type: types.SECTION_GET_SUCCESS, section });
export const sectionGetError = (error) => ({ type: types.SECTION_GET_ERROR, error });

export const sectionRemoveStart = (section) => ({ type: types.SECTION_REMOVE_START, section });
export const sectionRemoveSuccess = (section) => ({ type: types.SECTION_REMOVE_SUCCESS, section });
export const sectionRemoveError = (error) => ({ type: types.SECTION_REMOVE_ERROR, error });

export const clearCreateSection = () => ({ type: types.CLEAR_CREATE_SECTION });

export const openTextEditor = (text, fieldName) => ({ type: types.OPEN_TEXT_EDITOR, text, fieldName });
export const closeTextEditor = () => ({ type: types.CLOSE_TEXT_EDITOR });
export const nextTextEditor = (text) => ({ type: types.NEXT_TEXT_EDITOR, text });

export const setTextTextEditor = (text) => ({ type: types.SET_TEXT_TEXT_EDITOR, text });

export const open = (text) => ({ type: types.SET_TEXT_TEXT_EDITOR, text });

export const openTextEditorLinkWindow = () => ({ type: types.OPEN_TEXT_EDITOR_LINK_WINDOW });
export const closeTextEditorLinkWindow = () => ({ type: types.CLOSE_TEXT_EDITOR_LINK_WINDOW });

export const setTextEditorLinkWindow = (text) => ({ type: types.SET_TEXT_EDITOR_LINK_WINDOW, text });

export const openAddMapPage = () => ({ type: types.OPEN_ADD_MAP_PAGE });
export const closeAddMapPage = () => ({ type: types.CLOSE_ADD_MAP_PAGE });

export const closeSection = () => ({ type: types.CLOSE_SECTION });

export const startLoadingMapScript = () => ({ type: types.START_LOADING_MAP_SCRIPT });
export const finishLoadingMapScript = () => ({ type: types.FINISH_LOADING_MAP_SCRIPT });

export const openRemoveProductDialog = () => ({ type: types.OPEN_REMOVE_PRODUCT_DIALOG });
export const closeRemoveProductDialog = () => ({ type: types.CLOSE_REMOVE_PRODUCT_DIALOG });

export const openRemoveSectionDialog = () => ({ type: types.OPEN_REMOVE_SECTION_DIALOG });
export const closeRemoveSectionDialog = () => ({ type: types.CLOSE_REMOVE_SECTION_DIALOG });

export const openRemoveMapCoordinatesDialog = () => ({ type: types.OPEN_REMOVE_MAP_COORDINATES_DIALOG });
export const closeRemoveMapCoordinatesDialog = () => ({ type: types.CLOSE_REMOVE_MAP_COORDINATES_DIALOG });

export const showNotification = (notificationType, text) => ({ type: types.SHOW_NOTIFICATION, notificationType, text });
export const hideNotification = () => ({ type: types.HIDE_NOTIFICATION });

export const selectThemeForm = (theme) => ({ type: types.SELECT_THEME_FORM, theme });


export const loadPhotoProduct = (photo) => ({ type: types.LOAD_PHOTO_PRODUCT, photo});
export const unloadPhotoProduct = () => ({ type: types.UNLOAD_PHOTO_PRODUCT});

// для фоторедактора
export const fileAccepted = (file) => ({ type: types.FILE_ACCEPTED, file});
export const openImageEditor = (file, fileName, filePreview, crop) => ({ type: types.OPEN_IMAGE_EDITOR, file, fileName, filePreview, crop});
export const closeImageEditor = () => ({ type: types.CLOSE_IMAGE_EDITOR});
export const cropPhoto = (croppedImage, acceptedImage) => ({ type: types.CROP_PHOTO, croppedImage, acceptedImage});
export const clearLoadedFile = () => ({ type: types.CLEAR_LOADED_FILE});

export const clearChangePosition = () => ({ type: types.CLEAR_CHANGE_POSITION});

export const openChangePositionPage = (
    isNew,
    selectedCategory,
    allProducts,
    productId,
    categories,
    product,
) => ({
    type: types.OPEN_CHANGE_POSTION_PAGE,
    isNew,
    selectedCategory,
    allProducts,
    productId,
    categories,
    product,
});
export const closeChangePositionPage = () => ({ type: types.CLOSE_CHANGE_POSTION_PAGE});
export const changePosition = (newPosition) => ({
    type: types.CHANGE_POSITION,
    newPosition,
});

export const switchOnPhonecall = () => ({ type: types.SWITCH_ON_PHONECALL});
export const switchOffPhonecall = () => ({ type: types.SWITCH_OFF_PHONECALL});

export const switchOnWhatsappcall = () => ({ type: types.SWITCH_ON_WHATSAPPCALL});
export const switchOffWhatsappcall = () => ({ type: types.SWITCH_OFF_WHATSAPPCALL});

export const openConnectionCircle = () => ({ type: types.OPEN_CONNECTION_CIRCLE});
export const closeConnectionCircle = () => ({ type: types.CLOSE_CONNECTION_CIRCLE});

export const selectCategory = (category) => ({ type: types.SELECT_CATEGORY, category});

export const switchOnHideAllCategory = () => ({ type: types.SWITCH_ON_HIDE_ALL_CATEGORY});
export const switchOffHideAllCategory = () => ({ type: types.SWITCH_OFF_HIDE_ALL_CATEGORY});

export const categoryCreateStart = (category) => ({ type: types.CATEGORY_CREATE_START, category });
export const categoryCreateSuccess = (category) => ({ type: types.CATEGORY_CREATE_SUCCESS, category });
export const categoryCreateError = (error) => ({ type: types.CATEGORY_CREATE_ERROR, error });

export const categoryEditStart = (category) => ({ type: types.CATEGORY_EDIT_START, category });
export const categoryEditSuccess = (category) => ({ type: types.CATEGORY_EDIT_SUCCESS, category });
export const categoryEditError = (error) => ({ type: types.CATEGORY_EDIT_ERROR, error });

export const categoryRemoveStart = (category) => ({ type: types.CATEGORY_REMOVE_START, category });
export const categoryRemoveSuccess = (category) => ({ type: types.CATEGORY_REMOVE_SUCCESS, category });
export const categoryRemoveError = (error) => ({ type: types.CATEGORY_REMOVE_ERROR, error });

export const openRemoveCategoryDialog = () => ({ type: types.OPEN_REMOVE_CATEGORY_DIALOG });
export const closeRemoveCategoryDialog = () => ({ type: types.CLOSE_REMOVE_CATEGORY_DIALOG });

export const allCategoryStart = () => ({ type: types.ALL_CATEGORY_START });
export const allCategoryEnd = () => ({ type: types.ALL_CATEGORY_END });

export const hideAddCategoryButton = () => ({ type: types.HIDE_ADD_CATEGORY_BUTTON });

export const openCategorySelector = (categories, productCategories, defaultCategory) =>
    ({ type: types.OPEN_CATEGORY_SELECTOR , categories, productCategories, defaultCategory});

export const closeCategorySelector = () => ({ type: types.CLOSE_CATEGORY_SELECTOR });

export const checkboxOnCategorySelector = (category) => ({ type: types.CHECKBOX_ON_CATEGORY_SELECTOR, category });
export const checkboxOffCategorySelector = (category) => ({ type: types.CHECKBOX_OFF_CATEGORY_SELECTOR, category });
export const clearCategorySelector = () => ({ type: types.CLEAR_CATEGORY_SELECTOR });

export const selectCategoryChangePosition = (category) => ({ type: types.SELECT_CATEGORY_CHANGE_POSITION, category});

export const openChangePositionCategoryPage = (items, itemId) => ({type: types.OPEN_CHANGE_POSTION_CATEGORY_PAGE, items, itemId});
export const closeChangePositionCategoryPage = () => ({ type: types.CLOSE_CHANGE_POSTION_CATEGORY_PAGE});
export const changePositionCategory = (newPosition) => ({ type: types.CHANGE_POSITION_CATEGORY, newPosition});
export const clearChangePositionCategory = () => ({ type: types.CLEAR_CHANGE_POSITION_CATEGORY});

export const switchOnSettingsOrders = () => ({ type: types.SWITCH_ON_SETTINGS_ORDERS});
export const switchOffSettingsOrders = () => ({ type: types.SWITCH_OFF_SETTINGS_ORDERS});

export const addProductToClientCart = (product) => ({ type: types.ADD_PRODUCT_TO_CLIENT_CART, product});
export const removeProductFromClientCart = (product) => ({ type: types.REMOVE_PRODUCT_FROM_CLIENT_CART, product});
export const getCartFromStorage = (cart) => ({ type: types.GET_CART_FROM_STORAGE, cart});

export const openClientOrderSuccessItemsPanel = () => ({ type: types.OPEN_CLIENT_ORDER_SUCCESS_ITEMS_PANEL});
export const closeClientOrderSuccessItemsPanel = () => ({ type: types.CLOSE_CLIENT_ORDER_SUCCESS_ITEMS_PANEL});

export const clearClientCart = () => ({ type: types.CLEAR_CLIENT_CART});

export const switchOnOrderAccept = () => ({ type: types.SWITCH_ON_ORDER_ACCEPT});
export const switchOffOrderAccept = () => ({ type: types.SWITCH_OFF_ORDER_ACCEPT});

export const openChangeImageMenu = () => ({ type: types.OPEN_CHANGE_IMAGE_MENU});
export const closeChangeImageMenu = () => ({ type: types.CLOSE_CHANGE_IMAGE_MENU});