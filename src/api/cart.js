import {addToCart, removeFromCart, editCart} from "../utils/common";
import _ from "lodash";

const getClientCartId = (userId) => `client_cart_${userId}`

export const getCart = (userId) => {
    const cartString = localStorage.getItem(getClientCartId(userId))
    let cart = null

    if(cartString){
        cart = JSON.parse(cartString)
    }

    return cart
}

export const addProductToClientCart = (product, userId) => {
    const cart = getCart(userId)
    if(cart){
        const newCart1 = addToCart(cart, product)

        localStorage.setItem(getClientCartId(userId), JSON.stringify(newCart1))
    }else{
        const newProduct = _.cloneDeep(product)
        newProduct.count = 1

        const newCart = {
            items: [newProduct]
        }

        localStorage.setItem(getClientCartId(userId), JSON.stringify(newCart))
    }
}

export const removeProductFromClientCart = (product, userId) => {
    const cart = getCart(userId)

    if(cart){
        const newCart1 = removeFromCart(cart, product)

        localStorage.setItem(getClientCartId(userId), JSON.stringify(newCart1))
    }
}


export const editClientCart = (newCart, userId) => {
    const cart = getCart(userId)

    if(cart){
        const newCart1 = editCart(cart, newCart)

        localStorage.setItem(getClientCartId(userId), JSON.stringify(newCart1))
    }
}

export const clearCart = (userId) => {
    localStorage.setItem(getClientCartId(userId), null)
}

