import axios from 'axios';

export const getAuth = () => {
    return axios.post(`/api/users/getAuth`);
}

export const logout = () => {
    return axios.post(`/api/users/logout`);
}

export const createUser = (user) => {
    return axios.post(`/api/users`, user)
}

export const setUserAccountName = (userId, accountName) => {
    return axios.put(`/api/users/setAccountName/${userId}`, {accountName})
}

export const loginUser = (user) => {
    return axios.post(`/api/users/login`, user)
}

export const getUser = (userId) => {
    return axios.get(`/api/users/${userId}`);
}

export const editUserUni = (userId, editedUser, profile) => {

    const notUndefined = (prop) => (prop === undefined) ? null : prop

    const newUser = {
        companyName: notUndefined(profile.title),
        companyDescription: notUndefined(profile.description),
        companyPhone: notUndefined(profile.phone),
        email: profile.email,
        language: profile.language,
        theme: profile.theme,
        hasSeenSectionDemoPage: profile.hasSeenSectionDemoPage,
        hasSeenContactsDemoPage: profile.hasSeenContactsDemoPage,
        phonecallPhone: notUndefined(profile.phonecallPhone),
        phonecallSwitchOn: profile.phonecallSwitchOn,
        whatsappcallPhone: notUndefined(profile.whatsappcallPhone),
        whatsappcallText: notUndefined(profile.whatsappcallText),
        whatsappcallSwitchOn: profile.whatsappcallSwitchOn,
        hideAllCategorySwitchOn: profile.hideAllCategorySwitchOn,
        allCategoryStart: profile.allCategoryStart,
        allCategoryEnd: profile.allCategoryEnd,
        settingsOrdersSwitchOn: profile.settingsOrdersSwitchOn,
        orderFeedback: notUndefined(profile.orderFeedback),
        orderEmail: notUndefined(profile.orderEmail),
        categoriesProductsSwitchOn: profile.categoriesProductsSwitchOn,

        photoOriginal: null,
        photoSize1080: null,
        photoMetaCropX: null,
        photoMetaCropY: null,
        photoMetaCropWidth: null,
        photoMetaCropHeight: null,
        photoMetaPixelCropX: 'undefined',
        photoMetaPixelCropY: null,
        photoMetaPixelCropWidth: null,
        photoMetaPixelCropHeight: null,
    }

    Object.keys(editedUser).map(key => {
        if(editedUser[key] !== undefined){
            newUser[key] = editedUser[key]
        }
    })

    return editUser(userId, newUser)
}

export const editUser = (userId, user) => {
    var formData = new FormData();

    Object.keys(user).map((key) => {
        let value = user[key]
        if(value !== null) {
            formData.append(key, value);
        }
    })

    return axios.put(`/api/users/${userId}`, formData, {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    })
}

export const createProduct = (product) => {
    var formData = new FormData();

    Object.keys(product).map((key) => {
        let value = product[key]
        if(value !== null) {
            formData.append(key, value);
        }
    })

    // мобильный хром глючит в таком запросе post, сделал put
    return axios.post(`/api/products`, formData, {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    })
}

export const editProduct = (productId, product) => {
    var formData = new FormData();

    Object.keys(product).map((key) => {
        let value = product[key]
        if(value !== null) {
            formData.append(key, value);
        }
    })

    return axios.put(`/api/products/${productId}`, formData, {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    })
}

export const removeProduct = (userId, productId) => {
    return axios.delete(`/api/products/${userId}/${productId}`);
}

export const getUserOrders = (userId, skip=0, limit=10) => {
    return axios.get(`/api/orders/user/${userId}/${skip}/${limit}`);
}

export const getOrder = (orderId) => {
    return axios.get(`/api/orders/${orderId}`);
}

export const createOrder = (order) => {
    return axios.post(`/api/orders`, order);
}

export const editOrder = (orderId, order) => {
    return axios.put(`/api/orders/${orderId}`, order);
}

export const getFile = (fileId) => {
    return `/api/files/${fileId}`
}


export const getImageFile = (url) => {
    return axios({
              method:'get',
              url,
              responseType:'blob'
          })
}

export const getSection = (sectionId) => {
    return axios.get(`/api/sections/${sectionId}`);
}

export const createSection = (section) => {
    return axios.post(`/api/sections`, section);
}

export const editSection = (section) => {
    return axios.put(`/api/sections/${section.sectionId}`, section);
}

export const removeSection = (sectionId, userId) => {
    return axios.delete(`/api/sections/${sectionId}/${userId}`);
}

export const createCategory = (category) => {
    return axios.post(`/api/categories`, category);
}

export const editCategory = (categoryId, category) => {
    return axios.put(`/api/categories/${categoryId}`, category);
}

export const removeCategory = (categoryId, userId) => {
    return axios.delete(`/api/categories/${categoryId}/${userId}`);
}
