import React from 'react';
import i18next from 'i18next';


import {Header} from '../Header'
import {HeaderProfile} from '../HeaderProfile'
import {Products} from '../Products'
import {Dialog} from '../Dialog'
import {Menu} from '../Menu'

import styles from './App.css'

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import * as actions from '../../actions';
import {getUser} from '../../api';
import store from '../../store.js';
import {profilable} from '../hocs'
import {ClientHeader} from "../ClientHeader";
const classNames = require('classnames');
import {PhoneTemplate} from '../common'


import {getOrderButtonVisible} from '../../utils/common'


@profilable
export class AppPure extends React.Component {
    render() {
        const profile = this.props.profile
        const userId = this.props.userId

        const loginedUser = this.props.loginedUser
        const editMode = (loginedUser._id === userId)
        const menuOpened = this.props.menuOpened
        const history = this.props.history

        const hasOrderItems = this.props.clientCartItems.length > 0
        const hasStatus = !!this.props.clientCartStatus


        const styleobj = { }
        // styleobj[`${styles.appFixed}`] = menuOpened
        styleobj[`${styles.app}`] = true

        return (
            <PhoneTemplate>
                <div className={classNames(styleobj)}>
                    <Menu history={history}/>
                    <HeaderProfile />
                    {
                        getOrderButtonVisible(this.props.appState) && (hasOrderItems || hasStatus) &&
                        <ClientHeader history={history}/>
                    }

                    <Header
                        logo={profile.logo}
                        title={profile.title}
                        description={profile.description}
                        userId={userId}
                        editMode={editMode}
                    />
                    <Products userId={userId}/>
                </div>
            </PhoneTemplate>
        )
    }
}

const mapStateToProps = state => ({
    menuOpened: state.global.menuOpened,
    loginedUser: state.global.loginedUser,
    profile: state.profile,
    userId: state.profile._id,
    accountName: state.profile.accountName,
    global: state.global,
    language: state.global.language,
    clientCart: state,
    appState: state,
    clientCartItems: state.clientCart.items,
    clientCartStatus: state.clientCart.status,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const App = connect(mapStateToProps, mapDispatchToProps)(AppPure)