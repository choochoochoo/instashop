import React from 'react'
import InputMask from 'react-input-mask';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import {Dialog} from '../Dialog'
import {createOrder} from '../../api';
import * as actions from '../../actions';
import store from '../../store.js';

import {CallbackForm} from './CallbackForm.jsx'

export const CallbackDialogPure = ({opened, productId, phone, history, userId}) => {

    const handleCancel = () => {
        store.dispatch(actions.hideCallbackDialog());
    }

    const handleSubmit = (values) => {
        store.dispatch(actions.showProgress());
        store.dispatch(actions.orderCreateStart());

        createOrder({
            phone: values.phone,
            comment: values.comment,
            userId: userId,
            productId: productId,
        }).then(response => {
            store.dispatch(actions.orderCreateSuccess(response.data));
            store.dispatch(actions.hideProgress());
            store.dispatch(actions.hideCallbackDialog());
            history.push(`/${userId}/orders/${productId}`)
        }).catch(exception => {
            console.log(exception);
            store.dispatch(actions.orderCreateError(exception));
            store.dispatch(actions.hideProgress());
        });
    }

    return (
        <Dialog
            title='Обратный звонок'
            opened={opened}
            handleCancel={handleCancel}
        >
            <CallbackForm
                onSubmit={handleSubmit}
                handleCancel={handleCancel}
            />
        </Dialog>
    )
}


const mapStateToProps = state => ({
    opened: state.callbackDialog.opened,
    phone: state.callbackDialog.phone,
    global: state.global
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const CallbackDialog = connect(mapStateToProps, mapDispatchToProps)(CallbackDialogPure)