import React from 'react'
import {Field, reduxForm} from 'redux-form'
import { createNumberMask, createTextMask } from 'redux-form-input-masks';

const validate = values => {
    const errors = {}

    if (!values.phone) {
        errors.phone = 'Required'
    } else if (!/^[1-9]{1}[0-9]{3,14}$/i.test(values.phone)) {
        errors.phone = 'Invalid phone number'
    }

    return errors
}

const currencyMask = createNumberMask({
    prefix: 'US$ ',
    suffix: ' per item',
    decimalPlaces: 2,
    locale: 'en-US',
})

const phoneMask = createTextMask({
    pattern: '+9 (999) 999-9999',
});

const renderField = ({
                         input,
                         placeholder,
                         type,
                         accept,
                         meta: {touched, error, warning}
                     }) => (
        <div>
            <input
                {...input}
                placeholder={placeholder}
                type={type}
                accept={accept}
            />

            {touched &&
            ((error && <span>{error}</span>) ||
                (warning && <span>{warning}</span>))}
        </div>
)

let CreateProductFormPure = props => {
    const {handleSubmit, handleCancel} = props
    return (
        <form onSubmit={handleSubmit}>
            <Field
                name="phone"
                component="input"
                type="tel"
                {...phoneMask}
                placeholder='телефон'
                component={renderField}
            />

            <Field
                name="comment"
                component="textarea"
                rows="5"
                placeholder='комментарий'
            />

            <div>
                <button onClick={handleCancel}>Cancel</button>
                <button type="submit">Submit</button>
            </div>
        </form>
    )
}

export const CallbackForm = reduxForm({
    form: 'CallbackForm',
    validate
})(CreateProductFormPure)

