import React from 'react'
import styles from './AllCategoryPanel.css'
import {SvgIcon, Switch, SwitchInputField} from '../common'
import i18next from 'i18next';

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as actions from '../../actions';
import store from '../../store.js';

import {RadioButton} from './RadioButton.jsx'
import {editUserUni} from "../../api";
import * as notificaitonTypes from "../../constants/NotificationTypes";

export const AllCategoryPanelPure = ({
                                  hideAllCategorySwitchOn,
                                  allCategoryStart,
                                  allCategoryEnd,
                                  userId,
                                  profile,
                                }) => {

    const switchClick = () => {
        store.dispatch(actions.showProgress());
        store.dispatch(actions.userEditStart());

        editUserUni(userId, {
            hideAllCategorySwitchOn: !hideAllCategorySwitchOn,
        }, profile).then(response => {
            store.dispatch(actions.userEditSuccess(response.data));
            store.dispatch(actions.hideProgress());

            if(hideAllCategorySwitchOn) {
                store.dispatch(actions.switchOffHideAllCategory());
            }else{
                store.dispatch(actions.switchOnHideAllCategory());
            }

            store.dispatch(actions.showNotification(notificaitonTypes.SAVE));
            setTimeout(()=>{
                store.dispatch(actions.hideNotification());
            }, notificaitonTypes.TIMEOUT)

        }).catch(exception => {
            console.log('Произошла ошибка на сервере');
            console.log(exception);
            store.dispatch(actions.userEditError(exception));
            store.dispatch(actions.hideProgress());
        });

    }

    const allCategoryStartClick = () => {

        store.dispatch(actions.showProgress());
        store.dispatch(actions.userEditStart());

        editUserUni(userId, {
            allCategoryStart: true,
            allCategoryEnd: false,
        }, profile).then(response => {
            store.dispatch(actions.userEditSuccess(response.data));
            store.dispatch(actions.hideProgress());
            store.dispatch(actions.allCategoryStart());

            store.dispatch(actions.showNotification(notificaitonTypes.SAVE));
            setTimeout(()=>{
                store.dispatch(actions.hideNotification());
            }, notificaitonTypes.TIMEOUT)
        }).catch(exception => {
            console.log('Произошла ошибка на сервере');
            console.log(exception);
            store.dispatch(actions.userEditError(exception));
            store.dispatch(actions.hideProgress());
        });


    }

    const allCategoryEndClick = () => {

        store.dispatch(actions.showProgress());
        store.dispatch(actions.userEditStart());

        editUserUni(userId, {
            allCategoryStart: false,
            allCategoryEnd: true,
        }, profile).then(response => {
            store.dispatch(actions.userEditSuccess(response.data));
            store.dispatch(actions.hideProgress());
            store.dispatch(actions.allCategoryEnd());

            store.dispatch(actions.showNotification(notificaitonTypes.SAVE));
            setTimeout(()=>{
                store.dispatch(actions.hideNotification());
            }, notificaitonTypes.TIMEOUT)
        }).catch(exception => {
            console.log('Произошла ошибка на сервере');
            console.log(exception);
            store.dispatch(actions.userEditError(exception));
            store.dispatch(actions.hideProgress());
        });
    }

    return (
        <div className={styles.root}>
            <div className={styles.switch}>
                <div className={styles.switchLeftCol}>
                    {i18next.t('categories_hide_all_title')}
                </div>
                <div className={styles.switchRightCol}>
                    <Switch click={switchClick} value={hideAllCategorySwitchOn}/>
                </div>
            </div>

            {
                hideAllCategorySwitchOn &&
                <div className={styles.content}>
                    <RadioButton
                        name="start"
                        label={i18next.t('categories_hide_all_start')}
                        onClick={allCategoryStartClick}
                        checked={allCategoryStart}
                    />

                    <RadioButton
                        name="end"
                        label={i18next.t('categories_hide_all_end')}
                        onClick={allCategoryEndClick}
                        checked={allCategoryEnd}
                    />
                </div>
            }
        </div>
    )
}

const mapStateToProps = state => ({
    userId: state.profile._id,
    profile: state.profile,
    hideAllCategorySwitchOn: state.profile.hideAllCategorySwitchOn,
    allCategoryStart: state.profile.allCategoryStart,
    allCategoryEnd: state.profile.allCategoryEnd,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const AllCategoryPanel = connect(mapStateToProps, mapDispatchToProps)(AllCategoryPanelPure)

