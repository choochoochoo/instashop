import React from 'react'
import styles from './Categories.css'
import i18next from 'i18next';
import {ClosePanel, Switch, AddCategoryPanel, PhoneTemplate, Button, SecondButton} from '../common'

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import * as actions from '../../actions';
import store from '../../store.js';
import {profilable} from '../hocs'

import {CategoriesPanel} from './CategoriesPanel.jsx'
import {createCategory, editUserUni} from "../../api";
import * as notificaitonTypes from "../../constants/NotificationTypes";

import {AllCategoryPanel} from "./AllCategoryPanel.jsx";
import { categoriesSwitchOn} from '../../resources'



export const CategoriesPure = ({
                                   match,
                                   history,
                                   accountName,
                                   userId,
                                   profile,
                                    categoriesProductsSwitchOn,
                               }) => {

    const handleSubmit = (switchOn) => {
        store.dispatch(actions.showProgress());
        store.dispatch(actions.userEditStart())

        editUserUni(userId, {
            categoriesProductsSwitchOn: switchOn,
        }, profile).then(response => {
            store.dispatch(actions.userEditSuccess(response.data));
            store.dispatch(actions.hideProgress());
            // history.push(`/${accountName}`)

            store.dispatch(actions.showNotification(notificaitonTypes.SAVE));
            setTimeout(()=>{
                store.dispatch(actions.hideNotification());
            }, notificaitonTypes.TIMEOUT)

        }).catch(exception => {
            store.dispatch(actions.userEditError(exception));
            store.dispatch(actions.hideProgress());

            store.dispatch(actions.showNotification(notificaitonTypes.ERROR));
            setTimeout(()=>{
                store.dispatch(actions.hideNotification());
            }, notificaitonTypes.TIMEOUT)
        });
    }

    return (
        <PhoneTemplate>
            <div className={styles.root}>
                <ClosePanel
                    text={i18next.t('categories_title')}
                    userId={accountName}
                    link={`/${accountName}/settings`}
                    border={!!categoriesProductsSwitchOn}
                />
                {
                    categoriesProductsSwitchOn &&
                    <div>
                        <AllCategoryPanel />
                        <CategoriesPanel history={history}/>
                        <AddCategoryPanel />

                        <div className={styles.switchOffButton}>
                            <SecondButton
                                title={i18next.t('categories_button_switch_off')}
                                onClick={handleSubmit.bind(null, false)}
                            />
                        </div>
                    </div>
                }

                {
                    !categoriesProductsSwitchOn &&
                    <div>
                        <img src={`/${categoriesSwitchOn}`} width="220px"/>
                        <div className={styles.text}>{i18next.t('categories_switchon_text1')}</div>
                        <div className={styles.text2}>{i18next.t('categories_switchon_text2')}</div>
                        <div className={styles.switchOnButton}>
                            <Button
                                title={i18next.t('categories_button_switch_on')}
                                onClick={handleSubmit.bind(null, true)}
                            />
                        </div>
                    </div>
                }
            </div>
        </PhoneTemplate>
    )
}


const mapStateToProps = state => ({
    userId: state.profile._id,
    accountName: state.profile.accountName,
    categoriesProductsSwitchOn: state.profile.categoriesProductsSwitchOn,
    profile: state.profile,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const Categories = connect(mapStateToProps, mapDispatchToProps)(profilable(CategoriesPure))