import React, {PropTypes} from 'react'
import styles from './CategoriesPanel.css'

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as actions from '../../actions';
import store from '../../store.js';

import {CategoryItem} from './CategoryItem.jsx'

export const CategoriesPanelPure = ({
                                        categories,
                                        theme,
                                        history,
                                        accountName,
                                    }) => {
    return (
        <div className={styles.root}>
                {
                    categories.map((item, index) => {

                        const categoryClick = () => {
                            history.replace(`/${accountName}/categories/edit/${item._id}`)
                        }

                        return (
                            <CategoryItem
                                key={item._id}
                                title={item.title}
                                theme={theme}
                                click={categoryClick}
                            />
                        )
                    })
                }
        </div>
    )
}

const mapStateToProps = state => ({
    theme: state.theme,
    categories: state.profile.categories,
    accountName: state.profile.accountName,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const CategoriesPanel = connect(mapStateToProps, mapDispatchToProps)(CategoriesPanelPure)