import React, {PropTypes} from 'react'
import styles from './CategoryItem.css'
import {SvgIcon} from '../common'


export const CategoryItem = ({
                                title,
                                theme,
                                selected,
                                click,
                            }) => {


    return (
        <button className={styles.button} onClick={click}>
            <div className={styles.title}>
                {title}
            </div>
            <div className={styles.arrow}>
                <SvgIcon icon="arrow"/>
            </div>
        </button>
    )
}
