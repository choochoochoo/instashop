import React from 'react'
import {Link} from 'react-router-dom';
import i18next from 'i18next';
import {Field, reduxForm} from 'redux-form'

import styles from './RadioButton.css'

const classNames = require('classnames');

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import * as actions from '../../actions/index';
import store from '../../store.js';


export const RadioButtonPure = ({name, label, theme, onClick, checked}) => {

    const style = checked  ? {
        'background': '#f8d75c'
    } : {}

    return (

            <li className={styles.element}>
                <button onClick={onClick}>
                    <input type="radio"  name={name} className={styles.input} />
                    <label htmlFor={name} className={styles.label}>{label}</label>
                    <div className={styles.check} style={style}></div>
                </button>
            </li>
    )
}

const mapStateToProps = state => ({
    theme: state.theme
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const RadioButton = connect(mapStateToProps, mapDispatchToProps)(RadioButtonPure)