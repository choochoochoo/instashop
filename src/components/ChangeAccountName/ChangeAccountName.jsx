import React from 'react'
import styles from './ChangeAccountName.css'
import i18next from 'i18next';
import {Title, Subtitle, ClosePanel, SvgIcon, PhoneTemplate,} from '../common'

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import * as actions from '../../actions';
import store from '../../store.js';
import {profilable} from '../hocs'

import {ChangeAccountNameForm} from './ChangeAccountNameForm.jsx'

import {setUserAccountName} from '../../api';
import * as notificaitonTypes from '../../constants/NotificationTypes'
import {sendErrorToAnaliticsExc} from "../../utils/common";

export const ChangeAccountNamePure = ({
                                    match,
                                    history,
                                    accountName,
                                    profile,
                                    userId,
                                    serverError,
                                }) => {


        const handleSubmit = (values) => {

            store.dispatch(actions.showProgress());
            store.dispatch(actions.userChangeAccountNameStart())

            setUserAccountName(userId, values.accountName.trim().toLowerCase())
                .then(response => {

                    if(response.data.code === 'ok'){
                        store.dispatch(actions.userChangeAccountNameSuccess(response.data.user));
                        history.push(`/${response.data.user.accountName}`)
                        store.dispatch(actions.showNotification(notificaitonTypes.SAVE));
                        setTimeout(()=>{
                            store.dispatch(actions.hideNotification());
                        }, notificaitonTypes.TIMEOUT)
                    }else{
                        store.dispatch(actions.userChangeAccountNameNotSuccess(response.data.code));

                    }

                    store.dispatch(actions.hideProgress());


                }).catch(exception => {
                    sendErrorToAnaliticsExc(exception)

                    console.log(exception);
                    store.dispatch(actions.userChangeAccountNameError(exception));
                    store.dispatch(actions.hideProgress());

                    store.dispatch(actions.showNotification(notificaitonTypes.ERROR));

                    setTimeout(()=>{
                        store.dispatch(actions.hideNotification());
                    }, notificaitonTypes.TIMEOUT)
                });
        }




    return (
        <PhoneTemplate>
            <div className={styles.root}>
                <ClosePanel
                    text={i18next.t('change_account_name_page_title')}
                    userId={accountName}
                    link={`/${accountName}`}
                    border={false}
                />

                <ChangeAccountNameForm
                    onSubmit={handleSubmit}
                    serverError={serverError ? i18next.t(`change_account_name_error_${serverError}`) : null}
                />
            </div>
        </PhoneTemplate>
    )
}


const mapStateToProps = state => ({
    userId: state.profile._id,
    accountName: state.profile.accountName,
    profile: state.profile,
    serverError: state.global.changeAccountNamgeServerError,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const ChangeAccountName = connect(mapStateToProps, mapDispatchToProps)(profilable(ChangeAccountNamePure))