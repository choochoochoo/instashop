import React from 'react'
import {Field, reduxForm} from 'redux-form'
import i18next from 'i18next';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import {
    Button,
} from '../common'

import {InputField} from './InputField.jsx'

import styles from './ChangeAccountNameForm.css'

import names from '../../constants/names.json'


const accountNameRegExp = /^([a-z\-0-9._]*)$/


const validate = values => {
    const errors = {}
    const min_sym = 5

    if (!values.accountName) {
        errors.accountName = i18next.t('input_field_required')
    }

    if (values.accountName && values.accountName.length < min_sym) {
        errors.accountName = `${min_sym} ${i18next.t('input_field_min_length')}`
    }

    if (!accountNameRegExp.test(values.accountName)) {
        errors.accountName = i18next.t('account_name_field_rules')
    }


    return errors
}


let ChangeAccountNameFormPure = props => {
    const {
        handleSubmit,
        serverError,
    } = props

    return (
        <form onSubmit={handleSubmit} className={styles.root}>
            <InputField
                name="accountName"
                label={`${names.brand_name}/`}
                placeholder={names.account_name}
                required={true}
                serverError={serverError}
                autoComplete={false}
                autoCorrect={false}
                autoCapitalize={false}
                spellCheck={false}
            />

            <div className={styles.mainButton}>
                <Button
                    title={i18next.t('change_account_name_page_title')}
                />
            </div>
        </form>
    )
}

ChangeAccountNameFormPure = reduxForm({
    form: 'ChangeAccountNameForm',
    validate,
    enableReinitialize: true
})(ChangeAccountNameFormPure)


export const ChangeAccountNameForm = connect(
    state => {
        return {
            // initialValues: state.profile,
        }
    }
)(ChangeAccountNameFormPure)


