import React from 'react'
import {Link} from 'react-router-dom';
import i18next from 'i18next';
import {Field, reduxForm} from 'redux-form'

import styles from './InputField.css'

const classNames = require('classnames');

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import * as actions from '../../actions/index';
import store from '../../store.js';


const renderField = ({
                         input,
                         placeholder,
                         type,
                         accept,
                         meta: {touched, error, warning},
                         label,
                         theme,
                         serverError,
                         autoComplete,
                         autoCorrect,
                         autoCapitalize,
                         spellCheck,
                     }) => {

    const styleError = touched && error ? {
        borderTop: 'solid 1px #f57123',
        borderBottom: 'solid 1px #f57123',
    } : {}


    let errorText = (touched && error) ? `${error}` : null

    if(serverError){
        errorText = serverError
    }

    return (
        <div className={styles.inputWrapper}>
            <label className={styles.trueLabel}>
                <div className={styles.label}>
                    {label}
                </div>
                <div className={styles.inputWrapper2} style={styleError}>
                    <input
                        {...input}
                        placeholder={placeholder}
                        type={type}
                        accept={accept}
                        className={styles.input}
                        style={{
                            color: theme['common_field_text-color']
                        }}
                        autoComplete={autoComplete ? 'on' : 'off'}
                        autoCorrect={autoCorrect ? 'on' : 'off'}
                        autoCapitalize={autoCapitalize ? 'on' : 'off'}
                        spellCheck={spellCheck}
                    />
                </div>
            </label>
            { !errorText && <div className={styles.text}>
                <p>{i18next.t('change_account_name_page_persistent_link')}</p>
                <p><span className={styles.attention}>{i18next.t('change_account_name_page_persistent_link_attention')}</span></p>
            </div> }

            { errorText && <div  className={styles.error}>
                {errorText}
            </div> }
        </div>
    )
}


export const InputFieldPure = ({
                                   name,
                                   type,
                                   placeholder,
                                   label,
                                   theme,
                                   serverError,
                                   autoComplete = true,
                                   autoCorrect = true,
                                   autoCapitalize = true,
                                   spellCheck = true,
                               }) => {
    return (
        <div className={styles.root}>
            <Field
                name={name}
                type={type}
                placeholder={placeholder}
                component={renderField}
                label={label}
                theme={theme}
                serverError={serverError}
                autoComplete={autoComplete}
                autoCorrect={autoCorrect}
                autoCapitalize={autoCapitalize}
                spellCheck={spellCheck}
            />
        </div>
    )
}

const mapStateToProps = state => ({
    theme: state.theme
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const InputField = connect(mapStateToProps, mapDispatchToProps)(InputFieldPure)