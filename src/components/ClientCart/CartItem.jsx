import React, {PropTypes} from 'react'
import styles from './CartItem.css'
import {Counter} from './Counter.jsx'
import {getFile} from "../../api";
import {defaultProductImage} from "../../resources";

export const CartItem = ({
                                product,
                                theme,
                            }) => {

    const photo = product.photo ? getFile(product.photo.size1080) : `/${defaultProductImage}`

    return (
        <div className={styles.root}>
            <div className={styles.leftCol}>
                <div className={styles.photoWrapper}>
                    <img src={photo} width="100%"/>
                </div>
            </div>

            <div className={styles.rightCol}>

                <div className={styles.title}>{product.title}</div>
                <div className={styles.price}>{product.price}</div>
                <Counter product={product} />
            </div>
        </div>
    )
}
