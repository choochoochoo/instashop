import React, {PropTypes} from 'react'
import styles from './CartPanel.css'

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as actions from '../../actions';
import store from '../../store.js';

import {CartItem} from './CartItem.jsx'

export const CartPanelPure = ({
                                        items,
                                        theme,
                                        history,
                                        accountName,
                                    }) => {
    return (
        <div className={styles.root}>
                {
                    items.map((item, index) => {
                        return (
                            <CartItem
                                key={item._id}
                                theme={theme}
                                product={item}
                            />
                        )
                    })
                }
        </div>
    )
}

const mapStateToProps = state => ({
    theme: state.theme,
    items: state.clientCart.items,
    accountName: state.profile.accountName,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const CartPanel = connect(mapStateToProps, mapDispatchToProps)(CartPanelPure)