import React from 'react'
import styles from './ClientCart.css'
import i18next from 'i18next';
import {Title, Subtitle, ClosePanel, SvgIcon, FeedbackPanel, Button, PhoneTemplate} from '../common'

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import * as actions from '../../actions';
import store from '../../store.js';
import {profilable} from '../hocs'
import {CartPanel} from './CartPanel.jsx'

import {SecondButton} from './SecondButton.jsx'


import {editUserUni} from '../../api';
import * as notificaitonTypes from '../../constants/NotificationTypes'

export const ClientCartPure = ({
                                    match,
                                    history,
                                    accountName,
                                    profile,
                                    userId,
                                }) => {

    const orderRegister = () => {
        history.replace(`/${accountName}/client-order-register`)
    }

    const back = () => {
        history.replace(`/${accountName}`)
    }


    return (
        <PhoneTemplate>
            <div className={styles.root}>
                <ClosePanel
                    text={i18next.t('client_cart_title')}
                    userId={accountName}
                    border={false}
                    type="white"
                />

                <CartPanel />

                <SecondButton
                    onClick={back}
                    title={i18next.t('client_cart_button_add_more')}
                />

                <Button
                    onClick={orderRegister}
                    title={i18next.t('client_order_register_title')}
                    type="button"
                    typeStyle="white"
                />
            </div>
        </PhoneTemplate>
    )
}


const mapStateToProps = state => ({
    userId: state.profile._id,
    accountName: state.profile.accountName,
    profile: state.profile,

})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const ClientCart = connect(mapStateToProps, mapDispatchToProps)(profilable(ClientCartPure))