import React, {PropTypes} from 'react'
import styles from './Counter.css'

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as actions from '../../actions';
import store from '../../store.js';
import {addProductToClientCart, removeProductFromClientCart} from "../../api/cart";
import {SvgIcon} from "../common";

export const CounterPure = ({product, userId}) => {

    const plusClick = () => {
        addProductToClientCart(product, userId)
        store.dispatch(actions.addProductToClientCart(product));    }

    const minusClick = () => {
        removeProductFromClientCart(product, userId)
        store.dispatch(actions.removeProductFromClientCart(product));
    }

    return (
        <div className={styles.root}>
            <button onClick={minusClick} className={styles.button}>
                <SvgIcon icon="itemMinus" />
            </button>
            <div className={styles.count}>
                {product.count}
            </div>
            <button onClick={plusClick}>
                <SvgIcon icon="itemPlus" />
            </button>
        </div>
    )
}

const mapStateToProps = state => ({
    theme: state.theme,
    accountName: state.profile.accountName,
    userId: state.profile._id,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const Counter = connect(mapStateToProps, mapDispatchToProps)(CounterPure)