import React from 'react'

import styles from './CartButton.css'

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import * as actions from '../../actions';
import store from '../../store.js';

const classNames = require('classnames');

export const CartButtonPure = ({
                           title,
                           click,
                        }) => {
    return (
        <div className={styles.root}>
            <button type="button" className={styles.button} onClick={click} >
                <span className={styles.buttonText}>{title}</span>
             </button>
        </div>
    )
}

const mapStateToProps = state => ({
    theme: state.theme
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const CartButton = connect(mapStateToProps, mapDispatchToProps)(CartButtonPure)



