import React from 'react'
import {Link} from 'react-router-dom';
import i18next from 'i18next';

import styles from './ClientHeader.css'

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import * as actions from '../../actions';
import store from '../../store.js';

import {CartButton} from './CartButton.jsx'

export const ClientHeaderPure = ({
                                      logo,
                                      userId,
                                      loginedUser,
                                      history,
                                      accountName,
                                      orderStatus,
                                  }) => {

    const cartButtonClick = () => {
        if(orderStatus === 'new'){
            history.push(`/${accountName}/client-order-success`)
        }else{
            history.push(`/${accountName}/client-cart`)
        }
    }

    return (
        <div className={styles.root}>
            <div className={styles.content}>
                <div className={styles.leftCol}>

                </div>
                <div className={styles.rightCol}>
                    <CartButton
                        title={i18next.t('cart_button_title')}
                        click={cartButtonClick}
                    />
                </div>
            </div>
        </div>
    )
}


const mapStateToProps = state => ({
    viewModeEnabled: state.global.viewModeEnabled,
    accountName: state.profile.accountName,
    orderStatus: state.clientCart.status,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const ClientHeader = connect(mapStateToProps, mapDispatchToProps)(ClientHeaderPure)