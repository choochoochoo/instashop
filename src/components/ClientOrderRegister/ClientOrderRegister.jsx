import React from 'react'
import styles from './ClientOrderRegister.css'
import i18next from 'i18next';
import {Title, Subtitle, ClosePanel, SvgIcon, PhoneTemplate,} from '../common'

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import * as actions from '../../actions';
import store from '../../store.js';
import {profilable} from '../hocs'

import {ClientOrderRegisterForm} from './ClientOrderRegisterForm.jsx'

import {createOrder} from '../../api';
import * as notificaitonTypes from '../../constants/NotificationTypes'
import {sendErrorToAnaliticsExc} from "../../utils/common";

import {editClientCart} from '../../api/cart'

export const ClientOrderRegisterPure = ({
                                    match,
                                    history,
                                    accountName,
                                    profile,
                                    userId,
                                    orderItems,
                                }) => {


        const handleSubmit = (values) => {

            store.dispatch(actions.showProgress());
            store.dispatch(actions.orderCreateStart())

            createOrder({
                userId: userId,
                phone: values.phone,
                comment: values.comment,
                products: orderItems.map(item => {return {
                    _id: item._id,
                    title: item.title,
                    price: item.price,
                    photo: item.photo && item.photo.size1080,
                    count: item.count,
                }})

            }).then(response => {
                store.dispatch(actions.orderCreateSuccess(response.data));
                store.dispatch(actions.hideProgress());

                editClientCart(response.data, userId)

                history.push(`/${accountName}/client-order-success`)

                store.dispatch(actions.showNotification(notificaitonTypes.SAVE));
                setTimeout(()=>{
                    store.dispatch(actions.hideNotification());
                }, notificaitonTypes.TIMEOUT)
            }).catch(exception => {
                sendErrorToAnaliticsExc(exception)

                console.log(exception);
                store.dispatch(actions.orderCreateError(exception));
                store.dispatch(actions.hideProgress());

                store.dispatch(actions.showNotification(notificaitonTypes.ERROR));

                setTimeout(()=>{
                    store.dispatch(actions.hideNotification());
                }, notificaitonTypes.TIMEOUT)
            });
        }




    return (
        <PhoneTemplate>
            <div className={styles.root}>
                <ClosePanel
                    text={i18next.t('client_order_register_title')}
                    userId={accountName}
                    border={false}
                    type="white"
                    link={`/${accountName}/client-cart`}
                />

                <ClientOrderRegisterForm
                    onSubmit={handleSubmit}
                />


            </div>
        </PhoneTemplate>
    )
}


const mapStateToProps = state => ({
    userId: state.profile._id,
    accountName: state.profile.accountName,
    profile: state.profile,
    orderItems: state.clientCart.items
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const ClientOrderRegister = connect(mapStateToProps, mapDispatchToProps)(profilable(ClientOrderRegisterPure))