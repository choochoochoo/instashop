import React from 'react'
import {Field, reduxForm} from 'redux-form'
import i18next from 'i18next';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import {
    InputField,
    TextareaField,
    PhoneField,
    Button,
    FileUploadField,
    SelectField, SwitchInputField, SwitchTextareaField,
} from '../common'

import styles from './ClientOrderRegisterForm.css'

const validate = values => {
    const errors = {}

    if (!values.phone) {
        errors.phone = i18next.t('input_field_required')
    }

    // else if (!/^[1-9]{1}[0-9]{3,14}$/i.test(values.phone)) {
    //     errors.phone = i18next.t('input_field_invalid_phone')
    // }

    return errors
}

let ClientOrderRegisterFormPure = props => {
    const {
        handleSubmit,
    } = props

    return (
        <form onSubmit={handleSubmit} className={styles.root}>
            <SwitchInputField
                name="phone"
                label={i18next.t('сlient_order_register_form_phone_label')}
                placeholder={i18next.t('сlient_order_register_form_phone_placeholder')}
                required={true}
            />

            <SwitchTextareaField
                name="comment"
                label={i18next.t('сlient_order_register_form_comment_label')}
                placeholder={i18next.t('сlient_order_register_form_comment_placeholder')}
            />
            <div>
                <Button
                    title={i18next.t('сlient_order_register_button_title')}
                    typeStyle="white"
                />
            </div>
        </form>
    )
}

ClientOrderRegisterFormPure = reduxForm({
    form: 'ClientOrderRegisterForm',
    validate,
    enableReinitialize: true
})(ClientOrderRegisterFormPure)


export const ClientOrderRegisterForm = connect(
    state => {
        return {
            // initialValues: state.profile,
        }
    }
)(ClientOrderRegisterFormPure)


