import React, {PropTypes} from 'react'
import styles from './ClientOrderItemsPanel.css'
import {CartItemsPanel, SvgIcon} from "../common";
import i18next from 'i18next'


export const ClientOrderItemsPanel = ({items, click, showItems}) => {
    return (
        <button className={styles.button} onClick={click}>
            <div className={styles.panel}>
                <div className={styles.leftCol}>
                    {i18next.t('client_order_success_order_items_label')}
                </div>
                <div className={styles.rightCol}>
                    <SvgIcon icon="arrow"/>
                </div>
            </div>
            { showItems && <CartItemsPanel items={items}/> }
        </button>
    )
}
