import React from 'react'
import styles from './ClientOrderSuccess.css'
import i18next from 'i18next';
import {SvgIcon, Button, WhatsappLink, PhoneTemplate} from '../common'

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import * as actions from '../../actions';
import store from '../../store.js';
import {profilable} from '../hocs'
import {ClientOrderItemsPanel} from './ClientOrderItemsPanel.jsx'

import {SecondButton} from './SecondButton.jsx'

var pad = require('pad-number');


export const ClientOrderSuccessPure = ({
                                            match,
                                            history,
                                            accountName,
                                            profile,
                                            userId,
                                            orderItems,
                                            orderPhone,
                                            orderFeedback,
                                            orderNumber,
                                            openClientOrderSuccessItemsPanel,
                                            whatsappcallPhone,
                                            whatsappcallText,
                                            whatsappcallSwitchOn,
                                        }) => {

    const clickItemsPanel = () => {
        if(openClientOrderSuccessItemsPanel){
            store.dispatch(actions.closeClientOrderSuccessItemsPanel())

        }else{
            store.dispatch(actions.openClientOrderSuccessItemsPanel())
        }
    }

    const back = () => {
        history.replace(`/${accountName}`)
    }

    return (
        <PhoneTemplate>
            <div className={styles.root}>
                <div className={styles.numberCol}>
                    <div className={styles.successIcon}>
                        <SvgIcon icon="orderSuccess"/>
                    </div>
                    <div className={styles.numberLabel}>
                        {i18next.t('client_order_success_order_number_label')}
                    </div>
                    <div className={styles.number}>
                        {orderNumber ? pad(orderNumber, 3) : null}
                    </div>
                    <div className={styles.orderFeedback}>
                        {orderFeedback}
                    </div>
                </div>
                <div className={styles.phonePanel}>
                    <div className={styles.phonePanelLeftCol}>
                        {i18next.t('client_order_success_order_phone_label')}
                    </div>
                    <div className={styles.phonePanelRightCol}>
                        {orderPhone}
                    </div>
                </div>

                <ClientOrderItemsPanel
                    items={orderItems}
                    showItems={openClientOrderSuccessItemsPanel}
                    click={clickItemsPanel}
                />

                <SecondButton
                    onClick={back}
                    title={i18next.t('client_order_success_show_more')}
                />

                { whatsappcallSwitchOn && <WhatsappLink
                    phone={whatsappcallPhone}
                    text={whatsappcallText}
                >
                    <Button
                        title={i18next.t('connections_whatsappcall_title')}
                        type="button"
                        typeStyle="white"
                        className={styles.call}
                    />
                </WhatsappLink> }
            </div>
        </PhoneTemplate>
    )
}


const mapStateToProps = state => ({
    userId: state.profile._id,
    accountName: state.profile.accountName,
    profile: state.profile,
    orderFeedback: state.profile.orderFeedback,
    orderItems: state.clientCart.items,
    orderPhone: state.clientCart.phone,
    orderNumber: state.clientCart.number,
    openClientOrderSuccessItemsPanel: state.global.openClientOrderSuccessItemsPanel,
    whatsappcallPhone: state.profile.whatsappcallPhone,
    whatsappcallText: state.profile.whatsappcallText,
    whatsappcallSwitchOn: state.profile.whatsappcallSwitchOn,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const ClientOrderSuccess = connect(mapStateToProps, mapDispatchToProps)(profilable(ClientOrderSuccessPure))