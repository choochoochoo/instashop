import React from 'react'
import styles from './Connections.css'
import i18next from 'i18next';
import {Title, Subtitle, ClosePanel, SvgIcon, PhoneTemplate} from '../common'

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import * as actions from '../../actions';
import store from '../../store.js';
import {profilable} from '../hocs'
import {ConnectionsForm} from './ConnectionsForm.jsx'

import {editUserUni} from '../../api';
import * as notificaitonTypes from '../../constants/NotificationTypes'

export const ConnectionsPure = ({
                                    match,
                                    history,
                                    accountName,
                                    profile,
                                    phonecallSwitchOn,
                                    userId,
                                    whatsappcallSwitchOn,
                                }) => {

    const handleSubmit = (values) => {
        store.dispatch(actions.showProgress());
        store.dispatch(actions.userEditStart())

        editUserUni(userId, {
            phonecallPhone: values.phonecallPhone,
            phonecallSwitchOn: phonecallSwitchOn,
            whatsappcallPhone: values.whatsappcallPhone,
            whatsappcallText: values.whatsappcallText,
            whatsappcallSwitchOn: whatsappcallSwitchOn,

        }, profile).then(response => {
            store.dispatch(actions.userEditSuccess(response.data));
            store.dispatch(actions.hideProgress());
            history.push(`/${accountName}`)

            store.dispatch(actions.showNotification(notificaitonTypes.SAVE));
            setTimeout(()=>{
                store.dispatch(actions.hideNotification());
            }, notificaitonTypes.TIMEOUT)

        }).catch(exception => {
            store.dispatch(actions.userEditError(exception));
            store.dispatch(actions.hideProgress());

            store.dispatch(actions.showNotification(notificaitonTypes.ERROR));
            setTimeout(()=>{
                store.dispatch(actions.hideNotification());
            }, notificaitonTypes.TIMEOUT)
        });
    }


    const panel = (
        <div className={styles.description}>
            {i18next.t('connections_description')}
        </div>
    )

    return (
        <PhoneTemplate>
            <div className={styles.root}>
                <ClosePanel
                    text={i18next.t('connections_title')}
                    userId={accountName}
                    buttons={panel}
                    link={`/${accountName}/settings`}
                />

                <ConnectionsForm
                    onSubmit={handleSubmit}
                />
            </div>
        </PhoneTemplate>
    )
}


const mapStateToProps = state => ({
    userId: state.profile._id,
    accountName: state.profile.accountName,
    profile: state.profile,
    phonecallSwitchOn: state.global.phonecallSwitchOn,
    whatsappcallSwitchOn: state.global.whatsappcallSwitchOn,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const Connections = connect(mapStateToProps, mapDispatchToProps)(profilable(ConnectionsPure))