import React from 'react'
import {Field, reduxForm} from 'redux-form'
import {connect} from "react-redux";
import {Phonecall} from './Phonecall.jsx'
import {Whatsappcall} from './Whatsappcall.jsx'
import i18next from "i18next";


import {
    Button,
} from '../common'


const validate = values => {
    const errors = {}

    // if (!values.photo) {
    //     errors.photo = 'Required'
    // }

    // if (!values.title) {
    //     errors.title = 'Required'
    // }
    //
    // if (!values.price) {
    //     errors.price = 'Required'
    // }

    return errors
}



let ConnectionsFormPure = props => {
    const {
        handleSubmit,
    } = props

    return (
        <div>
            <form onSubmit={handleSubmit}>

                <Phonecall/>
                <Whatsappcall/>

                <div>
                    <Button title={i18next.t('profile_submit_save')} />
                </div>
            </form>
        </div>
    )
}

ConnectionsFormPure = reduxForm({
    form: 'ConnectionsForm',
    validate,
    enableReinitialize: true,
})(ConnectionsFormPure)

export const ConnectionsForm = connect(
    (state, ownParam) => {
        return {
             initialValues: {
                 phonecallPhone: state.profile.phonecallPhone,
                 whatsappcallPhone: state.profile.whatsappcallPhone,
                 whatsappcallText: state.profile.whatsappcallText,
             }
        }
    }
)(ConnectionsFormPure)
