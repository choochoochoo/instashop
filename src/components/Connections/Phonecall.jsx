import React from 'react'
import styles from './Phonecall.css'
import {SvgIcon, Switch, SwitchInputField} from '../common'
import i18next from 'i18next';

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as actions from '../../actions';
import store from '../../store.js';

export const PhonecallPure = ({phonecallSwitchOn}) => {

    const switchClick = () => {
        if(phonecallSwitchOn) {
            store.dispatch(actions.switchOffPhonecall());
        }else{
            store.dispatch(actions.switchOnPhonecall());
        }
    }

    return (
        <div className={styles.root}>
            <div className={styles.header}>
                <div className={styles.leftCol}>
                    <div className={styles.icon}>
                        <SvgIcon
                            icon="phonecall"
                            className={styles.iconSvg}
                        />
                    </div>

                </div>
                <div className={styles.rightCol}>
                    <div className={styles.title}>
                        {i18next.t('connections_phonecall_title')}
                    </div>
                    <Switch click={switchClick} value={phonecallSwitchOn}/>
                </div>
            </div>
            {
                phonecallSwitchOn &&
                <div className={styles.content}>
                    <div className={styles.contentText}>
                        {i18next.t('connections_phonecall_description')}
                    </div>
                    <div className={styles.input}>
                        <SwitchInputField
                            name="phonecallPhone"
                            label={i18next.t('connections_phonecall_phone_label')}
                            placeholder={i18next.t('connections_phonecall_phone_placeholder')}
                        />
                    </div>
                </div>
            }
        </div>
    )
}

const mapStateToProps = state => ({
    phonecallSwitchOn: state.global.phonecallSwitchOn,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const Phonecall = connect(mapStateToProps, mapDispatchToProps)(PhonecallPure)

