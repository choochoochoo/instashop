import React from 'react'
import styles from './Whatsappcall.css'
import {SvgIcon, Switch, SwitchInputField} from '../common'
import i18next from 'i18next';

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as actions from '../../actions';
import store from '../../store.js';

export const WhatsappcallPure = ({whatsappcallSwitchOn}) => {

    const switchClick = () => {
        if(whatsappcallSwitchOn) {
            store.dispatch(actions.switchOffWhatsappcall());
        }else{
            store.dispatch(actions.switchOnWhatsappcall());
        }
    }

    return (
        <div className={styles.root}>
            <div className={styles.header}>
                <div className={styles.leftCol}>
                    <div className={styles.icon}>
                        <SvgIcon
                            icon="whatsappcall"
                            className={styles.iconSvg}
                        />
                    </div>

                </div>
                <div className={styles.rightCol}>
                    <div className={styles.title}>
                        {i18next.t('connections_whatsappcall_title')}
                    </div>
                    <Switch click={switchClick} value={whatsappcallSwitchOn}/>
                </div>
            </div>
            {
                whatsappcallSwitchOn &&
                <div className={styles.content}>
                    <div className={styles.contentText}>
                        {i18next.t('connections_whatsappcall_description')}
                    </div>

                    <div className={styles.input}>
                        <SwitchInputField
                            name="whatsappcallPhone"
                            label={i18next.t('connections_whatsappcall_phone_label')}
                            placeholder={i18next.t('connections_whatsappcall_phone_placeholder')}
                        />
                    </div>

                    <div className={styles.input}>
                        <SwitchInputField
                            name="whatsappcallText"
                            label={i18next.t('connections_whatsappcall_text_label')}
                            placeholder={i18next.t('connections_whatsappcall_text_placeholder')}
                        />
                    </div>
                </div>
            }
        </div>
    )
}

const mapStateToProps = state => ({
    whatsappcallSwitchOn: state.global.whatsappcallSwitchOn,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const Whatsappcall = connect(mapStateToProps, mapDispatchToProps)(WhatsappcallPure)

