import React from 'react'
import { SvgIcon} from '../common'
import i18next from 'i18next'
import styles from './ArrowButton.css'

export const ArrowButton = ({
                                     title,
                                     click,
                                     border = false,
                                }) => {
    return (
        <button
            className={styles.root}
            onClick={click}
            type="button"
            style={{
                borderBottom: border ? 'solid 1px #d6dce3' : 'none',
                paddingBottom: border ? '20px' : '0',
            }}
        >
            <div className={styles.wrapper}>
                <div className={styles.leftCol}>
                    {title}
                </div>
                <div className={styles.rightCol}>
                    <SvgIcon icon="arrow"/>
                </div>
            </div>
        </button>
    )
}