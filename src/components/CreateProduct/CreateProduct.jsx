import React from 'react'
import InputMask from 'react-input-mask';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import {createProduct, editProduct, uploadFile, removeProduct} from '../../api';
import * as actions from '../../actions';
import store from '../../store.js';

import {CreateProductForm} from './CreateProductForm.jsx'
import i18next from "i18next";

import styles from './CreateProduct.css'
const _ = require('lodash');
import {profilable} from '../hocs'
import {getProduct} from '../../utils/data'
import {
    ClosePanel,
    ImageEditor,
    ChangePosition,
    CategorySelector,
    PhoneTemplate,
    ChangeImageMenu,
    ViewModeSwitch
} from '../common'
import * as notificaitonTypes from '../../constants/NotificationTypes'
import {
    getPreview,
    getPreviewForm,
    getOriginalPreview,
    getCrop,
    getSerializedCategories,
    getFullCategories,
    getSerializedPositions,
} from '../../utils/common'

import {ProductPure} from '../Product/Product.jsx'
import {ViewModePanel} from "./ViewModePanel.jsx";

export const CreateProductPure = ({
                                      match,
                                      history,

                                      userId,
                                      accountName,
                                      profile,
                                      newPositions,

                                      removeProductDialogOpened,
                                      openChangePositionPage,
                                      isOpenImageEditor,

                                      acceptedFileCrop,

                                      acceptedFile,
                                      acceptedFilePreview,
                                      acceptedFileName,

                                      pixelCropX,
                                      pixelCropY,
                                      pixelCropWidth,
                                      pixelCropHeight,

                                      cropX,
                                      cropY,
                                      cropWidth,
                                      cropHeight,

                                      croppedImageFilePreview,
                                      categories,
                                      checkedProductCategories,
                                      allProducts,
                                      profileSelectedCategory,
                                      profileSelectedCategoryTitle,
                                      categorySelectorDirty,
                                      categoriesProductsSwitchOn,
                                      openChangeImageMenu,
                                      viewModeEnabled,

                                        formTitle,
                                      formDescription,
                                      formPrice,
                                      appState,
                                      orderStatus,
                                 }) => {

    const productId = match.params.productId
    const product = getProduct(allProducts, productId)

    const photoOriginal = _.get(product, 'photo.original', null);
    const photoSize1080 = _.get(product, 'photo.size1080', null);
    const cropOriginal = _.get(product, 'photo.crop', null);

    let productCategories = _.get(product, 'categories', []);

    const isNew = !productId

    if(!productId && profileSelectedCategory !== 'all'){
        productCategories = [{
            _id: profileSelectedCategory,
            title: profileSelectedCategoryTitle,
        }]
    }

    if(categorySelectorDirty) {
        productCategories = checkedProductCategories.filter(item => item.checked === true)
    }

    const titleText = productId ?
        i18next.t('create_product_title_edit') :
        i18next.t('create_product_title')

    const handleRemove = () => {
        store.dispatch(actions.showProgress());
        store.dispatch(actions.productRemoveStart());

        removeProduct(userId, productId)
            .then(response => {
                store.dispatch(actions.productRemoveSuccess(response.data));
                store.dispatch(actions.hideProgress());
                history.push(`/${accountName}`)
                store.dispatch(actions.closeRemoveProductDialog());
                store.dispatch(actions.clearChangePosition());
                store.dispatch(actions.clearCategorySelector());

                store.dispatch(actions.showNotification(notificaitonTypes.REMOVE));
                setTimeout(()=>{
                    store.dispatch(actions.hideNotification());
                }, notificaitonTypes.TIMEOUT)

                store.dispatch(actions.closeRemoveProductDialog());
            })
            .catch(exception => {
                console.log(exception);
                store.dispatch(actions.productRemoveError(exception));
                store.dispatch(actions.hideProgress());

                store.dispatch(actions.showNotification(notificaitonTypes.ERROR));
                setTimeout(()=>{
                    store.dispatch(actions.hideNotification());
                }, notificaitonTypes.TIMEOUT)
            });
    }

    const handleSubmitEdit = (values) => {
        store.dispatch(actions.showProgress());
        store.dispatch(actions.productEditStart());

        editProduct(productId, {
            title: values.title,
            description: values.description,
            price: values.price,
            userId: userId,
            positions: getSerializedPositions(newPositions),
            categories: getSerializedCategories(productCategories),

            photoOriginal: photoOriginal,
            photoSize1080: photoSize1080,

            photo: acceptedFile,
            photoMetaFileName: acceptedFileName,

            photoMetaCropX: cropX,
            photoMetaCropY: cropY,
            photoMetaCropWidth: cropWidth,
            photoMetaCropHeight: cropHeight,

            photoMetaPixelCropX: pixelCropX,
            photoMetaPixelCropY: pixelCropY,
            photoMetaPixelCropWidth: pixelCropWidth,
            photoMetaPixelCropHeight: pixelCropHeight,
        }).then(response => {
            store.dispatch(actions.productEditSuccess(response.data));
            store.dispatch(actions.hideProgress());
            store.dispatch(actions.clearLoadedFile());
            store.dispatch(actions.clearChangePosition());
            store.dispatch(actions.clearCategorySelector());

            history.push(`/${accountName}`)

            store.dispatch(actions.showNotification(notificaitonTypes.SAVE));
            setTimeout(()=>{
                store.dispatch(actions.hideNotification());
            }, notificaitonTypes.TIMEOUT)

        }).catch(exception => {
            console.log(exception);
            store.dispatch(actions.productEditError(exception));
            store.dispatch(actions.hideProgress());

            store.dispatch(actions.showNotification(notificaitonTypes.ERROR));
            setTimeout(()=>{
                store.dispatch(actions.hideNotification());
            }, notificaitonTypes.TIMEOUT)
        });
    }

    const handleSubmitCreate = (values) => {
        store.dispatch(actions.showProgress());
        store.dispatch(actions.productCreateStart());

        createProduct({
            title: values.title,
            description: values.description,
            price: values.price,
            userId: userId,
            positions: getSerializedPositions(newPositions),
            categories: getSerializedCategories(productCategories),

            photo: acceptedFile,
            photoMetaFileName: acceptedFileName,

            photoMetaCropX: cropX,
            photoMetaCropY: cropY,
            photoMetaCropWidth: cropWidth,
            photoMetaCropHeight: cropHeight,

            photoMetaPixelCropX: pixelCropX,
            photoMetaPixelCropY: pixelCropY,
            photoMetaPixelCropWidth: pixelCropWidth,
            photoMetaPixelCropHeight: pixelCropHeight,
        }).then(response => {
             store.dispatch(actions.productCreateSuccess(response.data));
             store.dispatch(actions.hideProgress());
             history.push(`/${accountName}`)
             store.dispatch(actions.clearLoadedFile());
            store.dispatch(actions.clearChangePosition());
            store.dispatch(actions.clearCategorySelector());

            store.dispatch(actions.showNotification(notificaitonTypes.SAVE));
            setTimeout(()=>{
                store.dispatch(actions.hideNotification());
            }, notificaitonTypes.TIMEOUT)
        }).catch(exception => {
             store.dispatch(actions.productCreateError(exception));
             store.dispatch(actions.hideProgress());

            store.dispatch(actions.showNotification(notificaitonTypes.ERROR));
            setTimeout(()=>{
                store.dispatch(actions.hideNotification());
            }, notificaitonTypes.TIMEOUT)
        });
    }
    
    const handleAccepted = (file) => {
        store.dispatch(actions.openImageEditor(
            file.file,
            file.file.name,
            file.preview
        ));

        store.dispatch(actions.closeChangeImageMenu());

    }

    const handleClose = (photo) => {
        store.dispatch(actions.clearLoadedFile());
        store.dispatch(actions.clearChangePosition());
        store.dispatch(actions.clearCategorySelector());
    }

    const removeButtonClick = () => {
        store.dispatch(actions.openRemoveProductDialog());
    }

    const removeItemCancelClick = () => {
        store.dispatch(actions.closeRemoveProductDialog());
    }

    const changePositionClick = () => {

        if(!productId){

            store.dispatch(
                actions.openChangePositionPage(
                    true,
                    profileSelectedCategory,
                    allProducts,
                    'new',
                    getFullCategories(categories, productCategories),
                    {
                        title: 'Новый продукт',
                        _id: 'new',
                    },
                ),
            );

        }else{
            store.dispatch(
                actions.openChangePositionPage(
                    !productId,
                    profileSelectedCategory,
                    allProducts,
                    productId,
                    getFullCategories(categories, productCategories),
                    product,
                ),
            );
        }


    }

    const changeCategoryClick = () => {
        store.dispatch(actions.openCategorySelector(categories, productCategories));
    }

    const changeImageClick = () => {
        store.dispatch(actions.openChangeImageMenu());
    }

    const closeClick = () => {
        history.push(`/${accountName}`)
    }

    const imageEditorVisible = !openChangePositionPage && isOpenImageEditor
    const changePositionVisible = openChangePositionPage && !isOpenImageEditor
    const formVisible = !imageEditorVisible && !changePositionVisible

    let viewModeSaveClick = handleSubmitEdit.bind(null, {
        title: formTitle,
        description: formDescription,
        price: formPrice
    })

    if(isNew) {
        viewModeSaveClick = handleSubmitCreate.bind(null, {
            title: formTitle,
            description: formDescription,
            price: formPrice
        })
    }



    return (
        <PhoneTemplate>
            <div className={styles.root}>

                <ImageEditor visible={imageEditorVisible}/>

                <ChangePosition
                    visible={changePositionVisible}
                    productCategories={productCategories}
                    productId={productId ? productId : 'new'}
                />

                <ChangeImageMenu
                    visible={openChangeImageMenu}
                    photoAccepted={handleAccepted}
                    originalPreview={getOriginalPreview(photoOriginal, acceptedFilePreview)}
                    crop={getCrop(cropOriginal, acceptedFileCrop)}
                />

                <CategorySelector />

                <div className={styles.createProduct} style={{
                    display: formVisible ? 'block' : 'none' ,
                    visibility: formVisible ? 'visible' : 'hidden',
                }}>
                    <ViewModePanel
                        saveClick={viewModeSaveClick}
                        closeClick={closeClick}
                    />

                    <CreateProductForm
                        onSubmit={productId ? handleSubmitEdit : handleSubmitCreate}
                        productId={productId}
                        changePositionClick={changePositionClick}

                        photoAccepted={handleAccepted}
                        removeHandler={removeButtonClick}
                        removeProductDialogOpened={removeProductDialogOpened}
                        removeItemCancelClick={removeItemCancelClick}
                        removeItemOkClick={handleRemove}

                        // originalPreview={getOriginalPreview(photoOriginal, acceptedFilePreview)}
                        preview={getPreview(photoSize1080, croppedImageFilePreview)}
                        // crop={getCrop(cropOriginal, acceptedFileCrop)}

                        categories={productCategories}
                        changeCategoryClick={changeCategoryClick}
                        categoriesProductsSwitchOn={categoriesProductsSwitchOn}
                        changeImageClick={changeImageClick}
                        openChangeImageMenu={openChangeImageMenu}
                        visible={!viewModeEnabled}
                    />


                    {viewModeEnabled && <div>
                        <ProductPure
                            accountName={accountName}
                            formProduct={{
                                _id: productId,
                                title: formTitle,
                                description: formDescription,
                                price: formPrice,
                                photo: { size1080: getPreview(photoSize1080, croppedImageFilePreview) },
                                originalPhoto: photoSize1080,
                            }}
                            appState={appState}
                            orderStatus={orderStatus}
                            history={history}
                        />
                    </div>}
                </div>
            </div>
        </PhoneTemplate>
    )
}

const mapStateToProps = state => ({
    userId: state.profile._id,
    accountName: state.profile.accountName,
    profile: state.profile,
    newPositions: state.changePosition.newPositions,
    removeProductDialogOpened: state.global.removeProductDialogOpened,
    openChangePositionPage: state.changePosition.openChangePositionPage,
    imageEditorOpened: state.imageEditor.opened,


    // photoOriginal: _.get(state, 'profile.logo.original', null),
    // photoSize1080: _.get(state, 'profile.logo.size1080', null),
    //
    // cropOriginal: _.get(state, 'profile.logo.crop', null),
    acceptedFileCrop: state.global.crop,

    acceptedFile: state.global.acceptedFile,
    acceptedFilePreview: state.global.acceptedFilePreview,
    acceptedFileName: state.global.acceptedFileName,

    pixelCropX: _.get(state, 'global.pixelCrop.x'),
    pixelCropY: _.get(state, 'global.pixelCrop.y'),
    pixelCropWidth: _.get(state, 'global.pixelCrop.width'),
    pixelCropHeight: _.get(state, 'global.pixelCrop.height'),

    cropX: _.get(state, 'global.crop.x'),
    cropY: _.get(state, 'global.crop.y'),
    cropWidth: _.get(state, 'global.crop.width'),
    cropHeight: _.get(state, 'global.crop.height'),

    isOpenImageEditor: state.imageEditor.opened,
    croppedImageFilePreview: state.global.croppedImageFilePreview,

    categories: state.profile.categories,
    checkedProductCategories: state.categorySelector.categories,
    categorySelectorDirty: state.categorySelector.dirty,
    allProducts: state.profile.allProducts,
    profileSelectedCategory: state.profile.selectedCategory,
    profileSelectedCategoryTitle: state.profile.selectedCategoryTitle,
    categoriesProductsSwitchOn: state.profile.categoriesProductsSwitchOn,
    openChangeImageMenu: state.global.openChangeImageMenu,
    viewModeEnabled: state.global.viewModeEnabled,


    formTitle: _.get(state, 'form.CreateProductForm.values.title', null),
    formDescription: _.get(state, 'form.CreateProductForm.values.description', null),
    formPrice: _.get(state, 'form.CreateProductForm.values.price', null),
    appState: state,
    orderStatus: state.clientCart.status,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const CreateProduct = connect(mapStateToProps, mapDispatchToProps)(profilable(CreateProductPure))