import React from 'react'
import {Field, reduxForm} from 'redux-form'
import {connect} from "react-redux";

import {getProduct} from '../../utils/data'
import i18next from "i18next";

import {
    InputField,
    TextareaField,
    PhoneField,
    Button,
    SecondButton,
    ConfirmationDialog,
    ChangePositionButton, ChangeCategoryButton, ChangeImageMenu,
} from '../common'

import {ViewModePanel} from "./ViewModePanel.jsx";

import {ImageField} from './ImageField.jsx'



const validate = values => {
    const errors = {}

    // if (!values.photo) {
    //     errors.photo = 'Required'
    // }

    if (!values.title) {
        errors.title =  i18next.t('input_field_required')
    }

    //
    // if (!values.price) {
    //     errors.price = 'Required'
    // }

    return errors
}



let CreateProductFormPure = props => {
    const {
        handleSubmit,
        productId,
        position,
        changePositionClick,

        photoAccepted,
        removeHandler,
        removeItemOkClick,
        removeItemCancelClick,
        removeProductDialogOpened,

        originalPreview,
        preview,
        crop,

        changeCategoryClick,
        categories,
        categoriesProductsSwitchOn,
        changeImageClick,
        openChangeImageMenu,
        visible,
    } = props



    return (
        <form onSubmit={handleSubmit} encType="multipart/form-data" style={{
            opacity: visible ? 1 : 0,
            visibility: visible ? 'visible' : 'hidden',
            display: visible ? 'block' : 'none',
        }}>


            <ImageField
                preview={preview}
                positionClick={changePositionClick}
                imageClick={changeImageClick}
            />

            <InputField
                name="title"
                type="text"
                label={i18next.t('create_product_name_label')}
                placeholder={i18next.t('create_product_name_placeholder')}
                rows={3}
            />

            <TextareaField
                name="description"
                type="text"
                rows={3}
                label={i18next.t('create_product_description_label')}
                placeholder={i18next.t('create_product_description_placeholder')}
            />

            <InputField
                name="price"
                type="text"
                label={i18next.t('create_product_price_label')}
                placeholder={i18next.t('create_product_price_placeholder')}
            />

            {
                categoriesProductsSwitchOn &&
                <ChangeCategoryButton
                    label={i18next.t('change_category_button_label')}
                    placeholder={i18next.t('change_category_button_placeholder')}
                    click={changeCategoryClick}
                    categories={categories}
                />
            }

            {/*<ChangePositionButton*/}
                {/*label={i18next.t('change_position_button_label')}*/}
                {/*placeholder={i18next.t('change_position_button_placeholder')}*/}
                {/*click={changePositionClick}*/}
                {/*// position={position}*/}
            {/*/>*/}

            <div>
                <Button title={i18next.t('profile_submit_save')}/>
                {
                    productId &&
                    <SecondButton
                        title={i18next.t('create_product_remove_button')}
                        type="button"
                        onClick={removeHandler}
                    />
                }
            </div>

            <ConfirmationDialog
                text={i18next.t('create_new_product_remove_dialog_text')}
                okClick={removeItemOkClick}
                cancelClick={removeItemCancelClick}
                okTitle={i18next.t('create_new_product_remove_dialog_button_ok')}
                cancelTitle={i18next.t('create_new_product_remove_dialog_button_cancel')}
                opened={removeProductDialogOpened}
            />
        </form>
    )
}

CreateProductFormPure = reduxForm({
    form: 'CreateProductForm',
    validate,
    enableReinitialize: true,
})(CreateProductFormPure)

export const CreateProductForm = connect(
    (state, ownParam) => {
        return {
            initialValues: ownParam.productId ? getProduct(state.profile.allProducts, ownParam.productId): {
                title: '',
                description: '',
                price: '',
            }
        }
    }
)(CreateProductFormPure)




