import React from 'react'
import {Field} from 'redux-form'
import {FileUploadField, EditorButton} from '../common'
import i18next from 'i18next'
import {defaultProductImage} from '../../resources'

import styles from './ImageField.css'

import {ArrowButton} from './ArrowButton.jsx'

export const ImageField = ({
                               preview,
                               imageClick,
                               positionClick,
                            }) => {

    return (
        <div className={styles.root}>
            <div className={styles.leftCol}>
                <img
                    src={preview ? preview : `/${defaultProductImage}`}
                    width="100%"
                    className={styles.productLogo}
                />
            </div>
            <div className={styles.rightCol}>
                <ArrowButton
                    click={imageClick}
                    title={i18next.t('create_product_change_image')}
                    border={true}
                />
                <ArrowButton
                    click={positionClick}
                    title={i18next.t('create_product_change_position')}
                />
            </div>
        </div>
    )
}