import React from 'react'
import {Link} from 'react-router-dom';
import {Field, reduxForm} from 'redux-form'

import styles from './ViewModePanel.css'
import {SvgIcon, ViewModeSwitch, EditorButton} from "../common";
import {bindActionCreators} from "redux";
import * as actions from "../../actions";
import connect from "react-redux/es/connect/connect";
import i18next from 'i18next'

export const ViewModePanelPure = ({saveClick, viewModeEnabled, closeClick}) => {
    return (
        <div className={styles.root}>
            <div className={styles.leftCol}>
                <ViewModeSwitch />
            </div>
            <div className={styles.rightCol}>
                <div className={styles.close}>
                    {
                        !viewModeEnabled &&
                        <button onClick={closeClick}>
                            <SvgIcon
                                icon="close"
                                width={64}
                                height={64}
                            />
                        </button>
                    }

                    {
                        viewModeEnabled &&
                        <EditorButton
                            onClick={saveClick}
                            type="button"
                            text={i18next.t('profile_submit_save')}
                        />
                    }
                </div>
            </div>
        </div>
    )
}

const mapStateToProps = state => ({
    theme: state.theme,
    items: state.clientCart.items,
    viewModeEnabled: state.global.viewModeEnabled,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const ViewModePanel = connect(mapStateToProps, mapDispatchToProps)(ViewModePanelPure)
