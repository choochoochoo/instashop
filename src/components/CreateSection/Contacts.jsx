import React from 'react'
import styles from './Contacts.css'
import i18next from 'i18next';

import {
    Title,
    Subtitle,
    ClosePanel,
    InputField,
    TextEditorField,
    HiddenField,
    MapField,
    EditorButton, TextareaField,
} from '../common/index'

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import * as actions from '../../actions';
import store from '../../store.js';

import {MapButton} from './MapButton.jsx'


const FIELD_NAME = "additionalInfo"

export let ContactsPure = props => {

    const {
        textEditorText,
        coordinates,
        sectionId,
        openAddMapPage,
        userId,
        latitude,
        longitude,
    } = props


    const addMapClick = () => {
        store.dispatch(actions.openAddMapPage());

        store.dispatch({
            type: "@@redux-form/CHANGE",
            meta: {
                field: 'latitude',
                touch: false,
                form: 'MapForm'
            },
            payload: latitude
        })

        store.dispatch({
            type: "@@redux-form/CHANGE",
            meta: {
                field: 'longitude',
                touch: false,
                form: 'MapForm'
            },
            payload: longitude
        })
    }

    const hasMap = () => {
        return latitude && longitude
    }

    const buttons = (
        <EditorButton
            text={i18next.t('create_section_contacts_button_edit_map')}
            type="edit"
            className={styles.editCoordButton}
            onClick={addMapClick}
        />
    )

    return (
        <div>
            <InputField
                name="phone"
                label={i18next.t('create_section_contacts_phone_label')}
                placeholder={i18next.t('create_section_contacts_phone_placeholder')}
            />

            <InputField
                name="email"
                label={i18next.t('create_section_contacts_email_label')}
                placeholder={i18next.t('create_section_contacts_email_placeholder')}
            />

            <TextareaField
                name="additionalInfo"
                type="text"
                rows={6}
                label={i18next.t('create_section_contacts_additional_info_label')}
                placeholder={i18next.t('create_section_contacts_additional_info_placeholder')}
            />

            <InputField
                name="address"
                label={i18next.t('create_section_contacts_address_label')}
                placeholder={i18next.t('create_section_contacts_address_placeholder')}
            />

            <HiddenField
                name="latitude"
            />

            <HiddenField
                name="longitude"
            />

            {
                !hasMap() &&
                <MapButton
                    click={addMapClick}
                    latitude={latitude}
                    longitude={longitude}
                />
            }

            {
                hasMap() &&
                <MapField
                    label={i18next.t('create_section_map_map_field_label')}
                    placeholder={i18next.t('create_section_map_map_field_placeholder')}
                    latitude={latitude}
                    longitude={longitude}
                    hasMap={hasMap}
                    buttons={buttons}
                />
            }
        </div>
    )
}

const mapStateToProps = state => ({
    latitude: _.get(state, 'form.CreateSectionForm.values.latitude'),
    longitude: _.get(state, 'form.CreateSectionForm.values.longitude'),
    textEditorText: state.global.textEditorText,
    openAddMapPage: state.global.openAddMapPage,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const Contacts = connect(mapStateToProps, mapDispatchToProps)(ContactsPure)