import React from 'react'
import {Link} from 'react-router-dom';
import i18next from 'i18next';
import {Field, reduxForm} from 'redux-form'

import styles from './CoordinatesField.css'


const renderField = ({
                         input,
                         placeholder,
                         type,
                         accept,
                         meta: {touched, error, warning},
                         label,
                         rightBorder,
                     }) => (
    <div className={styles.inputWrapper}>
        <label className={styles.labelWrapper}>
            <span className={styles.label}>
                {label}
            </span>
            <span className={styles.inputWrapper2}>
                <input
                    {...input}
                    placeholder={placeholder}
                    type="text"
                    className={styles.input}
                    style={{
                        borderRight: rightBorder ? 'solid 1px #d6dce3': 'none'
                    }}
                />
            </span>
        </label>


        {touched &&
        ((error && <span>{error}</span>) ||
            (warning && <span>{warning}</span>))}
    </div>
)

export const CoordinatesField = ({}) => {
    return (
        <div className={styles.root}>
            <Field
                name="latitude"
                label={i18next.t('create_section_map_latitude_label')}
                placeholder={i18next.t('create_section_map_latitude_placeholder')}
                component={renderField}
                rightBorder={true}
            />
            <Field
                name="longitude"
                label={i18next.t('create_section_map_longitude_label')}
                placeholder={i18next.t('create_section_map_longitude_placeholder')}
                component={renderField}
            />
        </div>
    )
}