import React from 'react'
import styles from './CreateSection.css'
import i18next from 'i18next';
import {Title, Subtitle, ClosePanel, TextEditor, PhoneTemplate} from '../common/index'

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import {MapPage} from './MapPage.jsx'

import {createSection, getSection, editSection, removeSection} from '../../api';
import * as actions from '../../actions';
import store from '../../store.js';
import {profilable} from '../hocs'

import {CreateSectionForm} from './CreateSectionForm.jsx'

import {getElements} from './utils.js'
import * as notificaitonTypes from '../../constants/NotificationTypes'


const visible = (isVisible) => {
    return {
        display: isVisible ? 'block' : 'none',
        visibility: isVisible ? 'visible' : 'hidden',
    }
}

export const CreateSectionPure = ({
                                      match,
                                      userId,
                                      accountName,
                                      history,
                                      isOpenTextEditor,
                                      textEditorText,
                                      openAddMapPage,
                                      textEditorFieldName,
                                      removeSectionDialogOpened,
                                  }) => {

    const type = match.params.type
    const sectionId = match.params.sectionId
    let panelText = null;

    if(sectionId) {
        panelText =
            type === 'contacts' ?
                i18next.t('create_sections_contacts_panel_title_edit') :
                i18next.t('create_sections_section_panel_title_edit')
    }else{
        panelText =
            type === 'contacts' ?
                i18next.t('create_sections_contacts_panel_title') :
                i18next.t('create_sections_section_panel_title')
    }

    const close = () => {
        store.dispatch(actions.clearCreateSection());
    }

    const handleRemove = () => {
        store.dispatch(actions.showProgress());
        store.dispatch(actions.sectionRemoveStart());

        removeSection(sectionId, userId)
            .then(response => {
                store.dispatch(actions.sectionRemoveSuccess(response.data));
                store.dispatch(actions.hideProgress());
                history.push(`/${accountName}`)
                store.dispatch(actions.clearCreateSection());

                store.dispatch(actions.showNotification(notificaitonTypes.REMOVE));
                setTimeout(()=>{
                    store.dispatch(actions.hideNotification());
                }, notificaitonTypes.TIMEOUT)

                store.dispatch(actions.closeRemoveSectionDialog());

            })
            .catch(exception => {
                console.log(exception);
                store.dispatch(actions.sectionRemoveError(exception));
                store.dispatch(actions.hideProgress());

                store.dispatch(actions.showNotification(notificaitonTypes.ERROR));
                setTimeout(()=>{
                    store.dispatch(actions.hideNotification());
                }, notificaitonTypes.TIMEOUT)
            });
    }

    const handleSubmitCreate = (values) => {
        store.dispatch(actions.showProgress());
        store.dispatch(actions.sectionCreateStart());

        createSection({
             title: values.title,
             userId: userId,
             type: type,
             elements: values,
        }).then(response => {
            store.dispatch(actions.sectionCreateSuccess(response.data));
            store.dispatch(actions.hideProgress());
            store.dispatch(actions.clearCreateSection());
            history.push(`/${accountName}`)

            store.dispatch(actions.showNotification(notificaitonTypes.SAVE));
            setTimeout(()=>{
                store.dispatch(actions.hideNotification());
            }, notificaitonTypes.TIMEOUT)
        }).catch(exception => {
            store.dispatch(actions.sectionCreateError(exception));
            store.dispatch(actions.hideProgress());

            store.dispatch(actions.showNotification(notificaitonTypes.ERROR));
            setTimeout(()=>{
                store.dispatch(actions.hideNotification());
            }, notificaitonTypes.TIMEOUT)
        });
    }

    const handleSubmitEdit = (values) => {
        store.dispatch(actions.showProgress());
        store.dispatch(actions.sectionEditStart());

        editSection({
            sectionId,
            title: values.title,
            userId: userId,
            type: type,
            elements: values,
        }).then(response => {
            store.dispatch(actions.sectionEditSuccess(response.data));
            store.dispatch(actions.hideProgress());
            store.dispatch(actions.clearCreateSection());
            history.push(`/${accountName}`)

            store.dispatch(actions.showNotification(notificaitonTypes.SAVE));
            setTimeout(()=>{
                store.dispatch(actions.hideNotification());
            }, notificaitonTypes.TIMEOUT)
        }).catch(exception => {
            store.dispatch(actions.sectionEditError(exception));
            store.dispatch(actions.hideProgress());

            store.dispatch(actions.showNotification(notificaitonTypes.ERROR));
            setTimeout(()=>{
                store.dispatch(actions.hideNotification());
            }, notificaitonTypes.TIMEOUT)
        });
    }

    const closeTextEditor = () => {
        store.dispatch(actions.closeTextEditor());
    }

    const nextTextEditor = (text) => {
        store.dispatch(actions.nextTextEditor(text));

        store.dispatch({
            type: "@@redux-form/CHANGE",
            meta: {
                field: textEditorFieldName,
                touch: false,
                form: 'CreateSectionForm'
            },
            payload: text
        })
    }

    const removeButtonClick = () => {
        store.dispatch(actions.openRemoveSectionDialog());
    }

    const removeItemCancelClick = () => {
        store.dispatch(actions.closeRemoveSectionDialog());
    }

    return (
        <PhoneTemplate>
            <div className={styles.root}>
                <TextEditor
                    handleClose={closeTextEditor}
                    handleSave={nextTextEditor}
                    visible={isOpenTextEditor}
                    content={textEditorText}
                />

                <div style={visible(openAddMapPage)}>
                    <MapPage sectionId={sectionId} userId={accountName}/>
                </div>

                <div
                    className={styles.rootCreateSection}
                    style={visible(!isOpenTextEditor && !openAddMapPage)}
                >
                    <ClosePanel
                        text={panelText}
                        userId={accountName}
                        click={close}
                        border={false}
                    />
                    <CreateSectionForm
                        onSubmit={sectionId ? handleSubmitEdit : handleSubmitCreate}
                        type={type}
                        removeHandler={removeButtonClick}
                        sectionId={sectionId}
                        userId={userId}
                        removeSectionDialogOpened={removeSectionDialogOpened}
                        removeItemCancelClick={removeItemCancelClick}
                        removeItemOkClick={handleRemove}
                    />
                </div>
            </div>
        </PhoneTemplate>
    )
}

const mapStateToProps = state => ({
    userId: state.profile._id,
    accountName: state.profile.accountName,
    profile: state.profile,
    isOpenTextEditor: state.global.openTextEditor,
    textEditorText: state.global.textEditorText,
    openAddMapPage: state.global.openAddMapPage,
    textEditorFieldName: state.global.textEditorFieldName,
    removeSectionDialogOpened: state.global.removeSectionDialogOpened,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

class CreateSection2 extends React.Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {
        const sectionId = this.props.match.params.sectionId

        if(sectionId) {
            store.dispatch(actions.showProgress());
            store.dispatch(actions.sectionGetStart());

            getSection(sectionId).then(response => {
                store.dispatch(actions.sectionGetSuccess(response.data));
                store.dispatch(actions.hideProgress());
            }).catch(exception => {
                console.log(exception);
                store.dispatch(actions.sectionGetError(exception));
                store.dispatch(actions.hideProgress());
            });
        }
    }

    render() {
        return(
            <CreateSectionPure {...this.props}/>
        )
    }
}

export const CreateSection = connect(mapStateToProps, mapDispatchToProps)(profilable(CreateSection2))
