import React from 'react'
import {Field, reduxForm} from 'redux-form'
import {connect} from "react-redux";

import {getProduct} from '../../utils/data'
import i18next from "i18next";

import {
    InputField,
    TextareaField,
    PhoneField,
    Button,
    SecondButton,
    ConfirmationDialog,
} from '../common/index'

import {Contacts} from './Contacts.jsx'
import {Section} from './Section.jsx'

import styles from './CreateSection.css'

import {getFile} from '../../api'

import {elementsToForm} from './utils.js'

const validate = values => {
    const errors = {}
    const max_sym = 24

    if (!values.title) {
        errors.title =  i18next.t('input_field_required')
    }


    if (values.title && values.title.length > max_sym) {
        errors.title =  `${max_sym} ${i18next.t('input_field_max_length')}`
    }

    return errors
}

let CreateSectionFormPure = props => {
    const {
        handleSubmit,
        removeHandler,
        type = 'section',
        sectionId,
        elements,
        userId,
        removeItemOkClick,
        removeItemCancelClick,
        removeSectionDialogOpened,
    } = props

    const contacts = type === 'contacts'
    const section = type === 'section'

    return (
        <form onSubmit={handleSubmit}>

            { contacts && <Contacts sectionId={sectionId} userId={userId}/> }
            { section && <Section /> }
            <div>
                <Button title={i18next.t('create_section_submit_save')} className={styles.okButton}/>
                {
                    sectionId &&
                    <SecondButton
                        title={i18next.t('create_section_remove_button')}
                        type="button"
                        onClick={removeHandler}
                    />
                }
            </div>

            <ConfirmationDialog
                text={i18next.t('create_new_section_remove_dialog_text')}
                okClick={removeItemOkClick}
                cancelClick={removeItemCancelClick}
                okTitle={i18next.t('create_new_product_remove_dialog_button_ok')}
                cancelTitle={i18next.t('create_new_product_remove_dialog_button_cancel')}
                opened={removeSectionDialogOpened}
            />
        </form>
    )
}

CreateSectionFormPure = reduxForm({
    form: 'CreateSectionForm',
    validate,
    enableReinitialize: true,
})(CreateSectionFormPure)

export const CreateSectionForm = connect(
    (state, ownParam) => {
        return {
             initialValues: {
                 title: state.editSection.title,
                 ...state.editSection.elements,
             }
        }
    }
)(CreateSectionFormPure)
