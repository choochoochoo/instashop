import React from 'react'
import {Field, reduxForm} from 'redux-form'
import {connect} from "react-redux";
import sytles from './MapButton.css'
import i18next from "i18next";

import {
    SvgIcon,
} from '../common/index'


export let MapButton = props => {

    const {
        click,
    } = props

    return (
        <button className={sytles.button} type="button" onClick={click}>
            <div className={sytles.leftCol}>
                <div className={sytles.plus}>
                    +
                </div>
                <div className={sytles.text}>
                    {i18next.t('create_section_contacts_button_add_map')}
                </div>
            </div>
            <div className={sytles.icon}>
                <SvgIcon icon="mapMarker" />
            </div>
        </button>
    )
}


