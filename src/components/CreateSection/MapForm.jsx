import React from 'react'
import {Field, reduxForm} from 'redux-form'
import {connect} from "react-redux";

import {getProduct} from '../../utils/data'
import i18next from "i18next";

import {
    InputField,
    TextareaField,
    PhoneField,
    Button,
    SecondButton,
    MapField,
    EditorButton,
    ConfirmationDialog,
} from '../common/index'

import {Contacts} from './Contacts.jsx'
import {Section} from './Section.jsx'
import {CoordinatesField} from './CoordinatesField.jsx'

import styles from './MapForm.css'

const GET_COORD_LINK = 'https://gps-coordinates.org/'

const validate = values => {
    const errors = {}

    // if (!values.photo) {
    //     errors.photo = 'Required'
    // }

    // if (!values.title) {
    //     errors.title = 'Required'
    // }
    //
    // if (!values.price) {
    //     errors.price = 'Required'
    // }

    return errors
}

let MapFormPure = props => {
    const {
        handleSubmit,
        removeHandler,

        sectionId,
        elements,
        hasMap,
        latitude,
        longitude,

        removeItemOkClick,
        removeItemCancelClick,
        removeMapCoordinatesDialogOpened,
    } = props

    return (
        <form onSubmit={handleSubmit}>

            <MapField
                label={i18next.t('create_section_map_map_field_label')}
                placeholder={i18next.t('create_section_map_map_field_placeholder')}
                latitude={latitude}
                longitude={longitude}
                hasMap={hasMap}
            />

            <CoordinatesField />

            <div className={styles.text}>
                <div className={styles.comment}>
                    {i18next.t('create_section_map_comment')}
                </div>
                <a
                    href={GET_COORD_LINK}
                    target="_blank"
                    className={styles.link}
                >
                    {GET_COORD_LINK}
                </a>
            </div>

            <ConfirmationDialog
                text={i18next.t('create_new_section_map_coordinates_remove_dialog_text')}
                okClick={removeItemOkClick}
                cancelClick={removeItemCancelClick}
                okTitle={i18next.t('create_new_product_remove_dialog_button_ok')}
                cancelTitle={i18next.t('create_new_product_remove_dialog_button_cancel')}
                opened={removeMapCoordinatesDialogOpened}
            />

            <div>
                <Button title={i18next.t('create_section_submit_save')}/>
                {
                    hasMap &&
                    <SecondButton
                        title={i18next.t('create_section_remove_button')}
                        type="button"
                        onClick={removeHandler}
                    />
                }
            </div>
        </form>
    )
}

MapFormPure = reduxForm({
    form: 'MapForm',
    validate,
    enableReinitialize: true,
})(MapFormPure)

export const MapForm = connect(
    (state, ownParam) => {
        return {
            initialValues: {
                // latitude: state.editSection.latitude,
                // longitude: state.editSection.longitude,
            },
            latitude: _.get(state, 'form.MapForm.values.latitude'),
            longitude: _.get(state, 'form.MapForm.values.longitude'),
        }
    }
)(MapFormPure)
