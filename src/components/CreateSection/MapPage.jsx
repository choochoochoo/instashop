import React from 'react'
import styles from './CreateSection.css'
import i18next from 'i18next';
import {Title, Subtitle, ClosePanel, TextEditor} from '../common/index'

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import {createSection, getSection, editSection, removeSection} from '../../api';

import * as actions from '../../actions';
import store from '../../store.js';


import {MapForm} from './MapForm.jsx'

import * as notificaitonTypes from '../../constants/NotificationTypes'

export const MapPagePure = ({
                                userId,
                              history,
                              isOpenTextEditor,
                              textEditorText,
                              sectionId,
                              latitude,
                              longitude,
                                removeMapCoordinatesDialogOpened,
                                  }) => {

    const close = () => {
        store.dispatch(actions.closeAddMapPage());
    }


    const handleSubmit = (values) => {

        store.dispatch({
            type: "@@redux-form/CHANGE",
            meta: {
                field: 'latitude',
                touch: false,
                form: 'CreateSectionForm'
            },
            payload: values.latitude
        })

        store.dispatch({
            type: "@@redux-form/CHANGE",
            meta: {
                field: 'longitude',
                touch: false,
                form: 'CreateSectionForm'
            },
            payload: values.longitude
        })

        close()
    }

    const handleRemove = (values) => {
        store.dispatch({
            type: "@@redux-form/CHANGE",
            meta: {
                field: 'latitude',
                touch: false,
                form: 'CreateSectionForm'
            },
            payload: ''
        })

        store.dispatch({
            type: "@@redux-form/CHANGE",
            meta: {
                field: 'longitude',
                touch: false,
                form: 'CreateSectionForm'
            },
            payload: ''
        })

        store.dispatch(actions.showNotification(notificaitonTypes.REMOVE));
        setTimeout(()=>{
            store.dispatch(actions.hideNotification());
        }, notificaitonTypes.TIMEOUT)

        close()

        store.dispatch(actions.closeRemoveMapCoordinatesDialog());
    }

    const hasMap = () => {
        return latitude && longitude
    }


    const removeButtonClick = () => {
        store.dispatch(actions.openRemoveMapCoordinatesDialog());
    }

    const removeItemCancelClick = () => {
        store.dispatch(actions.closeRemoveMapCoordinatesDialog());
    }

    return (
        <div className={styles.root}>
            <ClosePanel
                text={
                    sectionId ?
                        i18next.t('create_section_map_edit_panel_text') :
                        i18next.t('create_section_map_add_panel_text')
                }
                userId={userId}
                click={close}
                isLink={false}
                border={false}
            />
            <MapForm
                onSubmit={handleSubmit}
                removeHandler={removeButtonClick}
                hasMap={hasMap()}
                removeMapCoordinatesDialogOpened={removeMapCoordinatesDialogOpened}
                removeItemCancelClick={removeItemCancelClick}
                removeItemOkClick={handleRemove}
            />
        </div>

    )
}

const mapStateToProps = state => ({
    latitude: _.get(state, 'form.CreateSectionForm.values.latitude'),
    longitude: _.get(state, 'form.CreateSectionForm.values.longitude'),
    removeMapCoordinatesDialogOpened: state.global.removeMapCoordinatesDialogOpened,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const MapPage = connect(mapStateToProps, mapDispatchToProps)(MapPagePure)

