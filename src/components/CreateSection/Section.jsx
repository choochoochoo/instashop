import React from 'react'
import styles from './Section.css'
import i18next from 'i18next';
import {
    Title,
    Subtitle,
    ClosePanel,
    Button,
    InputField,
    TextEditorField, TextareaField,
} from '../common'

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import * as actions from '../../actions';
import store from '../../store.js';

const FIELD_NAME = "text"

export let SectionPure = props => {

    // const {
    //     textEditorText,
    // } = props

    // const openTextEditor = () => {
    //     store.dispatch(actions.openTextEditor(textEditorText, FIELD_NAME));
    // }

    return (
        <div>
            <InputField
                name="title"
                label={i18next.t('create_section_title_label')}
                placeholder={i18next.t('create_section_title_placeholder')}
            />


            <TextareaField
                name="text"
                type="text"
                rows={10}
                label={i18next.t('create_section_section_text_label')}
                placeholder={i18next.t('create_section_section_text_placeholder')}
            />

        </div>
    )
}


const mapStateToProps = state => ({
    textEditorText: state.global.textEditorText,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const Section = connect(mapStateToProps, mapDispatchToProps)(SectionPure)