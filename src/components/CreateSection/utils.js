// export const getElements = (values) => {
//     const elements = []
//
//     Object.keys(values).map(key => {
//         if (key !== 'title') {
//             let item = values[key]
//
//             Object.keys(item).map(keyElement => {
//
//                 let element = item[keyElement]
//
//                 elements.push({
//                     ...element,
//                     type: key,
//                     fieldId: keyElement,
//                 })
//             })
//         }
//     })
//
//     return elements
// }

import _ from 'lodash'

export const getElements = (values) => {
    const elements = []

    Object.keys(values).map(key => {
        if (key !== 'title') {
            let value = values[key]
            let path = key.split('__')

            let idx = path[0]
            let type = path[1]
            let fieldName = path[2]

            _.set(elements, `[${idx}].type`, type);
            _.set(elements, `[${idx}].${fieldName}`, value);
            _.set(elements, `[${idx}].fieldId`, idx);
        }
    })


    return elements
}

export const elementsToForm = (elements) => {

    const newState = { }

    if(elements) {

        elements.forEach((item) => {
            Object.keys(item).map(key => {
                if (key !== 'title' && key !== 'type') {
                    newState[`${item.fieldId}__${item.type}__${key}`] = item[key]
                }
            })
        })
    }

    return newState
}