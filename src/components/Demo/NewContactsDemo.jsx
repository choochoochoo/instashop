import React from 'react'
import {Field, reduxForm} from 'redux-form'
import styles from './NewContactsDemo.css'
import i18next from "i18next";

import {NewContactsItem} from './NewContactsItem.jsx'

import {
    ClosePanel,
    Button,
    PhoneTemplate,
} from '../common/index'

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';


import * as actions from '../../actions';
import {editUserUni} from '../../api';
import store from '../../store.js';
import {profilable} from '../hocs'

import {circle} from '../../resources'


export let NewContactsDemoPure = props => {

    const {
        match,
        userId,
        accountName,
        history,
        click,
        profile,
    } = props


    const iconStyle = {
        backgroundImage: `url(/${circle})`,
        backgroundRepeat: 'no-repeat',
    }

    const changeUser = () => {
        store.dispatch(actions.showProgress());
        store.dispatch(actions.userEditStart());

        editUserUni(userId, {
            hasSeenContactsDemoPage: true,
        }, profile).then(response => {
            store.dispatch(actions.userEditSuccess(response.data));
            store.dispatch(actions.hideProgress());

            history.replace(`/${accountName}/sections/create/contacts`)
        }).catch(exception => {
            console.log('Произошла ошибка на сервере');
            console.log(exception);
            store.dispatch(actions.userEditError(exception));
            store.dispatch(actions.hideProgress());
        });
    }

    const nextClick = () => {
        changeUser()
    }

    return (
        <PhoneTemplate>
            <div className={styles.root} style={iconStyle}>
                <ClosePanel
                    text={i18next.t('create_contacts_demo_title')}
                    userId={accountName}
                    click={close}
                    type="white"
                    border={false}
                />

                <div className={styles.description}>
                    {i18next.t('create_contacts_demo_description')}
                </div>

                <div className={styles.items}>
                    <NewContactsItem
                        text={i18next.t('create_contacts_demo_location_text')}
                        icon="contactsMap"
                    />

                    <NewContactsItem
                        text={i18next.t('create_contacts_demo_location_phones_text')}
                        icon="contactsPhone"
                    />

                    <NewContactsItem
                        text={i18next.t('create_contacts_demo_location_additional_info')}
                        icon="contactsInfo"
                    />
                </div>

                <Button
                    title={i18next.t('create_contacts_demo_location_button_next')}
                    onClick={nextClick}
                    type="white"
                />
            </div>
        </PhoneTemplate>
    )
}


const mapStateToProps = state => ({
    profile: state.profile,
    userId: state.profile._id,
    accountName: state.profile.accountName,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch),
})

export const NewContactsDemo = connect(mapStateToProps, mapDispatchToProps)(profilable(NewContactsDemoPure))