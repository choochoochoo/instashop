import React from 'react'
import {Link} from 'react-router-dom';
import {Field, reduxForm} from 'redux-form'
import {SvgIcon} from '../common'

import styles from './NewContactsItem.css'

export const NewContactsItem = ({text, icon}) => {
    return (
        <div className={styles.root}>
            <div className={styles.icon}>
                <SvgIcon icon={icon} />
            </div>

            <div className={styles.text}>
                {text}
            </div>
        </div>
    )
}



