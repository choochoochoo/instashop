import React from 'react'
import {Link} from 'react-router-dom';
import {Field, reduxForm} from 'redux-form'

import styles from './NewSectionButton.css'

export const NewSectionButton = ({title}) => {
    return (
        <div className={styles.root}>
            <button type="button" className={styles.button}>
                <span className={styles.buttonText}>{title}</span>
             </button>
        </div>
    )
}



