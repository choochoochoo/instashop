import React from 'react'
import {Field, reduxForm} from 'redux-form'
import styles from './NewSectionDemo.css'
import i18next from "i18next";

import {NewSectionButton} from './NewSectionButton.jsx'

import {
    ClosePanel,
    Button,
    PhoneTemplate,
} from '../common/index'

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';


import * as actions from '../../actions';
import {editUserUni} from '../../api';
import store from '../../store.js';
import {profilable} from '../hocs'

import {circle} from '../../resources'


export let NewSectionDemoPure = props => {

    const {
        match,
        userId,
        accountName,
        history,
        click,
        profile,
    } = props


    const iconStyle = {
        backgroundImage: `url(/${circle})`,
        backgroundRepeat: 'no-repeat',
    }

    const changeUser = () => {
        store.dispatch(actions.showProgress());
        store.dispatch(actions.userEditStart());

        editUserUni(userId, {
            hasSeenSectionDemoPage: true,
        }, profile).then(response => {
            store.dispatch(actions.userEditSuccess(response.data));
            store.dispatch(actions.hideProgress());

            history.replace(`/${accountName}/sections/create/section`)
        }).catch(exception => {
            console.log('Произошла ошибка на сервере');
            console.log(exception);
            store.dispatch(actions.userEditError(exception));
            store.dispatch(actions.hideProgress());
        });
    }

    const nextClick = () => {
        changeUser()
    }

    return (
        <PhoneTemplate>
            <div className={styles.root} style={iconStyle}>
                <ClosePanel
                    text={i18next.t('create_section_demo_title')}
                    userId={accountName}
                    click={close}
                    type="white"
                    border={false}
                />

                <div className={styles.description}>
                    {i18next.t('create_section_demo_description')}
                </div>

                <div className={styles.buttons}>
                    <NewSectionButton
                        title={i18next.t('create_section_demo_button_how_to_find_us')}
                    />
                    <NewSectionButton
                        title={i18next.t('create_section_demo_button_about_me')}
                    />
                    <NewSectionButton
                        title={i18next.t('create_section_demo_button_something_else')}
                    />
                </div>

                <div className={styles.notice}>
                    {i18next.t('create_section_demo_notice')}
                </div>

                <Button
                    title={i18next.t('create_section_demo_button_next')}
                    onClick={nextClick}
                    type="white"
                />
            </div>
        </PhoneTemplate>
    )
}

const mapStateToProps = state => ({
    profile: state.profile,
    userId: state.profile._id,
    accountName: state.profile.accountName,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch),
})

export const NewSectionDemo = connect(mapStateToProps, mapDispatchToProps)(profilable(NewSectionDemoPure))