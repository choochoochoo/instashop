import React from 'react'
import styles from './Dialog.css'

export const Dialog = ({title, children, opened, handleCancel}) => {
    return (
        <div className={styles.modal} style={{
            opacity: opened ? 1 : 0,
            visibility: opened ? 'visible' : 'hidden'
        }}>
            <div className={styles.modalDialog}>
                <div className={styles.modalContent}>
                    <div className={styles.modalHeader}>
                        <h3 className={styles.modalTitle}>{title}</h3>
                        <button onClick={handleCancel} className={styles.close}>×</button>
                    </div>
                    <div className={styles.modalBody}>
                        {children}
                    </div>
                </div>
            </div>
        </div>
    )
}