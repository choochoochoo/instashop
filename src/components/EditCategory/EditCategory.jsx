import React from 'react'
import i18next from 'i18next';
import {ClosePanel, ChangePositionCategory, PhoneTemplate} from '../common'
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import styles from './EditCategory.css'
import * as actions from '../../actions'
import store from '../../store.js'
import {profilable} from '../hocs'

import {EditCategoryForm} from './EditCategoryForm.jsx'
import {editCategory, removeCategory} from "../../api";
import * as notificaitonTypes from "../../constants/NotificationTypes";
import {getCategory, getProduct} from "../../utils/data";
import {getOldPosition} from "../../utils/common";

export const EditCategoryPure = ({
                                     match,
                                     history,
                                     accountName,
                                     userId,
                                     removeCategoryDialogOpened,
                                     openChangePositionPage,
                                     categories,
                                     newPosition,
                                }) => {

    const categoryId = match.params.categoryId

    const category = getCategory(categories, categoryId)


    let position = getOldPosition(categories, categoryId)
    if(newPosition){
        position = newPosition
    }

    const handleSubmitEdit = (values) => {

        store.dispatch(actions.showProgress());
        store.dispatch(actions.categoryEditStart());

        editCategory(categoryId, {
            title: values.title,
            position: position,
            userId: userId,
        }).then(response => {
            store.dispatch(actions.categoryEditSuccess(response.data));
            store.dispatch(actions.hideProgress());
            store.dispatch(actions.clearChangePositionCategory());
            history.push(`/${accountName}/categories`)

            store.dispatch(actions.showNotification(notificaitonTypes.SAVE));
            setTimeout(()=>{
                store.dispatch(actions.hideNotification());
            }, notificaitonTypes.TIMEOUT)
        }).catch(exception => {
            store.dispatch(actions.categoryEditError(exception));
            store.dispatch(actions.hideProgress());

            store.dispatch(actions.showNotification(notificaitonTypes.ERROR));
            setTimeout(()=>{
                store.dispatch(actions.hideNotification());
            }, notificaitonTypes.TIMEOUT)
        });
    }

    const removeButtonClick = () => {
        store.dispatch(actions.openRemoveCategoryDialog());
    }

    const removeItemCancelClick = () => {
        store.dispatch(actions.closeRemoveCategoryDialog());
    }

    const handleRemove = () => {
        store.dispatch(actions.showProgress());
        store.dispatch(actions.categoryRemoveStart());

        removeCategory(categoryId, userId)
            .then(response => {
                store.dispatch(actions.categoryRemoveSuccess(response.data));
                store.dispatch(actions.hideProgress());
                history.push(`/${accountName}/categories`)
                store.dispatch(actions.closeRemoveCategoryDialog());
                //store.dispatch(actions.clearChangePosition());

                store.dispatch(actions.showNotification(notificaitonTypes.REMOVE));
                setTimeout(()=>{
                    store.dispatch(actions.hideNotification());
                }, notificaitonTypes.TIMEOUT)

                store.dispatch(actions.closeRemoveCategoryDialog());
            })
            .catch(exception => {
                console.log(exception);
                store.dispatch(actions.categoryRemoveError(exception));
                store.dispatch(actions.hideProgress());

                store.dispatch(actions.showNotification(notificaitonTypes.ERROR));
                setTimeout(()=>{
                    store.dispatch(actions.hideNotification());
                }, notificaitonTypes.TIMEOUT)
            });
    }

    const changePositionClick = () => {
        store.dispatch(actions.openChangePositionCategoryPage(categories, categoryId));
    }

    return (
        <PhoneTemplate>
            <div className={styles.root}>
                <ChangePositionCategory visible={openChangePositionPage} itemId={categoryId}/>

                <div className={styles.editCategory} style={{
                    display: !openChangePositionPage ? 'block' : 'none' ,
                    visibility: !openChangePositionPage ? 'visible' : 'hidden',
                }}>
                    <ClosePanel
                        text={i18next.t('edit_category_title')}
                        userId={accountName}
                        link={`/${accountName}/categories`}
                        border={false}
                    />

                    <EditCategoryForm
                        onSubmit={handleSubmitEdit}
                        categoryId={categoryId}

                        removeHandler={removeButtonClick}
                        removeCategoryDialogOpened={removeCategoryDialogOpened}
                        removeItemCancelClick={removeItemCancelClick}
                        removeItemOkClick={handleRemove}

                        changePositionClick={changePositionClick}
                        position={position}
                    />
                </div>
            </div>
        </PhoneTemplate>
    )
}

const mapStateToProps = state => ({
    userId: state.profile._id,
    accountName: state.profile.accountName,
    profile: state.profile,
    removeCategoryDialogOpened: state.global.removeCategoryDialogOpened,
    newPosition: state.changePositionCategory.selectedPosition,
    openChangePositionPage: state.changePositionCategory.openChangePositionPage,

    categories: state.profile.categories,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const EditCategory = connect(mapStateToProps, mapDispatchToProps)(profilable(EditCategoryPure))