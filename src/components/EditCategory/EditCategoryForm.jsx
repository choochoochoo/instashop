import React from 'react'
import {Field, reduxForm} from 'redux-form'
import {connect} from "react-redux";
import i18next from "i18next";
import {getCategory} from '../../utils/data'


import {
    Button,
    ChangePositionButton,
    ConfirmationDialog,
    SecondButton,
    InputField,
} from '../common'


const validate = values => {
    const errors = {}
    const max_sym = 20

    if (!values.title) {
        errors.title =  i18next.t('input_field_required')
    }


    if (values.title && values.title.length > max_sym) {
        errors.title =  `${max_sym} ${i18next.t('input_field_max_length')}`
    }

    return errors
}



let EditCategoryFormPure = props => {
    const {
        handleSubmit,
        removeHandler,
        removeItemOkClick,
        removeItemCancelClick,
        removeCategoryDialogOpened,

        position,
        changePositionClick,
    } = props

    return (
        <div>
            <form onSubmit={handleSubmit}>
                <InputField
                    name="title"
                    type="text"
                    label={i18next.t('edit_product_title_label')}
                    placeholder={i18next.t('edit_product_title_placeholder')}
                />

                <ChangePositionButton
                    label={i18next.t('category_change_position_button_label')}
                    click={changePositionClick}
                    placeholder={position}
                />

                <div>
                    <Button title={i18next.t('profile_submit_save')}/>
                    {
                        <SecondButton
                            title={i18next.t('edit_category_remove_button')}
                            type="button"
                             onClick={removeHandler}
                        />
                    }
                </div>

                <ConfirmationDialog
                    text={i18next.t('create_new_product_remove_dialog_text')}
                    okClick={removeItemOkClick}
                    cancelClick={removeItemCancelClick}
                    okTitle={i18next.t('create_new_product_remove_dialog_button_ok')}
                    cancelTitle={i18next.t('create_new_product_remove_dialog_button_cancel')}
                    opened={removeCategoryDialogOpened}
                />
            </form>
        </div>
    )
}

EditCategoryFormPure = reduxForm({
    form: 'ConnectionsForm',
    validate,
    enableReinitialize: true,
})(EditCategoryFormPure)

export const EditCategoryForm = connect(
    (state, ownParam) => {


        return {
            initialValues: ownParam.categoryId ? getCategory(state.profile.categories, ownParam.categoryId): {
                title: '',
            }
        }
    }
)(EditCategoryFormPure)
