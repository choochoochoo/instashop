import React from 'react';
import i18next from 'i18next';

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import * as actions from '../../actions';
import store from '../../store.js';
import {getAuth} from '../../api';
import {ProgressDialog} from '../ProgressDialog'
const classNames = require('classnames');

import {Notification} from '../common'



import {getLanguage} from '../../utils/common'

import {
    EDITING_PHOTO,
    CROPING_PHOTO,
} from '../../constants/CreateProductSteps.js';


import styles from './GlobalApp.css'

export class GlobalAppPure extends React.Component {

    componentWillMount() {

         const lng = getLanguage();

        i18next.init({
            lng: lng,
            resources: require(`../../languages/${lng}.json`)
        });

        store.dispatch(actions.showProgress());
        store.dispatch(actions.userGetAuthStart());

        getAuth().then(response => {
            store.dispatch(actions.userGetAuthSuccess(response.data));
            store.dispatch(actions.hideProgress());
        }).catch(exception => {
            console.log(exception);
            store.dispatch(actions.userGetAuthError(exception));
            store.dispatch(actions.hideProgress());
        });
    }

    componentWillReceiveProps(nextProps){
        if(this.props.language !== nextProps.language){
            i18next.init({
                lng: nextProps.language,
                resources: require(`../../languages/${nextProps.language}.json`)
            });
        }
    }

    render() {
        const step = this.props.step
        const progressFormOpen = this.props.progressFormOpen
        const openTextEditor = this.props.openTextEditor
        const notificationType = this.props.notificationType
        const notificationText = this.props.notificationText
        const openCategorySelector = this.props.openCategorySelector
        const theme = this.props.theme

        const styleobj = { }
        styleobj[`${styles.cropEdit}`] = step === CROPING_PHOTO
        styleobj[`${styles.textEdit}`] = openTextEditor
        styleobj[`${styles.blocked}`] = progressFormOpen
        styleobj[`${styles.ubuntuFont}`] = theme.common_font === 'Ubuntu'
        styleobj[`${styles.montserratFont}`] = theme.common_font === 'Montserrat'
        styleobj[`${styles.hideScroll}`] = openCategorySelector
        styleobj[`${styles.hideScroll}`] = openCategorySelector

        return (
            <div className={classNames(styleobj)}>
                <ProgressDialog/>

                <Notification
                    text={notificationText}
                    type={notificationType}
                    visible={!!notificationType}
                />

                {this.props.children}
            </div>
        )
    }
}

const mapStateToProps = state => ({
    language: state.profile.language,
    profile: state.profile,
    step: state.global.stepCreateProduct,
    progressFormOpen: state.global.progressFormOpen,
    openTextEditor: state.global.openTextEditor,
    notificationType: state.global.notificationType,
    notificationText: state.global.notificationText,
    theme: state.theme,
    openCategorySelector: state.global.openCategorySelector,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const GlobalApp = connect(mapStateToProps, mapDispatchToProps)(GlobalAppPure)
