import React from 'react'
import {Link} from 'react-router-dom';
import i18next from 'i18next';

import styles from './Header.css'


import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import * as actions from '../../actions';
import store from '../../store.js';
import {pencil} from '../../resources'
import {getFile} from '../../api'

const classNames = require('classnames');
import {Sections} from './Sections.jsx'
import {Logo} from '../common'

import {circleCloud, circleNavyBlue} from '../../resources'

export const HeaderPure = ({
                               logo,
                               title,
                               description,
                               userId,
                               accountName,
                               loginedUser,
                               editMode,
                               sections,
                               viewModeEnabled,
                               contactsSection,
                               hasSeenSectionDemoPage,
                               hasSeenContactsDemoPage,
                               theme,
                           }) => {

    const circles = {
        'navy-blue': circleNavyBlue,
        'cloud': circleCloud,
    }

    const themeCircle = circles[theme.key]

    const iconStyleBack = {
        // backgroundColor: backgroundColor,
        // color: textColor,
        backgroundImage: `url(/${themeCircle})`,
        backgroundRepeat: 'no-repeat',
        backgroundPositionX: '100px',
        backgroundPositionY: '-150px',
    }

    const myAccount = loginedUser._id === userId
    const myAccAndNotView = myAccount && !viewModeEnabled


    const link = myAccAndNotView ?
        `/${accountName}/profile` : `/${accountName}/sections/${contactsSection}`

    const icon = `${logo ? getFile(logo.size1080) : pencil}`

    const companyName = title ? title : i18next.t('header_company_name_placeholder')
    const companyDescription = description ? description : i18next.t('header_company_description_placeholder')

    const profileContent = (
        <div className={styles.content}>
            <div className={styles.contentText}>
                <div className={styles.title} style={{
                    color: theme['main_header-title_text-color']
                }}>
                    {companyName}
                </div>
                <div className={styles.description} style={{
                    color: theme['main_header-description_text-color']
                }}>
                    {companyDescription}
                </div>
            </div>
            <div className={styles.ava}>

                <Logo
                    icon={icon}
                />

            </div>
        </div>
    )

    return (
        <div
            className={classNames(styles.root, styles[`theme-${theme.key}`])}
            style={themeCircle ? iconStyleBack : null}
        >
            {
                ( myAccAndNotView || contactsSection ) && (
                    <Link to={link} className={styles.link}>
                        { profileContent }
                    </Link>
                )
            }

            {
                !contactsSection && !myAccAndNotView && profileContent
            }

            <Sections
                sections={sections}
                loginedUser={loginedUser}
                userId={userId}
                accountName={accountName}
                viewModeEnabled={viewModeEnabled}
                hasContacts={!!contactsSection}
                hasSeenSectionDemoPage={hasSeenSectionDemoPage}
                hasSeenContactsDemoPage={hasSeenContactsDemoPage}
                theme={theme}
            />
        </div>
    )
}


const mapStateToProps = state => ({
    loginedUser: state.global.loginedUser,
    userId: state.profile._id,
    accountName: state.profile.accountName,
    sections: state.profile.sections,
    viewModeEnabled: state.global.viewModeEnabled,
    contactsSection: state.profile.contactsSection,
    hasSeenSectionDemoPage: state.profile.hasSeenSectionDemoPage,
    hasSeenContactsDemoPage: state.profile.hasSeenContactsDemoPage,
    theme: state.theme
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const Header = connect(mapStateToProps, mapDispatchToProps)(HeaderPure)