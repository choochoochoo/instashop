import React from 'react'
import {Link} from 'react-router-dom'

import styles from './SectionCreateItem.css'
import i18next from "i18next";

export const SectionCreateItem = ({
                                  title,
                                  theme,
                            }) => {
    return (
        <button className={styles.root} style={{
            borderRadius: theme['main_header-section-button_border-radius'],
        }}>
            <span className={styles.text}>
                {title}
            </span>
        </button>
    )
}