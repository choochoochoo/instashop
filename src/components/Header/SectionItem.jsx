import React from 'react'
import {Link} from 'react-router-dom'

import styles from './SectionItem.css'

export const SectionItem = ({
                                title,
                                theme,
                            }) => {
    return (
        <button className={styles.root} style={{
            backgroundColor: theme['main_header-section-button_background-color'],
            borderRadius: theme['main_header-section-button_border-radius'],
        }}>
            <span className={styles.text} style={{
                color: theme['main_header-section-button_text-color'],
                fontWeight: theme['main_header-section-button_text-weight'],
                fontSize: theme['main_header-section-button_text-size'],
            }}>
                {title}
            </span>
        </button>
    )
}