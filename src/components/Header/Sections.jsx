import React from 'react'
import {Link} from 'react-router-dom'

import styles from './Sections.css'

import {SectionCreateItem} from './SectionCreateItem.jsx'
import {SectionItem} from './SectionItem.jsx'
import i18next from "i18next";
import {getSectionTitle} from '../../utils/common'

export const Sections = ({
                             sections,
                             loginedUser,
                             userId,
                             accountName,
                             viewModeEnabled,
                             oppositeBackgroundColor,
                             oppositeTextColor,
                             hasContacts,
                             hasSeenSectionDemoPage,
                             hasSeenContactsDemoPage,
                             theme,
                        }) => {

    const myAccount = loginedUser._id === userId
    const myAccAndNotView = myAccount && !viewModeEnabled

    const newSectionItemLink =
        hasSeenSectionDemoPage ?
            `/${accountName}/sections/create/section` :
            `/${accountName}/demo/section`

    const newContactsItemLink =
        hasSeenContactsDemoPage ?
            `/${accountName}/sections/create/contacts` :
            `/${accountName}/demo/contacts`

    return (
        <div className={styles.root}>
            {
                sections.map((item) => {
                    const link = `/${accountName}/sections/${item._id}`

                    return (
                        <Link to={link} key={item._id} className={styles.link}>
                            <SectionItem
                                key={item._id}
                                title={getSectionTitle(item.title, item.type)}
                                theme={theme}
                            />
                        </Link>
                    )
                })
            }

            {
                myAccAndNotView &&
                (
                    <Link to={newSectionItemLink} className={styles.link}>
                        <SectionCreateItem
                            type="section"
                            title={i18next.t('create_sections_section_button_text')}
                            theme={theme}
                        />
                    </Link>
                )
            }



            {
                myAccAndNotView && !hasContacts &&
                (
                    <Link to={newContactsItemLink} className={styles.link}>
                        <SectionCreateItem
                            type="contacts"
                            title={i18next.t('create_sections_contacts_button_text')}
                            theme={theme}
                        />
                    </Link>
                )
            }
        </div>
    )
}