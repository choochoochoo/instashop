import ReactDOM from 'react-dom';
import React, { Component } from 'react';
import Clipboard from 'react-clipboard.js';
import styles from './ClipboardButton.css'

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import * as actions from '../../actions';
import store from '../../store.js';

import * as notificaitonTypes from '../../constants/NotificationTypes'

import i18next from "i18next";

export class ClipboardButtonPure extends Component {
    constructor() {
        super();

        this.onSuccess = this.onSuccess.bind(this);
        this.getText = this.getText.bind(this);
    }

    onSuccess() {
        store.dispatch(actions.showNotification(notificaitonTypes.SAVE, i18next.t('clipboard_button_press_text')));
        setTimeout(()=>{
            store.dispatch(actions.hideNotification());
        }, notificaitonTypes.TIMEOUT)
    }

    getText() {
        return this.props.data;
    }

    render() {
        return (
            <Clipboard
                option-text={this.getText}
                onSuccess={this.onSuccess}
                className={styles.button}
            >
                <span className={styles.text}>
                    {this.props.text}
                </span>
            </Clipboard>
        );
    }
}

const mapStateToProps = state => ({

})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const ClipboardButton = connect(mapStateToProps, mapDispatchToProps)(ClipboardButtonPure)