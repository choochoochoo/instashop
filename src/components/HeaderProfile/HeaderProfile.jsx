import React from 'react'
import {Link} from 'react-router-dom';
import i18next from 'i18next';

import styles from './HeaderProfile.css'
import {companyLogo} from '../../resources'

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import * as actions from '../../actions';
import store from '../../store.js';
import {getFile} from '../../api'
import {StatusLink, SvgIcon, ViewModeSwitch} from '../common'

import {ClipboardButton} from './ClipboardButton.jsx'

export const HeaderProfilePure = ({
                                      logo,
                                      userId,
                                      loginedUser,
                                      history,
                                      viewModeEnabled,
                                      newOrders,
                                      accountName,
                                  }) => {


    const menuClick = () => {
        store.dispatch(actions.openMenu());
    }





    const myAccount = (loginedUser && loginedUser._id === userId)

    if(!myAccount) return <div/>

    return (
          <div className={styles.root}>
            <div className={styles.content}>
                <div className={styles.leftCol}>
                    <div className={styles.button}>
                        <ViewModeSwitch />
                    </div>

                    <div className={styles.newOrders}>
                        <StatusLink />
                    </div>
                </div>
                <div className={styles.rightCol}>
                    {
                        viewModeEnabled ?
                            <ClipboardButton
                                text={i18next.t('link_project_text')}
                                data={`${location.host}/${accountName}`}
                            /> :
                            <button className={styles.buttonMenu} onClick={menuClick}>
                                <span className={styles.textMenu}>
                                    {i18next.t('menu')}
                                </span>
                                <span>
                                    <SvgIcon icon="menu" width={64} height={64}/>
                                </span>
                            </button>
                    }
                </div>
            </div>
        </div>
    )
}


const mapStateToProps = state => ({
    newOrders: state.profile.newOrders,
    accountName: state.profile.accountName,
    userId: state.profile._id,
    loginedUser: state.global.loginedUser,
    viewModeEnabled: state.global.viewModeEnabled,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const HeaderProfile = connect(mapStateToProps, mapDispatchToProps)(HeaderProfilePure)