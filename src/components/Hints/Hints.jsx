import React from 'react'
import styles from './Hints.css'
import i18next from 'i18next';
import {Title, Subtitle, ClosePanel, SvgIcon, PhoneTemplate} from '../common'

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import * as actions from '../../actions';
import store from '../../store.js';
import {profilable} from '../hocs'


export const HintsPure = ({match, history, accountName}) => {
    const userId = match.params.userId


    return (
        <PhoneTemplate>
            <div className={styles.root}>
                <ClosePanel
                    text={i18next.t('hints_menu')}
                    userId={accountName}
                />


            </div>
        </PhoneTemplate>
    )
}


const mapStateToProps = state => ({
    userId: state.profile._id,
    accountName: state.profile.accountName,
    profile: state.profile,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const Hints = connect(mapStateToProps, mapDispatchToProps)(profilable(HintsPure))