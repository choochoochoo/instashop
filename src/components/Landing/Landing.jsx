import React from 'react'
import InputMask from 'react-input-mask';

import {Link} from 'react-router-dom';
import styles from './Landing.css'
import i18next from 'i18next';
import {SvgIcon, LinkButton} from '../common'
import {landingPhone, logo} from '../../resources'
import names from '../../constants/names.json'

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as actions from '../../actions';
import store from '../../store.js';

import {authorizable} from '../hocs/'

import {Whatsappcall} from "../Product/Connection.jsx";



export const LandingPure = ({}) => {
    const iconStyle = {
        backgroundImage: `url(${landingPhone})`,
    }

    return (
        <div className={styles.root}>

            {/*<div className={styles.single}>*/}
                {/*<Whatsappcall*/}
                    {/*phone={'+79152312046'}*/}
                    {/*text={'Привет'}*/}
                    {/*backColor={true}*/}
                {/*/>*/}
            {/*</div>*/}

            <div className={styles.rootPhone} >

                <div className={styles.header}>
                    <div className={styles.linkCol}>
                        <span className={styles.headerLinkWrapper}>
                            <a
                                href={names.brand_link}
                                className={styles.headerLink}
                            >
                                {names.brand_name}
                            </a>
                        </span>
                    </div>
                    <div className={styles.logoCol}>
                        <SvgIcon
                            icon="logo"
                            width={64}
                            height={64}
                            className={styles.logo}
                        />
                    </div>
                </div>

                <div className={styles.title}>
                    {i18next.t('landing_title')}
                </div>

                <div className={styles.buttonPanel}>
                    <LinkButton
                        title={i18next.t('landing_register_button')}
                        link={`/register`}
                        type="landing-register"
                        className={styles.registerButton}
                    />

                    <LinkButton
                        title={i18next.t('landing_login_button')}
                        link={`/login`}
                        type="landing-login"
                    />
                </div>

                <div className={styles.description1Wrapper}>
                    <div className={styles.description1}>
                        {i18next.t('landing_description_1')}
                    </div>
                </div>

                <div className={styles.imageBlock}>
                    <img src={landingPhone} className={styles.imagePhone}/>
                </div>

                <div className={styles.description2Block}>
                    <div className={styles.description2Wrapper}>
                        <span className={styles.linkYourCompany}>
                            flicky.io/your-company
                        </span>
                        <div className={styles.description2}>
                            {i18next.t('landing_description_2')}
                        </div>
                    </div>

                    <div className={styles.description3Wrapper}>
                        <div className={styles.description3}>
                            {i18next.t('landing_description_3_1')}

                            <Link to={`/register`} >
                                <span className={styles.registerDescriptionLink}>
                                    {i18next.t('landing_description_3_2')}
                                </span>
                            </Link>&nbsp;

                            {i18next.t('landing_description_3_3')}
                        </div>
                    </div>


                    <div className={styles.bottomLogoWrapper}>
                        <div className={styles.bottomLogo}>
                            <div className={styles.bottomLogoText}>
                                {names.brand_name}
                            </div>
                        </div>
                    </div>

                    <div className={styles.contactsBlock}>
                        <div className={styles.contact}>
                            <a
                                className={styles.contactTitle}
                                href={names.facebook_community_link}
                                target="_blank"
                            >
                                {names.facebook_community_name}
                            </a>
                            <div className={styles.contactDescription}>
                                {i18next.t('langing_community_description')}
                            </div>
                        </div>

                        <div className={styles.contact} style={{
                            marginTop: '20px'
                        }}>
                            <a
                                className={styles.contactTitle}
                                href={`mailto:${names.official_mail}`}
                                target="_blank"
                            >
                                {names.official_mail}
                            </a>
                            <div className={styles.contactDescription}>
                                {i18next.t('langing_mail_description')}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className={styles.rootDesktop}>
                <div className={styles.rootDesktopContent}>
                    <div className={styles.rootDesktopContent2}>
                        <div className={styles.header}>
                            <div className={styles.linkCol}>
                            <span className={styles.headerLinkWrapper}>
                                <a
                                    href={names.brand_link}
                                    className={styles.headerLink}
                                >
                                    {names.brand_name}
                                </a>
                            </span>
                            </div>
                            <div className={styles.logoCol}>
                                <SvgIcon
                                    icon="logo"
                                    width={64}
                                    height={64}
                                    className={styles.logo}
                                />
                            </div>
                        </div>
                        <div className={styles.desktopContent}>
                            <div className={styles.desktopLeftCol}>
                                <div className={styles.desktopTitle}>
                                    {i18next.t('landing_title')}
                                </div>

                                <div className={styles.desktopBttonPanel}>
                                    <LinkButton
                                        title={i18next.t('landing_register_button')}
                                        link={`/register`}
                                        type="landing-register"
                                        className={styles.registerButton}
                                    />

                                    <LinkButton
                                        title={i18next.t('landing_login_button')}
                                        link={`/login`}
                                        type="landing-login"
                                    />
                                </div>

                                <div className={styles.description1Wrapper}>
                                    <div className={styles.description1}>
                                        {i18next.t('landing_description_1')}
                                    </div>
                                </div>

                                <div className={styles.desktopDescription2Block}>
                                    <div className={styles.description2Wrapper}>
                                        <span className={styles.linkYourCompany}>
                                            flicky.io/your-company
                                        </span>
                                        <div className={styles.description2}>
                                            {i18next.t('landing_description_2')}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className={styles.desktopRightCol}>
                                <div className={styles.imageBlock}>
                                    <img src={landingPhone} className={styles.imagePhone}/>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div className={styles.desktopFooter}>
                    <div className={styles.desktopFooterContent}>
                        <div className={styles.desktopFooterContent2}>
                            <div className={styles.desktopFooterContent3}>

                                <div className={styles.description3Wrapper}>
                                    <div className={styles.description3}>
                                        {i18next.t('landing_description_3_1')}

                                        <Link to={`/register`} >
                                            <span className={styles.registerDescriptionLink}>
                                                {i18next.t('landing_description_3_2')}
                                            </span>
                                        </Link> &nbsp;

                                        {i18next.t('landing_description_3_3')}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className={styles.desktopFooter2ContentWrapper}>
                        <div className={styles.desktopFooter2Content}>

                            <div className={styles.bottomLogoWrapper}>
                                <div className={styles.bottomLogo}>
                                    <div className={styles.bottomLogoText}>
                                        {names.brand_name}
                                    </div>
                                </div>
                            </div>

                            <div className={styles.desktopContactsBlock}>
                                <div className={styles.desktopContactsBlock2}>
                                    <div className={styles.desktopContact}>
                                        <a
                                            className={styles.contactTitle}
                                            href={names.facebook_community_link}
                                            target="_blank"
                                        >
                                            {names.facebook_community_name}
                                        </a>
                                        <div className={styles.contactDescription}>
                                            {i18next.t('langing_community_description')}
                                        </div>
                                    </div>

                                    <div className={styles.desktopContact}>
                                        <a
                                            className={styles.contactTitle}
                                            href={`mailto:${names.official_mail}`}
                                            target="_blank"
                                        >
                                            {names.official_mail}
                                        </a>
                                        <div className={styles.contactDescription}>
                                            {i18next.t('langing_mail_description')}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    )
}

const mapStateToProps = state => ({
    loginedUser: state.global.loginedUser,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch),
})

export const Landing = connect(mapStateToProps, mapDispatchToProps)(authorizable(LandingPure))
