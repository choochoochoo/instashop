import React from 'react'
import InputMask from 'react-input-mask';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';

import {loginUser} from '../../api';
import * as actions from '../../actions';
import store from '../../store.js';

import {LoginForm} from './LoginForm.jsx'
import styles from './Login.css'
import {ClosePanel, PhoneTemplate, InstagrammLoginButton} from '../common'
import i18next from "i18next";

import {authorizable} from '../hocs/'

import _ from 'lodash'

export const LoginPure = ({loginCode, history}) => {

    const handleSubmit = (values) => {
        store.dispatch(actions.showProgress());
        store.dispatch(actions.userLoginStart());

        loginUser({
            login: _.trim(values.login.toLowerCase()),
            password: _.trim(values.password),
        }).then(response => {
            if(response.data.user){
                store.dispatch(actions.userLoginSuccess(response.data.user));
                history.push(`/${response.data.user.accountName}`)
            }else{
                store.dispatch(actions.userLoginNotSuccess(response.data.code));
            }

            store.dispatch(actions.hideProgress());

        }).catch(exception => {
            store.dispatch(actions.userLoginError(exception));
            store.dispatch(actions.hideProgress());
        });
    }

    const click = () => {
        history.push(`/`)
    }

    return (
        <PhoneTemplate>
            <div className={styles.root}>
                <ClosePanel
                    text={i18next.t('login_title')}
                    border={false}
                    isLink={false}
                    click={click}
                />

                {/*<InstagrammLoginButton title={i18next.t('instagramm_button_title_login')}/>*/}

                {/*<div className={styles.orPanel}>*/}
                    {/*<div className={styles.orPanelText}>*/}
                        {/*{i18next.t('register_title_or')}*/}
                    {/*</div>*/}
                {/*</div>*/}

                <LoginForm
                    onSubmit={handleSubmit}
                    loginCode={loginCode}
                />

            </div>
        </PhoneTemplate>
    )
}


const mapStateToProps = state => ({
    loginedUser: state.global.loginedUser,
    loginCode: state.global.loginCode,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const Login = connect(mapStateToProps, mapDispatchToProps)(authorizable(LoginPure))