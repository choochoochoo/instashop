import React from 'react'
import {Field, reduxForm} from 'redux-form'
import i18next from "i18next";
import {SwitchInputField, Button, ErrorMessage} from '../common'
import styles from './Login.css'

const validate = values => {
    const errors = {}

    if (!values.login) {
        errors.login = i18next.t('input_field_required')
    }

    if (!values.password) {
        errors.password = i18next.t('input_field_required')
    }

    return errors
}


let LoginFormPure = props => {
    const {handleSubmit, handleCancel, loginCode} = props
    return (
        <form onSubmit={handleSubmit} className={styles.formWrapper}>
            <SwitchInputField
                name="login"
                type="text"
                label={i18next.t('login_login_label')}
                placeholder={i18next.t('login_login_placeholder')}
            />

            <SwitchInputField
                name="password"
                type="password"
                label={i18next.t('login_password_label')}
                placeholder={i18next.t('login_password_placeholder')}
            />

            <div className={styles.buttonWrapper}>
                <ErrorMessage
                    text={i18next.t(`login_error_${loginCode}`)}
                    visible={!!loginCode}
                />
                <Button
                    title={i18next.t('login_button')}
                    className={styles.loginButton}
                    typeStyle="white"
                />
            </div>
        </form>
    )
}

export const LoginForm = reduxForm({
    form: 'LoginForm',
    validate
})(LoginFormPure)

