import React from 'react'
import {Link} from 'react-router-dom';
import i18next from 'i18next';

import styles from './CloseButton.css'
import {companyLogo} from '../../resources'

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import * as actions from '../../actions';
import store from '../../store.js';
import {getFile} from '../../api'
import {SvgIcon} from '../common'

const closeSvgs = {
    'navy-blue': 'closeSvgNavyBlue',
    'cloud': 'closeSvgCloud',
    'steve-jobs': 'closeSvgSteveJobs',
}

export const CloseButton = ({theme}) => {
    const menuClick = () => {
        store.dispatch(actions.closeMenu());
    }

    const closeStyle = styles[closeSvgs[theme.key]]

    return (
        <div className={styles.closeButtonWrapper}>
            <button className={styles.buttonMenu} onClick={menuClick}>
                <span className={styles.textMenu}>
                    {i18next.t('menu')}
                </span>
                <span>
                    <SvgIcon icon="closeMenu" width={64} height={64} className={closeStyle}/>
                </span>
            </button>
        </div>
    )
}
