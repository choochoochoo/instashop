import React from 'react'
import {Link} from 'react-router-dom';
import i18next from 'i18next';

import styles from './Menu.css'
import {companyLogo} from '../../resources'

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import * as actions from '../../actions';
import store from '../../store.js';
import {logout} from '../../api'
import {StatusLink} from '../common'
const classNames = require('classnames');

import {CloseButton} from './CloseButton.jsx'
import {SupportPanel} from './SupportPanel.jsx'
import {MenuItem} from './MenuItem.jsx'

export const MenuPure = ({opened, loginedUser, history, selectedTheme, theme, settingsOrdersSwitchOn}) => {
    const itemClick = () => {
        store.dispatch(actions.closeMenu());
    }

    const logoutButtonClick = () => {
        store.dispatch(actions.showProgress());
        store.dispatch(actions.userLogoutStart());

        logout()
            .then(response => {
                store.dispatch(actions.userLogoutSuccess(response.data));
                store.dispatch(actions.hideProgress());
                store.dispatch(actions.closeMenu());
                history.push(`/`)
            })
            .catch(exception => {
                console.log(exception);
                store.dispatch(actions.userLogoutError(exception));
                store.dispatch(actions.hideProgress());
                store.dispatch(actions.closeMenu());
            });
    }

    const closeClick = () => {
        store.dispatch(actions.closeMenu());
    }

    const styleobj = { }
    styleobj[`${styles.rootVisible}`] = opened
    styleobj[`${styles.root}`] = true

    const orderLink = settingsOrdersSwitchOn
        ? `/${loginedUser.accountName}/orders` :
        `/${loginedUser.accountName}/settings/orders`

    return (
        <div className={classNames(styleobj)}>
            <button className={styles.back} onClick={closeClick}/>
            <div className={styles.panel} style={{
                backgroundColor: theme['menu_background-color']
            }}>
                <div className={styles.panelInner} style={{
                    color: theme['menu_item_text-color']
                }}>
                    <CloseButton theme={theme}/>

                    <MenuItem
                        click={itemClick}
                        link={orderLink}
                        text={i18next.t('settings_orders')}
                        label={<StatusLink />}
                    />

                    <MenuItem
                        click={itemClick}
                        link={`/${loginedUser.accountName}/profile`}
                        text={i18next.t('edit_profile')}
                    />

                    <MenuItem
                        click={itemClick}
                        link={`/${loginedUser.accountName}/settings`}
                        text={i18next.t('settings_menu')}
                    />

                    {/*<MenuItem*/}
                        {/*click={itemClick}*/}
                        {/*link={`/${loginedUser.accountName}/hints`}*/}
                        {/*text={i18next.t('hints_menu')}*/}
                    {/*/>*/}

                    <MenuItem
                        click={itemClick}
                        link={`/${loginedUser.accountName}/qrcode`}
                        text={i18next.t('qrcode_link')}
                    />

                    {/*<SupportPanel theme={theme} />*/}

                    <div className={styles.logoutWrapper}>
                        <button onClick={logoutButtonClick} className={styles.logoutButton}>
                            {i18next.t('logout')}
                        </button>
                    </div>
                </div>
            </div>
        </div>
    )
}

const mapStateToProps = state => ({
    opened: state.global.menuOpened,
    loginedUser: state.global.loginedUser,
    settingsOrdersSwitchOn: state.profile.settingsOrdersSwitchOn,
    theme: state.theme,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const Menu = connect(mapStateToProps, mapDispatchToProps)(MenuPure)
