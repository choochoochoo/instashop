import React from 'react'
import {Link} from 'react-router-dom';
import i18next from 'i18next';

import styles from './MenuItem.css'

export const MenuItem = ({link, text, click, label}) => {
    return (
        <div className={styles.menuItemButtonWrapper}>
            <div className={styles.leftCol}>
                {label}
            </div>
            <button onClick={click} className={styles.menuItemButton}>
                <Link to={link} className={styles.menuItem}>
                    {text}
                </Link>
            </button>
        </div>
    )
}
