import React from 'react'
import {Link} from 'react-router-dom';
import i18next from 'i18next';

import styles from './SupportPanel.css'
import {companyLogo} from '../../resources'

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import * as actions from '../../actions';
import store from '../../store.js';
import {getFile} from '../../api'
import {SvgIcon} from '../common'

export const SupportPanel = ({theme}) => {
    return (
        <div className={styles.rootWrapper}>
            <div className={styles.root} style={{
                borderColor: theme['menu_divider_color']
            }}>
                <div className={styles.supportText}>
                    {i18next.t('support_text')}
                </div>
                <div className={styles.supportLinkWrapper}>
                    <a
                        href={i18next.t('support_link_full')}
                        target="_blank"
                        className={styles.supportLink}
                    >
                        {i18next.t('support_link')}
                    </a>
                </div>
            </div>
        </div>
    )
}
