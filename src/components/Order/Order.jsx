import React from 'react'
import styles from './Order.css'
import i18next from 'i18next';
import {SvgIcon, Button, WhatsappLink, ClosePanel, Status, CartItemsPanel, PhoneLink, PhoneTemplate} from '../common'

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import * as actions from '../../actions';
import store from '../../store.js';
import {profilable, orderable} from '../hocs'
import {editOrder, getOrder} from "../../api";
import {getNumber, sendErrorToAnaliticsExc} from "../../utils/common";

import {OrdersSwitch} from './OrdersSwitch.jsx'
import * as notificaitonTypes from "../../constants/NotificationTypes";

const saveOrder = (orderId, newStatus) => {
    store.dispatch(actions.showProgress());
    store.dispatch(actions.orderEditStart());

    editOrder(orderId, {
        status: newStatus,
    }).then(response => {
        store.dispatch(actions.orderEditSuccess(response.data));
        store.dispatch(actions.hideProgress());

        store.dispatch(actions.showNotification(notificaitonTypes.SAVE));
        setTimeout(()=>{
            store.dispatch(actions.hideNotification());
        }, notificaitonTypes.TIMEOUT)
    }).catch(exception => {
        sendErrorToAnaliticsExc(exception)

        console.log(exception);
        store.dispatch(actions.orderEditError(exception));
        store.dispatch(actions.hideProgress());

        store.dispatch(actions.showNotification(notificaitonTypes.ERROR));

        setTimeout(()=>{
            store.dispatch(actions.hideNotification());
        }, notificaitonTypes.TIMEOUT)
    });
}

export const OrderPure = ({
                              match,
                              history,
                              accountName,
                              profile,
                              userId,
                              orderId,
                              phone,
                              comment,
                              orderUserId,
                              createDate,
                              number,
                              status,
                              products,
                          }) => {



    const switchClick = () => {
        const newStatus = status === 'accept' ? 'read' : 'accept'
        saveOrder(orderId, newStatus)
    }

    const closeClick = () => {
        if(status === 'new'){
            saveOrder(orderId, 'read')
        }
    }

    const titlePanel = (
        <div className={styles.titlePanel}>
            <span className={styles.number}>{getNumber(number)}</span>
            <Status
                text={i18next.t(`order_status_${status}`)}
                status={status}
            />
        </div>
    )

    return (
        <PhoneTemplate>
            <div className={styles.root}>
                <ClosePanel
                    userId={accountName}
                    link={`/${accountName}/orders`}
                    border={true}
                    titlePanel={titlePanel}
                    click={closeClick}
                />

                <OrdersSwitch
                    switchClick={switchClick}
                    switchOnOrderAccept={status === 'accept'}
                />

                <div className={styles.clientData}>
                    <div className={styles.phonePanel}>
                        <div className={styles.phoneLabel}>{i18next.t('order_phone_label')}</div>
                        <div className={styles.phone}>{phone}</div>
                    </div>

                    <div className={styles.commentPanel}>
                        <div className={styles.comment}>{comment}</div>
                    </div>
                </div>

                <div className={styles.cartItemsPanel}>
                    <CartItemsPanel items={products}/>
                </div>

                <PhoneLink phone={phone}>
                    <Button
                        title={i18next.t('order_call_button_title')}
                        type="button"
                        typeStyle="white"
                    />
                </PhoneLink>
            </div>
        </PhoneTemplate>
    )
}


const mapStateToProps = state => ({
    userId: state.profile._id,
    accountName: state.profile.accountName,
    profile: state.profile,
    orderId: state.order._id,
    phone: state.order.phone,
    comment: state.order.comment,
    orderUserId: state.order.userId,
    createDate: state.order.createDate,
    number: state.order.number,
    status: state.order.status,
    products: state.order.products,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

class Order2 extends React.Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {
        const orderId = this.props.match.params.orderId

        store.dispatch(actions.showProgress());
        store.dispatch(actions.orderGetStart());

        getOrder(orderId).then(response => {
            store.dispatch(actions.orderGetSuccess(response.data));
            store.dispatch(actions.hideProgress());
        }).catch(exception => {
            console.log(exception);
            store.dispatch(actions.orderGetError(exception));
            store.dispatch(actions.hideProgress());
        });

    }

    render() {
        return(
            <OrderPure {...this.props}/>
        )
    }
}

export const Order = connect(mapStateToProps, mapDispatchToProps)(profilable(Order2))
