import React from 'react'
import styles from './OrdersSwitch.css'
import {Button, SvgIcon, Switch, SwitchTextareaField} from '../common'
import i18next from 'i18next';

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as actions from '../../actions';
import store from '../../store.js';

export const OrdersSwitchPure = ({switchOnOrderAccept, switchClick}) => {
    return (
        <div className={styles.root}>
            <div className={styles.header}>
                <div className={styles.leftCol}>
                    <div className={styles.title}>
                        {i18next.t('order_switch_accept_label')}
                    </div>
                </div>
                <div className={styles.rightCol}>
                    <Switch click={switchClick} value={switchOnOrderAccept}/>
                </div>
            </div>
        </div>
    )
}

const mapStateToProps = state => ({
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const OrdersSwitch = connect(mapStateToProps, mapDispatchToProps)(OrdersSwitchPure)

