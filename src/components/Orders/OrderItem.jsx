import React, {PropTypes} from 'react'
import styles from './OrderItem.css'
import {SvgIcon, PhoneLink, Status} from '../common'
import moment from 'moment'
import i18next from 'i18next'
import {getNumber} from "../../utils/common";

export const OrderItem = ({
                                order,
                                click,
                            }) => {

    const getTitle = (products) => {
        if(!products) return

        if(products.length === 1){
            return products[0].title
        }else{
            return `${products.length} ${i18next.t('order_item_product_count', {count: products.length})}`
        }
    }

    return (
        <button className={styles.button} onClick={click}>
            <div className={styles.leftCol}>
                <div className={styles.numberWrapper}>
                    <span className={styles.number}>{getNumber(order.number)}</span>
                    <Status
                        text={i18next.t(`order_status_${order.status}`)}
                        status={order.status}
                    />
                </div>
                <div className={styles.title}>
                    {getTitle(order.products)}
                </div>
                <div className={styles.phone}>
                    {/*<PhoneLink*/}
                        {/*phone={order.phone}*/}
                    {/*>*/}
                        {/*<span className={styles.phoneWrapper}>*/}
                            {/*<span className={styles.phone}>{order.phone}</span>*/}
                        {/*</span>*/}
                    {/*</PhoneLink>*/}
                    <span className={styles.phone}>{order.comment}</span>

                </div>
            </div>
            <div className={styles.rightCol}>
                <div className={styles.date}>
                    {moment(order.createDate).format('DD.MM.YYYY')}
                </div>
                <div className={styles.arrow}>
                    <SvgIcon icon="arrow"/>
                </div>
            </div>



        </button>
    )
}
