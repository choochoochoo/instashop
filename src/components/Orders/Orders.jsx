import React from 'react'
import styles from './Orders.css'
import i18next from 'i18next';
import {SvgIcon, Button, WhatsappLink, ClosePanel, SecondButton, PhoneTemplate} from '../common'

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import * as actions from '../../actions';
import store from '../../store.js';
import {profilable} from '../hocs'
import {OrderItem} from "./OrderItem.jsx";
import {getUserOrders} from "../../api";

export const OrdersPure = ({
                                            match,
                                            history,
                                            accountName,
                                            profile,
                                            userId,
                                            orders,
                                            skip,
                                            limit,
                                            count,
                                        }) => {

    const loadMore = () => {
        store.dispatch(actions.showProgress());
        store.dispatch(actions.ordersGetStart());

        getUserOrders(userId, orders.length, limit).then(orderResponse => {

            store.dispatch(actions.ordersGetSuccess(orderResponse.data));
            store.dispatch(actions.hideProgress());

        }).catch(exception => {
            console.log(exception);
            store.dispatch(actions.ordersGetError(exception));
            store.dispatch(actions.hideProgress());
        });
    }

    return (
        <PhoneTemplate>
            <div className={styles.root}>
                <ClosePanel
                    text={i18next.t('settings_orders')}
                    userId={accountName}
                    link={`/${accountName}`}
                    border={false}
                />

                {
                    orders.length === 0 &&
                        <div className={styles.noOrders}>
                            {i18next.t('orders_no_orders_message')}
                        </div>
                }

                <div className={styles.ordersPanel}>
                {
                    orders.map((item, index) => {

                        const itemClick = () => {
                            history.replace(`/${accountName}/orders/${item._id}`)
                        }

                        return (
                            <OrderItem
                                key={item._id}
                                order={item}
                                click={itemClick}
                            />
                        )
                    })
                }
                </div>

                {
                    orders.length < count &&
                    <SecondButton
                        onClick={loadMore}
                        title={i18next.t('orders_load_more_button_title')}
                    />
                }
            </div>
        </PhoneTemplate>
    )
}


const mapStateToProps = state => ({
    userId: state.profile._id,
    accountName: state.profile.accountName,
    profile: state.profile,
    orders: state.orders.items,
    skip: state.orders.skip,
    limit: state.orders.limit,
    count: state.orders.count,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const Orders = connect(mapStateToProps, mapDispatchToProps)(profilable(OrdersPure))