import React from 'react'
import styles from './Connection.css'

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as actions from '../../actions';
import store from '../../store.js';

import {SvgIcon, WhatsappLink, PhoneLink} from '../common'


import i18next from 'i18next'

export const Phonecall = ({phone, description, backColor}) => {
    return (
        <PhoneLink
            phone={phone}
            className={styles.link}
        >
            <div className={backColor ? styles.wrapper : ''}>
                <SvgIcon icon="phonecall" className={styles.icon}/>
            </div>
            {
                description && (
                    <div className={styles.phonecallTitle}>
                        {description}
                    </div>
                )
            }
        </PhoneLink>
    )
}

export const Whatsappcall = ({phone, text, description, backColor}) => {
    return (
            <WhatsappLink
                phone={phone}
                text={text}
                className={styles.link}
            >
                <div className={backColor ? styles.wrapper : ''}>
                    <SvgIcon icon="whatsappcall" className={styles.icon}/>
                </div>
                {
                    description && (
                        <div className={styles.phonecallTitle}>
                            {description}
                        </div>
                    )
                }
            </WhatsappLink>
    )
}

export const Circle = ({phoneNumber, whatsappNumber, whatsappText, closeClick}) => {
    return (
        <div className={styles.circle}>
            <div className={styles.phonecall}>
                <Phonecall
                    phone={phoneNumber}
                    description={i18next.t('connections_phonecall_circle_title')}
                />
            </div>
            <div className={styles.whatsappcall}>
                <Whatsappcall
                    phone={whatsappNumber}
                    text={whatsappText}
                    description={i18next.t('connections_whatsappcall_circle_title')}
                />
            </div>
            <div className={styles.close}>
                <button onClick={closeClick}>
                    <SvgIcon icon="close" />
                </button>
            </div>
        </div>
    )
}


export const ConnectionPure = ({
                               phonecallPhone,
                               phonecallSwitchOn,
                               whatsappcallPhone,
                               whatsappcallText,
                               whatsappcallSwitchOn,
                               openConnectionCircle,
                           }) => {

    const openClick = () => {
        store.dispatch(actions.openConnectionCircle());
    }

    const closeClick = () => {
        store.dispatch(actions.closeConnectionCircle());
    }

    let element = null

    if(phonecallSwitchOn && whatsappcallSwitchOn ){
        element = (
            <div>
                {
                    !openConnectionCircle && (
                        <div className={styles.single}>
                            <button onClick={openClick}>
                                <SvgIcon icon="phonecall"/>
                            </button>
                        </div>
                    )
                }
                {
                    openConnectionCircle && (
                        <Circle
                            phoneNumber={phonecallPhone}
                            whatsappNumber={whatsappcallPhone}
                            whatsappText={whatsappcallText}
                            closeClick={closeClick}
                        />
                    )
                }

            </div>
        )
    }
    else if(phonecallSwitchOn) {
        element = (
            <div className={styles.single}>
                <Phonecall phone={phonecallPhone} backColor={true}/>
            </div>
        )
    }
    else if(whatsappcallSwitchOn){
        element = (
            <div className={styles.single}>
                <Whatsappcall
                    phone={whatsappcallPhone}
                    text={whatsappcallText}
                    backColor={true}
                />
            </div>
        )

    }

    return (
        element
    )
}

const mapStateToProps = state => ({
    phonecallPhone: state.profile.phonecallPhone,
    phonecallSwitchOn: state.profile.phonecallSwitchOn,
    whatsappcallPhone: state.profile.whatsappcallPhone,
    whatsappcallText: state.profile.whatsappcallText,
    whatsappcallSwitchOn: state.profile.whatsappcallSwitchOn,
    openConnectionCircle: state.global.openConnectionCircle,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const Connection = connect(mapStateToProps, mapDispatchToProps)(ConnectionPure)

