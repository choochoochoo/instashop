import React from 'react'
import {Link} from 'react-router-dom';
import {Field, reduxForm} from 'redux-form'

import styles from './OrderButton.css'

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import * as actions from '../../actions';
import store from '../../store.js';

const classNames = require('classnames');

export const OrderButtonPure = ({
                           title,
                           click,
                        }) => {
    return (
        <div className={styles.root}>
            <button type="button" className={styles.button} onClick={click} >
                <span className={styles.buttonText}>{title}</span>
             </button>
        </div>
    )
}

const mapStateToProps = state => ({
    theme: state.theme
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const OrderButton = connect(mapStateToProps, mapDispatchToProps)(OrderButtonPure)



