import React from 'react'
import InputMask from 'react-input-mask';
import i18next from 'i18next';

import styles from './Product.css'


import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import * as actions from '../../actions';
import store from '../../store.js';

import {getProduct} from '../../utils/data'

import {addProductToClientCart, clearCart} from '../../api/cart';
import {profilable} from '../hocs'
import {ClosePanel, PhoneTemplate} from '../common'
import {getFile} from '../../api'
import {normalizeTextareaText, getOrderButtonVisible} from '../../utils/common'
import {defaultProductImage} from '../../resources'

import {Connection} from './Connection.jsx'
import {OrderButton} from './OrderButton.jsx'

export const ProductPure = ({
                                match,
                                history,
                                allProducts,
                                appState,
                                userId,
                                accountName,
                                orderStatus,

                                formProduct,
                            }) => {
    let product = null

    if(!formProduct){
        product = getProduct(allProducts, match.params.productId);
    }else{
        product = formProduct
    }

    let title = null;
    let price = null;
    let description = null;
    let photo = null;

    if(product){
        title = product.title;
        price = product.price;
        description = product.description


        if(!formProduct) {
            photo = product.photo ? getFile(product.photo.size1080) : `/${defaultProductImage}`
        }else{
            photo = product.photo.size1080 ? product.photo.size1080 : `/${defaultProductImage}`

            product.photo.size1080 = product.originalPhoto

            if(product.description) {
                description = product.description.replace(/\n/g, '\r\n');
            }
        }
    }


    const orderButtonClick = () => {
        if(orderStatus === 'new'){
            clearCart(userId)
            store.dispatch(actions.clearClientCart());

            setTimeout(() => {
                addProductToClientCart(product, userId)
                store.dispatch(actions.addProductToClientCart(product));
                history.replace(`/${accountName}/client-cart`)
            }, 1);

        }else{
            addProductToClientCart(product, userId)
            store.dispatch(actions.addProductToClientCart(product));
            history.replace(`/${accountName}/client-cart`)
        }

    }

    return (
        <PhoneTemplate>
            <div className={styles.root}>
                <ClosePanel
                    text={title}
                    userId={accountName}
                    type="white"
                    border={false}
                    showClose={!formProduct}
                />
                <div className={styles.photoWrapper}>
                    <img src={photo} width="100%"/>
                </div>
                <div className={styles.price}>
                    {price}
                </div>

                {
                    product && product._id && getOrderButtonVisible(appState) &&
                    <OrderButton
                        title={i18next.t("product_order_button_title")}
                        click={orderButtonClick}
                    />
                }

                <div
                    className={styles.description}
                    dangerouslySetInnerHTML={{__html: normalizeTextareaText(description)}}
                />

                <Connection />
            </div>
        </PhoneTemplate>
    )
}


const mapStateToProps = state => ({
    profile: state.profile,
    userId: state.profile._id,
    global: state.global,
    allProducts: state.profile.allProducts,
    appState: state,
    accountName: state.profile.accountName,
    orderStatus: state.clientCart.status,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const Product = connect(mapStateToProps, mapDispatchToProps)(profilable(ProductPure))