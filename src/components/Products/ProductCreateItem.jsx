import React, {PropTypes} from 'react'
import styles from './ProductCreateItem.css'

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import * as actions from '../../actions';
import store from '../../store.js';

import {createProduct, createProductFake} from '../../resources'
import i18next from "i18next";

const classNames = require('classnames');

export const ProductCreateItemPure = (props) => {

    const iconStyle = {
        backgroundImage: `url(${createProduct})`,
        backgroundRepeat: 'no-repeat',
    }

    return (
        <div className={classNames(styles.item, styles[`theme-${props.theme.key}-item`])}>
            <img src={`/${createProductFake}`} width="100%"/>
            <div className={styles.price}>
                123
            </div>
            <div className={styles.title}>
                123
            </div>

            <div className={styles.textWrapper}>
                <div className={styles.textPlus}>+</div>
                <div className={classNames(styles.text, styles[`theme-${props.theme.key}-text`])}>
                    {i18next.t('create_new_product_button')}
                </div>
            </div>


            <div className={styles.icon}>
                <div
                    style={iconStyle}
                    className={classNames(styles.icon, styles[`theme-${props.theme.key}-icon`])}
                >
                </div>
            </div>
        </div>
    )
}


const mapStateToProps = state => ({
    global: state.global
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const ProductCreateItem = connect(mapStateToProps, mapDispatchToProps)(ProductCreateItemPure)