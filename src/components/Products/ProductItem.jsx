import React, {PropTypes} from 'react'
import styles from './ProductItem.css'

const classNames = require('classnames');


export const ProductItem = ({
                                icon,
                                price,
                                title,
                                theme,

                            }) => {

    let block = (
        <div>
            <div className={classNames(styles.price, styles[`theme-${theme.key}-price`])}>
                {price}
            </div>
            <div className={classNames(styles.title, styles[`theme-${theme.key}-title`])}>
                {title}
            </div>
        </div>
    )

    if(theme.key === 'steve-jobs'){
        block = (
            <div>
                <div className={classNames(styles.title, styles[`theme-${theme.key}-title`])}>
                    {title}
                </div>
                <div className={classNames(styles.price, styles[`theme-${theme.key}-price`])}>
                    {price}
                </div>
            </div>
        )
    }


    return (
        <div className={classNames(styles.item, styles[`theme-${theme.key}-item`])}>
            <img
                src={icon}
                width="100%"
                className={classNames(styles.image, styles[`theme-${theme.key}-image`])}
            />
            { block }
        </div>
    )
}
