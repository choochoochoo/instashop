import React, {PropTypes} from 'react'
import {Link} from 'react-router-dom';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as actions from '../../actions';
import store from '../../store.js';

import {ProductItem} from './ProductItem.jsx'
import {ProductCreateItem} from './ProductCreateItem.jsx'

import styles from './Products.css'
const classNames = require('classnames');

import {
    defaultProductImage,
    exampleProductImage1,
    exampleProductImage2,
    exampleProductImage3
} from '../../resources'

import {getFile} from '../../api'

import {CategoriesPanel} from '../common'

import {getProducts} from '../../utils/common'

const EXAMPLE_PRODUCT1 = '5c0f8cd07ccb0e0eb727bdb9'
const EXAMPLE_PRODUCT2 = '5c0f8cd07ccb0e0eb727bdba'
const EXAMPLE_PRODUCT3 = '5c0f8cd07ccb0e0eb727bdbb'

const getExapmleImage = (id) => {
    switch (id) {
        case EXAMPLE_PRODUCT1:
            return exampleProductImage1
        case EXAMPLE_PRODUCT2:
            return exampleProductImage2
        case EXAMPLE_PRODUCT3:
            return exampleProductImage3
        default:
            return defaultProductImage
    }
}

export const ProductsPure = ({
                                 userId,
                                 accountName,
                                 loginedUser,
                                 viewModeEnabled,
                                 theme,
                                 categories,
                                 allProducts,
                                 selectedCategory,
                                 categoriesProductsSwitchOn,
                            }) => {

    const myAccount = loginedUser._id === userId
    const myAccAndNotView = myAccount && !viewModeEnabled

    const categoryAllClick = () => {
        store.dispatch(actions.selectCategory({products: allProducts, _id: 'all'}));
    }

    const categoryItemClick = (item) => {
        store.dispatch(actions.selectCategory(item));
    }

    const products = getProducts(allProducts, categories, selectedCategory)

    return (
        <div className={styles.root}>
            {
                categories.length > 0 && categoriesProductsSwitchOn && <CategoriesPanel
                    categories={categories}
                    categoryAllClick={categoryAllClick}
                    categoryItemClick={categoryItemClick}
                    selectedCategory={selectedCategory}
                />
            }
            <div className={classNames(styles.items, styles[`theme-${theme.key}-items`])}>
                {
                    myAccAndNotView &&
                    (
                        <Link to={`/${accountName}/products/create`} className={styles.link}>
                            <ProductCreateItem theme={theme}/>
                        </Link>
                    )
                }

                {
                    products.map((item) => {

                        const size1080 = item.photo ? getFile(item.photo.size1080) : getExapmleImage(item._id)



                        const price = item.price ? item.price : '—'

                        const link = myAccount ?
                            `/${accountName}/products/edit/${item._id}` :
                            `/${accountName}/products/${item._id}`

                        return (
                            <Link to={link} key={item._id} className={styles.link}>
                                <ProductItem
                                    key={item._id}
                                    title={item.title}
                                    desctiption={item.desctiption}
                                    price={price}
                                    icon={size1080}
                                    theme={theme}
                                />
                            </Link>
                        )
                    })
                }
            </div>
        </div>
    )
}

const mapStateToProps = state => ({
    accountName: state.profile.accountName,
    loginedUser: state.global.loginedUser,
    viewModeEnabled: state.global.viewModeEnabled,
    theme: state.theme,
    categories: state.profile.categories,
    allProducts: state.profile.allProducts,
    selectedCategory: state.profile.selectedCategory,
    categoriesProductsSwitchOn: state.profile.categoriesProductsSwitchOn,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const Products = connect(mapStateToProps, mapDispatchToProps)(ProductsPure)