import React from 'react'
import {SvgIcon} from '../common'
import i18next from 'i18next'
import styles from './ChangeAccountNameButton.css'

import names from '../../constants/names.json'

export const ChangeAccountNameButton = ({click}) => {

    return (
        <button className={styles.root} onClick={click}>
            <div>
                <div className={styles.title}>
                    {i18next.t('change_account_name_button_title')}
                </div>
                <div className={styles.accountLable}>
                    <span className={styles.accountLableText}>
                        {`${names.brand_name}/${names.account_name}`}
                    </span>
                    <span className={styles.refresh}>
                        <SvgIcon icon="refresh"/>
                    </span>
                </div>
            </div>
            <div className={styles.rightCol}>
                <SvgIcon icon="arrow"/>
            </div>
        </button>
    )
}
