import React from 'react'
import {Field} from 'redux-form'
import {FileUploadField, EditorButton, SvgIcon, Logo} from '../common'
import i18next from 'i18next'
import {profileLogoDefault} from '../../resources'

import styles from './ImageField.css'


export const ImageField = ({
                               preview,
                               imageClick,
                            }) => {

    return (
        <button className={styles.button} onClick={imageClick} type="button">
            <div className={styles.root}>
                <div className={styles.leftCol}>
                    <div>
                        <Logo icon={preview ? preview : `/${profileLogoDefault}`} />
                    </div>
                    <div className={styles.text}>
                        {i18next.t('profile_change_logo_title')}
                    </div>
                </div>
                <div className={styles.rightCol}>
                    <SvgIcon icon="arrow"/>
                </div>
            </div>
        </button>
    )
}