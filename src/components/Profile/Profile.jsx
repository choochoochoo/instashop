import React, {Component} from 'react';
import styles from './Profile.css'

import {Link} from 'react-router-dom';

import {ProfileForm} from './ProfileForm.jsx'
import {Title, Subtitle, ClosePanel, ImageEditor, PhoneTemplate, ChangeImageMenu} from '../common'

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import * as actions from '../../actions';
import {getUser, editUserUni} from '../../api';
import store from '../../store.js';
import i18next from "i18next";
import {profilable} from '../hocs'
const _ = require('lodash');

import {
    getPreview,
    getOriginalPreview,
    getCrop,
    sendErrorToAnaliticsExc,
} from '../../utils/common'

import * as notificaitonTypes from '../../constants/NotificationTypes'

import names from '../../constants/names.json'


export const ProfilePure = ({
                                match,
                                history,
                                userId,
                                accountName,
                                theme,
                                profile,

                                photoOriginal,
                                photoSize1080,

                                cropOriginal,
                                acceptedFileCrop,

                                acceptedFile,
                                acceptedFilePreview,
                                acceptedFileName,

                                pixelCropX,
                                pixelCropY,
                                pixelCropWidth,
                                pixelCropHeight,

                                cropX,
                                cropY,
                                cropWidth,
                                cropHeight,

                                isOpenImageEditor,
                                croppedImageFilePreview,
                                openChangeImageMenu,
                            }) => {

    const handleSubmit = (values) => {
        store.dispatch(actions.showProgress());
        store.dispatch(actions.userEditStart())

        editUserUni(userId, {
            email: values.email,
            companyPhone: values.phone,
            companyName: values.title,
            companyDescription: values.description,
            theme: theme,
            language: values.language,

            photoOriginal: photoOriginal,
            photoSize1080: photoSize1080,

            photo: acceptedFile,
            photoMetaFileName: acceptedFileName,
            photoMetaCropX: cropX,
            photoMetaCropY: cropY,
            photoMetaCropWidth: cropWidth,
            photoMetaCropHeight: cropHeight,
            photoMetaPixelCropX: pixelCropX,
            photoMetaPixelCropY: pixelCropY,
            photoMetaPixelCropWidth: pixelCropWidth,
            photoMetaPixelCropHeight: pixelCropHeight,
        }, profile).then(response => {
            store.dispatch(actions.userEditSuccess(response.data));
            store.dispatch(actions.clearLoadedFile());
            store.dispatch(actions.hideProgress());
            history.push(`/${accountName}`)

            store.dispatch(actions.showNotification(notificaitonTypes.SAVE));
            setTimeout(()=>{
                store.dispatch(actions.hideNotification());
            }, notificaitonTypes.TIMEOUT)
        }).catch(exception => {
            sendErrorToAnaliticsExc(exception)

            console.log(exception);
            store.dispatch(actions.userEditError(exception));
            store.dispatch(actions.hideProgress());

            store.dispatch(actions.showNotification(notificaitonTypes.ERROR));

            setTimeout(()=>{
                store.dispatch(actions.hideNotification());
            }, notificaitonTypes.TIMEOUT)
        });
    }

    const handleClose = (photo) => {
         store.dispatch(actions.clearLoadedFile());
    }

    const handleAccepted = (file) => {
        store.dispatch(actions.openImageEditor(
            file.file,
            file.file.name,
            file.preview
        ));

        store.dispatch(actions.closeChangeImageMenu());
    }

    const changeAccountNameButtonClick = () => {
        history.replace(`/${accountName}/profile/account-name`)
    }

    const changeImageClick = () => {
        store.dispatch(actions.openChangeImageMenu());
    }

    const imageEditorVisible = isOpenImageEditor
    const formVisible = !imageEditorVisible

    const hasAccountName = userId !== accountName

    let accountNameLabel = null

    if(hasAccountName){
        accountNameLabel = <div className={styles.accountNameLabel}>{`${names.brand_name}/${accountName}`}</div>
    }


    return (
        <PhoneTemplate>
            <div>
                <ImageEditor visible={imageEditorVisible}/>

                <ChangeImageMenu
                    visible={openChangeImageMenu}
                    photoAccepted={handleAccepted}
                    originalPreview={getOriginalPreview(photoOriginal, acceptedFilePreview)}
                    crop={getCrop(cropOriginal, acceptedFileCrop)}
                />

                <div className={styles.root} style={{
                    display: !isOpenImageEditor ? 'block' : 'none' ,
                    visibility: !isOpenImageEditor ? 'visible' : 'hidden',
                }}>
                    <ClosePanel
                        text={i18next.t('profile')}
                        userId={accountName}
                        click={handleClose}
                        buttons={accountNameLabel}
                    />

                    <ProfileForm
                        onSubmit={handleSubmit}
                        //photoAccepted={handleAccepted}
                        //originalPreview={getOriginalPreview(photoOriginal, acceptedFilePreview)}
                        preview={getPreview(photoSize1080, croppedImageFilePreview)}
                        //crop={getCrop(cropOriginal, acceptedFileCrop)}
                        changeAccountNameButtonClick={changeAccountNameButtonClick}
                        hasAccountName={hasAccountName}
                        changeImageClick={changeImageClick}
                    />
                </div>
            </div>
        </PhoneTemplate>
    )
}

const mapStateToProps = state => ({
    userId: state.profile._id,
    accountName: state.profile.accountName,
    theme: state.profile.theme,
    profile: state.profile,

    photoOriginal: _.get(state, 'profile.logo.original', null),
    photoSize1080: _.get(state, 'profile.logo.size1080', null),

    cropOriginal: _.get(state, 'profile.logo.crop', null),
    acceptedFileCrop: state.global.crop,

    acceptedFile: state.global.acceptedFile,
    acceptedFilePreview: state.global.acceptedFilePreview,
    acceptedFileName: state.global.acceptedFileName,

    pixelCropX: _.get(state, 'global.pixelCrop.x'),
    pixelCropY: _.get(state, 'global.pixelCrop.y'),
    pixelCropWidth: _.get(state, 'global.pixelCrop.width'),
    pixelCropHeight: _.get(state, 'global.pixelCrop.height'),

    cropX: _.get(state, 'global.crop.x'),
    cropY: _.get(state, 'global.crop.y'),
    cropWidth: _.get(state, 'global.crop.width'),
    cropHeight: _.get(state, 'global.crop.height'),

    isOpenImageEditor: state.imageEditor.opened,
    croppedImageFilePreview: state.global.croppedImageFilePreview,
    openChangeImageMenu: state.global.openChangeImageMenu,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch),
})

export const Profile = connect(mapStateToProps, mapDispatchToProps)(profilable(ProfilePure))