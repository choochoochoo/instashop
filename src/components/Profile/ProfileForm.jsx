import React from 'react'
import {Field, reduxForm} from 'redux-form'
import i18next from 'i18next';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import {ChangeAccountNameButton} from './ChangeAccountNameButton.jsx'
import {ImageField} from "./ImageField.jsx";

import {
    InputField,
    TextareaField,
    PhoneField,
    Button,
    FileUploadField,
    SelectField,
} from '../common'

import list from '../../languages/list.json'
import {addProductToClientCart, clearCart} from "../../api/cart";
import store from "../../store";
import * as actions from "../../actions";


const validate = values => {
    const errors = {}

    // if (!values.logo) {
    //     errors.logo = 'Required'
    // }

    // if (!values.title) {
    //     errors.title = 'Required'
    // }
    //
    // if (!values.phone) {
    //     errors.phone = 'Required'
    // } else if (!/^[1-9]{1}[0-9]{3,14}$/i.test(values.phone)) {
    //     errors.phone = 'Invalid phone number'
    // }

    // if (!values.email) {
    //     errors.email = 'Required'
    // }

    return errors
}




let ProfileFormPure = props => {
    const {
        handleSubmit,
        photoAccepted,
        originalPreview,
        preview,
        crop,
        changeAccountNameButtonClick,
        hasAccountName,
        changeImageClick
    } = props

    return (
        <form onSubmit={handleSubmit}>

            <ImageField
                preview={preview}
                imageClick={changeImageClick}
            />

            { !hasAccountName && <ChangeAccountNameButton click={changeAccountNameButtonClick} /> }

            <InputField
                name="title"
                type="text"
                label={i18next.t('profile_company_name_label')}
                placeholder={i18next.t('profile_company_name_placeholder')}
            />

            <TextareaField
                name="description"
                type="text"
                rows={3}
                label={i18next.t('profile_company_description_label')}
                placeholder={i18next.t('profile_company_description_placeholder')}
            />

            {/*<InputField*/}
                {/*name="phone"*/}
                {/*type="text"*/}
                {/*label={i18next.t('profile_company_phone_label')}*/}
                {/*placeholder={i18next.t('profile_company_phone_placeholder')}*/}
            {/*/>*/}

            {/*<SelectField*/}
                {/*list={list}*/}
                {/*name="language"*/}
                {/*label={i18next.t('profile_company_language_label')}*/}
            {/*/>*/}

            <div>
                <Button title={i18next.t('profile_submit_save')} />
            </div>
        </form>
    )
}

ProfileFormPure = reduxForm({
    form: 'ProfileForm',
    validate,
    enableReinitialize: true
})(ProfileFormPure)


export const ProfileForm = connect(
    state => {
        return {
            initialValues: state.profile,
        }
    }
)(ProfileFormPure)


