import React from 'react'
import InputMask from 'react-input-mask';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import styles from './ProgressDialog.css'

import {makeOrder} from '../../api';
import * as actions from '../../actions';
import store from '../../store.js';

export const ProgressDialogPure = ({opened}) => {
    return (
        <div className={styles.modal} style={{
            opacity: opened ? 1 : 0,
            visibility: opened ? 'visible' : 'hidden'
        }}>
            <div className={styles.loading}>
                <div className={styles.loadingBar}></div>
                <div className={styles.loadingBar}></div>
                <div className={styles.loadingBar}></div>
                <div className={styles.loadingBar}></div>
            </div>
        </div>
    )
}


const mapStateToProps = state => ({
    opened: state.global.progressFormOpen
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const ProgressDialog = connect(mapStateToProps, mapDispatchToProps)(ProgressDialogPure)