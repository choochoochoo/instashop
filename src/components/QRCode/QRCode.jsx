import React from 'react'
import styles from './QRCode.css'
import i18next from 'i18next';
import {Title, Subtitle, ClosePanel, SvgIcon, FeedbackPanel, PhoneTemplate} from '../common'

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import * as actions from '../../actions';
import store from '../../store.js';

import {getProfileLink} from "../../utils/common";

var qr = require('qrcode')
import jsPDF from "jspdf";


export class QRCodePure extends React.Component {
    state = {
        qrcodeUrl: null,
    }

    componentWillMount() {
        const userId = this.props.match.params.userId
        const link = getProfileLink(userId)

        console.log(link)

        var opts = {
            errorCorrectionLevel: 'H',
            type: 'image/png',
            rendererOpts: {
                quality: 0.92
            }
        }

        qr.toDataURL(link, opts, function (err, url) {
            if (err) throw err
            store.dispatch(actions.setSrcQRCode(url));
        })
    }

    render() {
        const userId = this.props.match.params.userId
        const srcQRCode = this.props.srcQRCode

        const downloadPng = () => {
            const link = getProfileLink(userId)

            var canvas = document.createElement('canvas')

            qr.toCanvas(canvas, link, function (error) {
                if (error) console.error(error)


                canvas.toBlob(file => {
                    var FileSaver = require('file-saver');
                    FileSaver.saveAs(file, 'qrcode.png');
                }, 'image/png');

                console.log('success!');
            })
        }

        const downloadPdf = () => {
            const link = getProfileLink(userId)

            var canvas = document.createElement('canvas')

            qr.toCanvas(canvas, link, function (error) {
                if (error) console.error(error)

                var imgData = canvas.toDataURL("image/png", 1.0);
                var pdf = new jsPDF();

                pdf.addImage(imgData, 'JPEG', 0, 0);
                pdf.save("qrcode.pdf");
            })
        }

        return (
            <PhoneTemplate>
                <div className={styles.root}>
                    <ClosePanel
                        text={i18next.t('qrcode_title')}
                        userId={userId}
                        border={false}
                        type="white"
                    />
                    <div className={styles.content}>
                        <img width="240px" src={srcQRCode}></img>
                    </div>

                    <div className={styles.description}>
                        {i18next.t('qr_code_description')}
                    </div>

                    <div className={styles.buttonPanel}>
                        <button onClick={downloadPng}>
                            <SvgIcon icon="pngFile" width={64} height={64}/>
                        </button>
                        <span className={styles.textBetweenButton}>
                            {i18next.t('qr_code_text_between_button')}
                        </span>
                        <button onClick={downloadPdf}>
                            <SvgIcon icon="pdfFile" width={64} height={64}/>
                        </button>
                    </div>

                    {/*<FeedbackPanel text={i18next.t('qr_code_feedback_text')} />*/}
                </div>
            </PhoneTemplate>
        )
    }
}


const mapStateToProps = state => ({
    srcQRCode: state.global.srcQRCode,
    accountName: state.profile.accountName,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const QRCode = connect(mapStateToProps, mapDispatchToProps)(QRCodePure)