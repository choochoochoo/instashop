import React from 'react'
import InputMask from 'react-input-mask';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import {createUser} from '../../api';
import * as actions from '../../actions';
import store from '../../store.js';

import styles from './Register.css'
import {RegisterForm} from './RegisterForm.jsx'
import {ClosePanel, PhoneTemplate, InstagrammLoginButton} from '../common'
import i18next from "i18next";
import _ from 'lodash'

import {authorizable} from '../hocs/'

import {getLanguage} from '../../utils/common'

export const RegisterPure = ({registerCode, history}) => {

    const handleSubmit = (values) => {
        store.dispatch(actions.showProgress());
        store.dispatch(actions.userCreateStart());

        createUser({
            email: _.trim(values.email.toLowerCase()),
            language: getLanguage(),
        }).then(response => {
            if(response.data.user){
                store.dispatch(actions.userCreateSuccess(response.data.user));
                history.push(`/register-success`)
            }else{
                store.dispatch(actions.userCreateNotSuccess(response.data.code));
            }

            store.dispatch(actions.hideProgress());

        }).catch(exception => {
            console.log(exception);
            store.dispatch(actions.userCreateError(exception));
            store.dispatch(actions.hideProgress());
        });
    }

    const click = () => {
        history.push(`/`)
    }

    return (
        <PhoneTemplate>
            <div className={styles.root}>
                <ClosePanel
                    text={i18next.t('register_title')}
                    border={false}
                    isLink={false}
                    click={click}
                />

                {/*<InstagrammLoginButton title={i18next.t('instagramm_button_title_register')}/>*/}

                {/*<div className={styles.orPanel}>*/}
                    {/*<div className={styles.orPanelText}>*/}
                        {/*{i18next.t('register_title_or')}*/}
                    {/*</div>*/}
                {/*</div>*/}

                <RegisterForm
                    onSubmit={handleSubmit}
                    registerCode={registerCode}
                />
            </div>
        </PhoneTemplate>
    )
}


const mapStateToProps = state => ({
    loginedUser: state.global.loginedUser,
    registerCode: state.global.registerCode,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const Register = connect(mapStateToProps, mapDispatchToProps)(authorizable(RegisterPure))