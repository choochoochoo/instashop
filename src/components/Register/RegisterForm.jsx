import React from 'react'
import {Field, reduxForm} from 'redux-form'
import styles from './Register.css'

import {
    SwitchInputField,
    Button,
    ErrorMessage,
} from '../common'
import i18next from "i18next";

const emailRegExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/i;

const validate = values => {
    const errors = {}

    if (!values.email) {
        errors.email = i18next.t('input_field_required')
    } else if (!emailRegExp.test(values.email)) {
        errors.email = i18next.t('input_field_invalid_email')
    }

    return errors
}

let RegisterFormPure = props => {
    const {handleSubmit, handleCancel, registerCode} = props
    return (
        <form onSubmit={handleSubmit} className={styles.formWrapper}>
            <SwitchInputField
                name="email"
                type="email"
                label={i18next.t('register_email_label')}
                placeholder={i18next.t('register_email_placeholder')}
            />

            <div className={styles.buttonWrapper}>
                <ErrorMessage
                    text={i18next.t(`register_error_${registerCode}`)}
                    visible={!!registerCode}
                />
                <Button
                    title={i18next.t('register_button')}
                    className={styles.loginButton}
                    typeStyle="white"
                />
            </div>
        </form>
    )
}

export const RegisterForm = reduxForm({
    form: 'RegisterForm',
    validate
})(RegisterFormPure)

