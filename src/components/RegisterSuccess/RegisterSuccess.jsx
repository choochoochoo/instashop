import React from 'react'
import styles from './RegisterSuccess.css'
import {Title, Subtitle, SvgIcon, LinkButton, PhoneTemplate} from '../common'
import i18next from "i18next";

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import {createUser} from '../../api';
import * as actions from '../../actions';
import store from '../../store.js';
const _ = require('lodash')

export const RegisterSuccessPure = ({email, userId}) => {
    return (
        <PhoneTemplate>
            <div className={styles.root}>
                <Title text={i18next.t('register_success_text')}/>
                <div className={styles.iconWrapper}>
                    <SvgIcon icon="email" width={64} height={64}/>
                </div>
                <div className={styles.subtitleWrapper}>
                    <Subtitle text={i18next.t('register_success_description')}/>
                </div>
                <div className={styles.emailWrapper}>
                    {email}
                </div>
                <div className={styles.buttonWrapper}>
                    <LinkButton
                        title={i18next.t('register_success_button')}
                        link={`/${userId}`}
                    />
                </div>
            </div>
        </PhoneTemplate>
    )
}

const mapStateToProps = state => ({
    userId: _.get(state, 'global.loginedUser._id'),
    email: _.get(state, 'global.loginedUser.email'),
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const RegisterSuccess = connect(mapStateToProps, mapDispatchToProps)(RegisterSuccessPure)
