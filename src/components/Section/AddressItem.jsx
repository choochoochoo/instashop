import React from 'react'
import styles from './AddressItem.css'
import {SvgIcon} from "../common";

export const AddressItem = ({value, icon}) => {
    return (
        <div className={styles.phoneBlock}>
            <div className={styles.phoneIcon}>
                <SvgIcon icon={icon}/>
            </div>
            <div className={styles.phoneText}>
                {value}
            </div>
        </div>
    )
}



