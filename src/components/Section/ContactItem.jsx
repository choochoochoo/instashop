import React from 'react'
import styles from './ContactItem.css'
import {SvgIcon} from "../common";

export const ContactItem = ({value, icon, type}) => {
    return (
        <a href={`${type}:${value}`} target="_self">
            <div className={styles.phoneBlock}>
                <div className={styles.phoneIcon}>
                    <SvgIcon icon={icon}/>
                </div>
                <div className={styles.phoneText}>
                    {value}
                </div>
            </div>
        </a>
    )
}



