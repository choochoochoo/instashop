import React from 'react'
import {Field, reduxForm} from 'redux-form'
import {connect} from "react-redux";

import i18next from "i18next";

import styles from './ContactsView.css'
import sectionViewStyles from '../common/TextEditor/SectionView.css'

import {MapView, SvgIcon} from '../common'

import {urlify} from '../../utils/common'

import {ContactItem} from "./ContactItem.jsx"
import {AddressItem} from "./AddressItem.jsx"
import {TextAreaView} from "../common/TextAreaView.jsx";


export let ContactsView = props => {

    const {
        phone,
        email,
        additionalInfo,
        address,
        latitude,
        longitude,
    } = props

    const hasMap = () => {
        return latitude && longitude
    }

    return (
        <div className={styles.root}>

            { phone && <ContactItem value={phone} icon="contactPhone" type="tel" /> }

            { email && <ContactItem value={email} icon="contactMail" type="mailto"/> }

            { address && <AddressItem value={address} icon="contactLocation"/> }

            {
                additionalInfo && <hr className={styles.line}/>
            }

            <TextAreaView content={additionalInfo} className={styles.additionalInfo} />

            {
                hasMap() && <hr className={styles.line}/>
            }

            <div className={styles.mapSection}>
                {
                    hasMap() && <MapView
                        lat={parseFloat(latitude)}
                        lng={parseFloat(longitude)}
                        width="100%"
                        height="166px"
                    />
                }
            </div>
        </div>
    )
}
