import React from 'react'
import InputMask from 'react-input-mask';
import i18next from 'i18next';

import styles from './Section.css'

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import * as actions from '../../actions';
import store from '../../store.js';

import {getSection} from '../../api';
import {profilable} from '../hocs'
import {ClosePanel, SvgIcon, EditButton, PhoneTemplate} from '../common'
import {getFile} from '../../api'

import {SectionView} from './SectionView.jsx'
import {ContactsView} from './ContactsView.jsx'
import {getSectionTitle} from '../../utils/common'


const getElement = (type, elements) =>{

    switch (type) {
        case 'section':

            return (
                <SectionView content={elements.text} />
            )

            break;
        case 'contacts':

            return (
                <ContactsView
                    phone={elements.phone}
                    email={elements.email}
                    additionalInfo={elements.additionalInfo}
                    address={elements.address}
                    latitude={elements.latitude}
                    longitude={elements.longitude}
                />
            )

            break;
        default:
            return <div/>
    }
}

export const SectionPure = ({
                                match,
                                history,
                                profile,
                                title,
                                elements,
                                type,
                                _id,
                                loginedUser,
                                viewModeEnabled,
                                userId,
                                accountName,
                            }) => {

    const myAccount = loginedUser._id === userId
    const myAccAndNotView = myAccount && !viewModeEnabled

    const link = `/${accountName}/sections/edit/${type}/${_id}`

    const clickPanel = () => {
        store.dispatch(actions.clearCreateSection());
    }

    const buttons = myAccAndNotView ? (
        <EditButton
            title={i18next.t('section_view_edit')}
            link={link}
            type
        /> ): null

    return (
        <PhoneTemplate>
            <div className={styles.root}>
                <ClosePanel
                    text={getSectionTitle(title, type)}
                    userId={accountName}
                    buttons={buttons}
                    click={clickPanel}
                />

                { getElement(type, elements) }
            </div>
        </PhoneTemplate>
    )
}


const mapStateToProps = state => ({
    profile: state.profile,
    global: state.global,
    loginedUser: state.global.loginedUser,
    viewModeEnabled: state.global.viewModeEnabled,
    title: state.editSection.title,
    type: state.editSection.type,
    elements: state.editSection.elements,
    _id: state.editSection._id,
    userId: state.profile._id,
    accountName: state.profile.accountName,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

class Section2 extends React.Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {
        const sectionId = this.props.match.params.sectionId

        store.dispatch(actions.showProgress());
        store.dispatch(actions.sectionGetStart());

        getSection(sectionId).then(response => {
            store.dispatch(actions.sectionGetSuccess(response.data));
            store.dispatch(actions.hideProgress());
        }).catch(exception => {
            console.log(exception);
            store.dispatch(actions.sectionGetError(exception));
            store.dispatch(actions.hideProgress());
        });

    }

    render() {
        return(
            <SectionPure {...this.props}/>
        )
    }
}

export const Section = connect(mapStateToProps, mapDispatchToProps)(profilable(Section2))