import React from 'react'

import styles from './SectionView.css'
import {TextAreaView} from "../common/TextAreaView.jsx";

export let SectionView = props => {

    const {
        content
    } = props


    return (
        <div className={styles.root}>
            <TextAreaView content={content} />
        </div>
    )
}
