import React from 'react'
import styles from './Settings.css'
import i18next from 'i18next';
import {Title, Subtitle, ClosePanel, SvgIcon, PhoneTemplate} from '../common'

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import * as actions from '../../actions';
import store from '../../store.js';
import {profilable} from '../hocs'
import {SettingsItem} from './SettingsItem.jsx'

export const SettingsPure = ({match, history, accountName}) => {
    const userId = match.params.userId


    return (
        <PhoneTemplate>
            <div className={styles.root}>
                <ClosePanel
                    text={i18next.t('settings_menu')}
                    userId={accountName}
                />

                <div className={styles.settings}>

                    <SettingsItem
                        link={`/${accountName}/categories`}
                        text={i18next.t('settings_categories')}
                    />

                    <SettingsItem
                        link={`/${accountName}/connections`}
                        text={i18next.t('settings_connections')}
                    />

                    <SettingsItem
                        link={`/${accountName}/settings/orders`}
                        text={i18next.t('settings_orders')}
                    />

                    <SettingsItem
                        link={`/${accountName}/theme`}
                        text={i18next.t('settings_change_theme')}
                    />

                </div>
            </div>
        </PhoneTemplate>
    )
}


const mapStateToProps = state => ({
    userId: state.profile._id,
    accountName: state.profile.accountName,
    profile: state.profile,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const Settings = connect(mapStateToProps, mapDispatchToProps)(profilable(SettingsPure))