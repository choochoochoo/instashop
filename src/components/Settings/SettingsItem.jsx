import React from 'react'
import {Link} from 'react-router-dom';
import i18next from 'i18next';

import styles from './SettingsItem.css'
import {SvgIcon} from "../common";

export const SettingsItem = ({link, text, click}) => {
    return (
        <div className={styles.menuItemButtonWrapper}>
            <Link to={link} className={styles.menuItem}>
                <button onClick={click} className={styles.menuItemButton}>
                    <div className={styles.text}>
                        {text}
                    </div>
                    <div className={styles.arrow}>
                        <SvgIcon icon="arrow"/>
                    </div>
                </button>
            </Link>
        </div>
    )
}
