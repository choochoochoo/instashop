import React from 'react'
import styles from './OrdersSwitch.css'
import {Button, SvgIcon, Switch, SwitchInputField, SwitchTextareaField} from '../common'
import i18next from 'i18next';

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as actions from '../../actions';
import store from '../../store.js';

export const OrdersSwitchPure = ({settingsOrdersSwitchOn}) => {

    const switchClick = () => {
        if(settingsOrdersSwitchOn) {
            store.dispatch(actions.switchOffSettingsOrders());
        }else{
            store.dispatch(actions.switchOnSettingsOrders());
        }
    }

    return (
        <div className={styles.root}>
            <div className={styles.header}>
                <div className={styles.leftCol}>
                    <div className={styles.title}>
                        {i18next.t('settings_orders_switch_label')}
                    </div>
                </div>
                <div className={styles.rightCol}>
                    <Switch click={switchClick} value={settingsOrdersSwitchOn}/>
                </div>
            </div>
            {
                settingsOrdersSwitchOn &&
                <div className={styles.content}>
                    <div className={styles.form}>
                        <div className={styles.input}>
                            <SwitchTextareaField
                                name="orderFeedback"
                                label={i18next.t('settings_order_feedback_label')}
                                placeholder={i18next.t('settings_order_feedback_placeholder')}
                            />

                            <SwitchInputField
                                name="orderEmail"
                                label={i18next.t('settings_order_email_label')}
                                placeholder={i18next.t('settings_order_email_placeholder')}
                            />
                        </div>

                    </div>


                </div>
            }


        </div>
    )
}

const mapStateToProps = state => ({
    settingsOrdersSwitchOn: state.profile.settingsOrdersSwitchOn,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const OrdersSwitch = connect(mapStateToProps, mapDispatchToProps)(OrdersSwitchPure)

