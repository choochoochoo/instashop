import React from 'react'
import styles from './SettingsOrders.css'
import i18next from 'i18next';
import {ClosePanel, PhoneTemplate} from '../common'

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import * as actions from '../../actions';
import store from '../../store.js';
import {profilable} from '../hocs'
import {SettingsOrdersForm} from './SettingsOrdersForm.jsx'

import {editUserUni} from '../../api';
import * as notificaitonTypes from '../../constants/NotificationTypes'

export const SettingsOrdersPure = ({
                                    match,
                                    history,
                                    accountName,
                                    profile,
                                    userId,
                                    settingsOrdersSwitchOn,
                                    email,
                                }) => {

    const handleSubmit = (values) => {
        store.dispatch(actions.showProgress());
        store.dispatch(actions.userEditStart())

        editUserUni(userId, {
            settingsOrdersSwitchOn: settingsOrdersSwitchOn,
            orderFeedback: values.orderFeedback,
            orderEmail: values.orderEmail,
        }, profile).then(response => {
            store.dispatch(actions.userEditSuccess(response.data));
            store.dispatch(actions.hideProgress());
            history.push(`/${accountName}`)

            store.dispatch(actions.showNotification(notificaitonTypes.SAVE));
            setTimeout(()=>{
                store.dispatch(actions.hideNotification());
            }, notificaitonTypes.TIMEOUT)

        }).catch(exception => {
            store.dispatch(actions.userEditError(exception));
            store.dispatch(actions.hideProgress());

            store.dispatch(actions.showNotification(notificaitonTypes.ERROR));
            setTimeout(()=>{
                store.dispatch(actions.hideNotification());
            }, notificaitonTypes.TIMEOUT)
        });
    }

    return (
        <PhoneTemplate>
            <div className={styles.root}>
                <ClosePanel
                    text={i18next.t('settings_orders')}
                    userId={accountName}
                    link={`/${accountName}/settings`}
                />

                <SettingsOrdersForm
                    onSubmit={handleSubmit}
                    email={email}
                />

                {
                    !settingsOrdersSwitchOn &&
                    <div className={styles.text}>
                        <div className={styles.text1}>
                            {i18next.t('settings_orders_switch_text_1')}
                        </div>
                        <div className={styles.text2}>
                            {i18next.t('settings_orders_switch_text_2')}
                        </div>
                    </div>
                }

            </div>
        </PhoneTemplate>
    )
}


const mapStateToProps = state => ({
    userId: state.profile._id,
    accountName: state.profile.accountName,
    profile: state.profile,
    settingsOrdersSwitchOn: state.profile.settingsOrdersSwitchOn,
    email: state.profile.email,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const SettingsOrders = connect(mapStateToProps, mapDispatchToProps)(profilable(SettingsOrdersPure))