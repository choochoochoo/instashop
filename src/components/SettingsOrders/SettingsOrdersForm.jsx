import React from 'react'
import {Field, reduxForm} from 'redux-form'
import {connect} from "react-redux";
import {OrdersSwitch} from './OrdersSwitch.jsx'
import i18next from "i18next";
import styles from "./SettingsOrders.css";
import {Button} from "../common";

const validate = values => {
    const errors = {}

    // if (!values.orderFeedback) {
    //     errors.orderFeedback = i18next.t('input_field_required')
    // }

    return errors
}



let SettingsOrdersFormPure = props => {
    const {
        handleSubmit,
        email,
    } = props

    return (
        <div>
            <form onSubmit={handleSubmit}>
                <OrdersSwitch/>

                <div className={styles.notificationText}>
                    {i18next.t('settings_orders_notification_text')} <br/>
                </div>

                <div className={styles.button}>
                    <Button title={i18next.t('profile_submit_save')} typeStyle={'white'} />
                </div>
            </form>
        </div>
    )
}

const getOrderFeedback = (profile) => {
    const orderFeedback =
            profile.orderFeedback ?
                profile.orderFeedback :
                i18next.t('settings_order_feedback_default_text')

    return orderFeedback
}

const getOrderEmail = (profile) => {
    const orderEmail =
        profile.orderEmail ?
            profile.orderEmail :
            profile.email

    return orderEmail
}

SettingsOrdersFormPure = reduxForm({
    form: 'SettingsOrdersForm',
    validate,
    enableReinitialize: true,
})(SettingsOrdersFormPure)

export const SettingsOrdersForm = connect(
    (state, ownParam) => {


        return {
             initialValues: {
                 settingsOrdersSwitchOn: state.profile.settingsOrdersSwitchOn,
                     orderFeedback: getOrderFeedback(state.profile),
                     orderEmail: getOrderEmail(state.profile),
             }
        }
    }
)(SettingsOrdersFormPure)
