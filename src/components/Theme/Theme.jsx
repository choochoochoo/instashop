import React from 'react'
import {Link} from 'react-router-dom';
import i18next from 'i18next';

import styles from './Theme.css'
import {ClosePanel, Subtitle, PhoneTemplate} from '../common'
import {ThemeSelector} from './ThemeSelector.jsx'

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import * as actions from '../../actions';
import store from '../../store.js';
import {profilable} from '../hocs'


export const ThemePure = ({match, history, accountName}) => {
    const userId = match.params.userId

    const subtitle = (
        <div className={styles.subtitle}>
            {i18next.t('change_theme_text')}
        </div>
    )

    return (
        <PhoneTemplate>
            <div className={styles.root}>
                <ClosePanel
                    text={i18next.t('change_theme_menu')}
                    userId={userId}
                    buttons={subtitle}
                    link={`/${accountName}/settings`}
                />
                <ThemeSelector
                    history={history}
                    match={match}
                />
            </div>
        </PhoneTemplate>
    )
}

const mapStateToProps = state => ({
    userId: state.profile._id,
    accountName: state.profile.accountName,
    profile: state.profile,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const Theme = connect(mapStateToProps, mapDispatchToProps)(profilable(ThemePure))