import React from 'react'
import {Link} from 'react-router-dom';
import i18next from 'i18next';

import styles from './ThemeSelector.css'
import themes from './themes.json'
import * as res from '../../resources'

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as actions from '../../actions';
import store from '../../store.js';
import {editUserUni} from '../../api';
import {profilable} from '../hocs'
import {SvgIcon, Button} from '../common'

import * as notificaitonTypes from '../../constants/NotificationTypes'

const themesIcons = {
    'navy-blue': 'themeNavyBlue',
    'cloud': 'themeCloud',
    'steve-jobs': 'themeSteveJobs',
}

export const ThemeSelectorPure = ({ userId, history, profile, match, selectThemeForm, accountName}) => {

    const saveClick = () => {
        store.dispatch(actions.showProgress());
        store.dispatch(actions.userEditStart())

        editUserUni(userId, {
            theme: selectThemeForm,
        }, profile).then(response => {
            store.dispatch(actions.userEditSuccess(response.data));
            store.dispatch(actions.changeTheme(selectThemeForm));
            store.dispatch(actions.hideProgress());
            history.push(`/${accountName}`)

            store.dispatch(actions.showNotification(notificaitonTypes.SAVE));
            setTimeout(()=>{
                store.dispatch(actions.hideNotification());
            }, notificaitonTypes.TIMEOUT)

        }).catch(exception => {
            store.dispatch(actions.userEditError(exception));
            store.dispatch(actions.hideProgress());

            store.dispatch(actions.showNotification(notificaitonTypes.ERROR));
            setTimeout(()=>{
                store.dispatch(actions.hideNotification());
            }, notificaitonTypes.TIMEOUT)
        });
    }

    const themeClick = (theme) => {
        store.dispatch(actions.selectThemeForm(theme));
    }

    return (
        <div className={styles.root}>
            <div className={styles.buttonPanel}>
                {
                    Object.keys(themes).map((key) => {
                        let theme = themes[key]

                        let isCheckedTheme = key === selectThemeForm

                        let styleTitle = {
                            fontFamily: theme.common_font,
                            color: theme['common_title_text-color'],
                        }

                        let lineStyle = {
                            borderTopColor: isCheckedTheme ? '#232c30' : '#d6dce3'
                        }

                        return (
                            <button
                                className={styles.button}
                                onClick={themeClick.bind(null, key)}
                                key={key}
                            >
                                <div className={styles.titleRow}>
                                    <SvgIcon icon={themesIcons[key]} />
                                    <div className={styles.title} style={styleTitle}>
                                        {theme.name}
                                    </div>
                                </div>
                                <hr className={styles.line} style={lineStyle}/>
                                {
                                    isCheckedTheme &&
                                    <div className={styles.checkOk}>
                                        <SvgIcon icon="checkOk" />
                                    </div>
                                }
                            </button>
                        )
                    })
                }
            </div>
            <Button
                title={i18next.t('profile_submit_save')}
                typeStyle="white"
                type="button"
                onClick={saveClick}
            />
        </div>
    )
}

const mapStateToProps = state => ({
    profile: state.profile,
    userId: state.profile._id,
    accountName: state.profile.accountName,
    selectThemeForm: state.global.selectThemeForm,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch),
})

export const ThemeSelector = connect(mapStateToProps, mapDispatchToProps)(profilable(ThemeSelectorPure))