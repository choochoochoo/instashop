import React, {PropTypes} from 'react'
import styles from './AddCategoryButton.css'
import {SvgIcon} from '../index'
import i18next from 'i18next'

export const AddCategoryButton = ({
                                 theme,
                                 click,
                             }) => {
    return (
        <div className={styles.root}>
            <button className={styles.button} onClick={click}>
                <div className={styles.title}>
                    {i18next.t('categories_add_new')}
                </div>
                <div className={styles.plus}>
                    <SvgIcon icon="plus"/>
                </div>
            </button>
        </div>
    )
}
