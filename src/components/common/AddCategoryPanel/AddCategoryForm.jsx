import React from 'react'
import {Field, reduxForm} from 'redux-form'
import {connect} from "react-redux";
import i18next from "i18next";
import {InputField} from './InputField.jsx'


const validate = values => {
    const errors = {}
    const max_sym = 20

    if (!values.title) {
        errors.title =  i18next.t('input_field_required')
    }


    if (values.title && values.title.length > max_sym) {
        errors.title =  `${max_sym} ${i18next.t('categories_input_field_max_length')}`
    }

    return errors
}



let AddCategoryFormPure = props => {
    const {
        handleSubmit,
    } = props

    return (
        <div>
            <form onSubmit={handleSubmit}>
                <InputField
                    name="title"
                    placeholder={i18next.t('categories_add_new_placeholder')}
                />

                {/*<div>*/}
                    {/*<Button title={i18next.t('profile_submit_save')} />*/}
                {/*</div>*/}
            </form>
        </div>
    )
}

AddCategoryFormPure = reduxForm({
    form: 'ConnectionsForm',
    validate,
    enableReinitialize: true,
})(AddCategoryFormPure)

export const AddCategoryForm = connect(
    (state, ownParam) => {
        return {
             initialValues: {
             }
        }
    }
)(AddCategoryFormPure)
