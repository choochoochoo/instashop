import React from 'react'
import i18next from 'i18next';

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import * as actions from '../../../actions';
import store from '../../../store.js';
import {createCategory} from "../../../api";
import * as notificaitonTypes from "../../../constants/NotificationTypes";

import {AddCategoryForm} from './AddCategoryForm.jsx'
import {AddCategoryButton} from "./AddCategoryButton.jsx";

export const AddCategoryPanelPure = ({
                                   match,
                                   history,
                                   accountName,
                                   userId,
                                   hideAddCategoryButton,
                               }) => {


    const handleSubmitCreate = (values) => {

        store.dispatch(actions.showProgress());
        store.dispatch(actions.categoryCreateStart());

        createCategory({
            title: values.title,
            userId: userId,
        }).then(response => {
            store.dispatch(actions.categoryCreateSuccess(response.data));
            store.dispatch(actions.hideProgress());
            //store.dispatch(actions.clearCreateSection());
            //history.push(`/${accountName}`)

            store.dispatch(actions.showNotification(notificaitonTypes.SAVE));
            setTimeout(()=>{
                store.dispatch(actions.hideNotification());
            }, notificaitonTypes.TIMEOUT)
        }).catch(exception => {
            store.dispatch(actions.categoryCreateError(exception));
            store.dispatch(actions.hideProgress());

            store.dispatch(actions.showNotification(notificaitonTypes.ERROR));
            setTimeout(()=>{
                store.dispatch(actions.hideNotification());
            }, notificaitonTypes.TIMEOUT)
        });
    }


    const addCategoryButtonClick = () => {
        store.dispatch(actions.hideAddCategoryButton());
    }

    return (
        <div>
            {
                hideAddCategoryButton && <AddCategoryForm onSubmit={handleSubmitCreate} />
            }
            {
                !hideAddCategoryButton &&  <AddCategoryButton click={addCategoryButtonClick}/>
            }
        </div>
    )
}


const mapStateToProps = state => ({
    userId: state.profile._id,
    accountName: state.profile.accountName,
    profile: state.profile,
    hideAddCategoryButton: state.global.hideAddCategoryButton,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const AddCategoryPanel = connect(mapStateToProps, mapDispatchToProps)(AddCategoryPanelPure)