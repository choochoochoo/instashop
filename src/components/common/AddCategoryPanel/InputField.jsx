import React from 'react'
import {Link} from 'react-router-dom';
import i18next from 'i18next';
import {Field, reduxForm} from 'redux-form'

import styles from './InputField.css'

const classNames = require('classnames');

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import * as actions from '../../../actions/index';
import store from '../../../store.js';


const renderField = ({
                         input,
                         placeholder,
                         type,
                         accept,
                         meta: {touched, error, warning},
                         label,
                         theme,
                     }) => {

    const styleError = touched && error ? {
        backgroundColor: 'rgba(245, 113, 35, 0)',
        border: 'solid 1.5px #f57123',
    } : {}

    const styleErrorText = touched && error ? {
        color: '#f57123',
        fontSize: '13px',
    } : {}

    const buttonText = (touched && error) ? error : i18next.t('categories_add_new_button')

    return (
        <div className={styles.inputWrapper}>
            <input
                {...input}
                placeholder={placeholder}
                type={type}
                accept={accept}
                className={styles.input}
                style={{
                    color: theme['common_field_text-color']
                }}
            />
            <button type="submit" className={styles.button} style={styleError}>
                <div className={styles.buttonText} disabled={touched && error} style={styleErrorText}>
                    {buttonText}
                </div>
            </button>
        </div>
    )
}

export const InputFieldPure = ({name, type, placeholder, label, theme}) => {
    return (
        <div className={styles.root}>
            <Field
                name={name}
                type={type}
                placeholder={placeholder}
                component={renderField}
                label={label}
                theme={theme}
            />
        </div>
    )
}

const mapStateToProps = state => ({
    theme: state.theme
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const InputField = connect(mapStateToProps, mapDispatchToProps)(InputFieldPure)