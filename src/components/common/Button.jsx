import React from 'react'
import {Link} from 'react-router-dom';
import {Field, reduxForm} from 'redux-form'

import styles from './Button.css'

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import * as actions from '../../actions';
import store from '../../store.js';

const classNames = require('classnames');

export const ButtonPure = ({
                           title,
                           onClick,
                           type = 'submit',
                           typeStyle='',
                            theme,
                            className,
                        }) => {
    return (
        <div className={classNames(styles.root, styles[typeStyle], className)}>
            <button type={type} className={styles.button} onClick={onClick} style={{
                backgroundColor: theme['common_active-button_background-color'],
            }}>
                <span className={styles.buttonText}>{title}</span>
             </button>
        </div>
    )
}

const mapStateToProps = state => ({
    theme: state.theme
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const Button = connect(mapStateToProps, mapDispatchToProps)(ButtonPure)



