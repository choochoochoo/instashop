import React, {PropTypes} from 'react'
import styles from './CartItem.css'
import {getFile} from "../../../api";
import {defaultProductImage} from "../../../resources";
import i18next from 'i18next'

export const CartItem = ({
                                product,
                            }) => {

    let image = null
    if(product.photo && product.photo.size1080){
        image = product.photo.size1080
    }else{
        image = product.photo
    }

    const photo = image ? getFile(image) : `/${defaultProductImage}`

    return (
        <div className={styles.root}>
            <div  className={styles.leftColImage}>
                <div >
                    <div className={styles.photoWrapper}>
                        <img src={photo} width="100%"/>
                    </div>
                </div>

                <div className={styles.rightCol}>

                    <div className={styles.title}>{product.title}</div>
                    <div className={styles.price}>{product.price}</div>
                </div>
            </div>
            <div className={styles.rightColImage}>
                 {`${product.count} ${i18next.t('cart_item_count_title')}`}
            </div>
        </div>
    )
}
