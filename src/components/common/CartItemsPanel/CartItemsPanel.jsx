import React, {PropTypes} from 'react'
import styles from './CartItemsPanel.css'

import {CartItem} from './CartItem.jsx'

export const CartItemsPanel = ({items}) => {
    return (
        <div className={styles.root}>
                {
                    items.map((item, index) => {
                        return (
                            <CartItem
                                key={item._id}
                                product={item}
                            />
                        )
                    })
                }
        </div>
    )
}
