import React, {PropTypes} from 'react'
import styles from './CategoriesPanel.css'

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as actions from '../../../actions/index';
import store from '../../../store.js';
import i18next from 'i18next'

import {CategoryItem} from './CategoryItem.jsx'

export const CategoriesPanelPure = ({
                                categories,
                                theme,
                                selectedCategory,
                                allProducts,
                                hideAllCategorySwitchOn,
                                allCategoryStart,
                                allCategoryEnd,
                                categoryAllClick,
                                categoryItemClick,
                            }) => {
    return (
        <nav className={styles.root}>
            <div className={styles.scroll}>
                {
                    hideAllCategorySwitchOn && allCategoryStart && <CategoryItem
                        title={i18next.t('category_all_title')}
                        theme={theme}
                        click={categoryAllClick}
                        selected={selectedCategory === 'all'}
                    />
                }
                {
                    categories.map((item, index) => {
                        return (
                            <CategoryItem
                                key={item._id}
                                title={item.title}
                                theme={theme}
                                selected={item._id === selectedCategory }
                                click={categoryItemClick.bind(null, item)}
                            />
                        )
                    })
                }
                {
                    hideAllCategorySwitchOn && allCategoryEnd && <CategoryItem
                        title={i18next.t('category_all_title')}
                        theme={theme}
                        click={categoryAllClick}
                        selected={selectedCategory === 'all'}
                    />
                }
            </div>
        </nav>
    )
}

const mapStateToProps = state => ({
    theme: state.theme,
    hideAllCategorySwitchOn: state.profile.hideAllCategorySwitchOn,
    allCategoryStart: state.profile.allCategoryStart,
    allCategoryEnd: state.profile.allCategoryEnd,
    allProducts: state.profile.allProducts,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const CategoriesPanel = connect(mapStateToProps, mapDispatchToProps)(CategoriesPanelPure)