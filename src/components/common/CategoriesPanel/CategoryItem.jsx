import React, {PropTypes} from 'react'
import styles from './CategoryItem.css'

const classNames = require('classnames');


export const CategoryItem = ({
                                title,
                                theme,
                                selected,
                                click,
                            }) => {

    const styleobj = { }
    styleobj[`${styles.button}`] = true
    styleobj[`${styles.selected}`] = selected

    return (
        <button className={classNames(styleobj)} onClick={click}>
            <span className={styles.title}>
                {title}
            </span>
        </button>
    )
}
