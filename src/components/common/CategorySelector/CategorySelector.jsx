import React from 'react'
import {Link} from 'react-router-dom';
import {Field, reduxForm} from 'redux-form'
import i18next from 'i18next'

import styles from './CategorySelector.css'
import {bindActionCreators} from "redux";
import * as actions from "../../../actions";
import connect from "react-redux/es/connect/connect";

import {CategorySelectorHeader} from './CategorySelectorHeader.jsx'
import {CategorySelectorItem} from './CategorySelectorItem.jsx'
import store from "../../../store";
import {AddCategoryPanel} from "../AddCategoryPanel/AddCategoryPanel.jsx";

export const CategorySelectorPure = ({
                                         visible,
                                         categories,
                                    }) => {

    const closeClick = () => {
        store.dispatch(actions.closeCategorySelector())
    }

    const checkboxClick = (category) => {

        if(category.checked){
            store.dispatch(actions.checkboxOffCategorySelector(category))
        }else{
            store.dispatch(actions.checkboxOnCategorySelector(category))
        }

    }

    return (
        <div className={styles.modal}  style={{
            opacity: visible ? 1 : 0,
            visibility: visible ? 'visible' : 'hidden'
        }}>
            <button className={styles.back} onClick={closeClick}/>
            <div className={styles.root}>
                <CategorySelectorHeader closeClick={closeClick} />
                {
                    categories && categories.map((item, index) => {
                        return (
                            <CategorySelectorItem
                                key={item._id}
                                title={item.title}
                                click={checkboxClick.bind(null, item)}
                                checked={item.checked}
                            />
                        )
                    })
                }

                <div className={styles.changePosition}>
                    {i18next.t('category_selector_change_position')}
                </div>
            </div>
        </div>
    )
}


const mapStateToProps = state => ({
    userId: state.profile._id,
    accountName: state.profile.accountName,
    theme: state.profile.theme,
    profile: state.profile,
    visible: state.global.openCategorySelector,
    categories: state.categorySelector.categories,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch),
})

export const CategorySelector = connect(mapStateToProps, mapDispatchToProps)(CategorySelectorPure)
