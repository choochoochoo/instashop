import React from 'react'
import styles from './CategorySelectorHeader.css'
import i18next from 'i18next'

export const CategorySelectorHeader = ({closeClick}) => {
    return (
        <div className={styles.root}>
            <div className={styles.title}>
                {i18next.t('category_selector_title')}
            </div>
            <div>
                <button className={styles.close} onClick={closeClick}>
                    {i18next.t('category_selector_close')}
                </button>
            </div>
        </div>
    )
}



