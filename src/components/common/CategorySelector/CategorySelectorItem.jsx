import React from 'react'
import {Link} from 'react-router-dom';
import {SvgIcon, Button} from '../index'

import styles from './CategorySelectorItem.css'
import {Checkbox} from './Checkbox.jsx'

export const CategorySelectorItem = ({title, click, checked}) => {

    return (
        <button className={styles.root} onClick={click}>
            <div className={styles.title}>
                {title}
            </div>
            <div className={styles.checkbox}>
                <Checkbox checked={checked}/>
            </div>
        </button>
    )
}



