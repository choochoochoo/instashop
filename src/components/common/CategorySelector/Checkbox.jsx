import React from 'react'
import {SvgIcon} from "../SvgIcon/SvgIcon.jsx";

export const Checkbox = ({
                            checked
                        }) => {
    return (
        <span>
            <SvgIcon icon={checked ? 'checkboxOn' : 'checkboxOff'} width="24" height="24"/>
        </span>
    )
}
