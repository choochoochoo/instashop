import React from 'react'
import styles from './ChangeCategoryButton.css'
import i18next from 'i18next';
import {SvgIcon} from '../common'
import store from "../../store";
import * as actions from "../../actions";
import {ChangeCategoryButtonItem} from "./ChangeCategoryButtonItem.jsx";


export let ChangeCategoryButton = props => {

    const {
        label,
        placeholder,
        click,
        categories,
    } = props

    return (
        <div>
            <button className={styles.editorView} type="button" onClick={click}>
                <div className={styles.label}>
                    {label}
                </div>

                <div className={styles.editorViewTextWrapper}>
                    <div className={styles.value}>
                        {
                            categories.length === 0 ? <div className={styles.categoryPlaceholder}>{placeholder}</div> : null
                        }
                        {
                            categories.map((item, index) => {

                                return (
                                    <ChangeCategoryButtonItem
                                        key={item._id}
                                        title={item.title}
                                    />
                                )
                            })
                        }
                    </div>
                    <div className={styles.arrow}>
                        <SvgIcon icon="arrow"/>
                    </div>
                </div>
            </button>
        </div>
    )
}