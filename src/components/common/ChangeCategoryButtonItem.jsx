import React from 'react'
import styles from './ChangeCategoryButtonItem.css'
import i18next from 'i18next';



export let ChangeCategoryButtonItem = props => {

    const {
        title,
    } = props

    return (
        <div className={styles.root}>
            <span className={styles.text}>
                {title}
            </span>
        </div>
    )
}