import React from 'react'
import {Link} from 'react-router-dom';
import {Field, reduxForm} from 'redux-form'
import i18next from 'i18next'

import styles from './ChangeImageMenu.css'
import {bindActionCreators} from "redux";
import * as actions from "../../../actions";
import connect from "react-redux/es/connect/connect";

import {ChangeImageMenuHeader} from './ChangeImageMenuHeader.jsx'
import {LoadImageItem} from './LoadImageItem.jsx'
import {EditImageItem} from './EditImageItem.jsx'
import store from "../../../store";

export const ChangeImageMenuPure = ({
                                         visible,
                                         originalPreview,
                                         crop,
                                        photoAccepted,
                                    }) => {

    const closeClick = () => {
        store.dispatch(actions.closeChangeImageMenu())
    }

    const editImageHandler = () => {
        store.dispatch(actions.openImageEditor(null, null, originalPreview, crop));
        store.dispatch(actions.closeChangeImageMenu())
    }

    return (
        <div className={styles.modal}  style={{
            opacity: visible ? 1 : 0,
            visibility: visible ? 'visible' : 'hidden'
        }}>
            <button
                className={styles.back}
                onClick={closeClick}
                type="button"
            />
            <div className={styles.root}>
                <ChangeImageMenuHeader closeClick={closeClick} />
                <LoadImageItem photoAccepted={photoAccepted} />
                { !!originalPreview && <EditImageItem click={editImageHandler} /> }
            </div>
        </div>
    )
}


const mapStateToProps = state => ({
    userId: state.profile._id,
    accountName: state.profile.accountName,
    theme: state.profile.theme,
    profile: state.profile,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch),
})

export const ChangeImageMenu = connect(mapStateToProps, mapDispatchToProps)(ChangeImageMenuPure)
