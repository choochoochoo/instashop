import React from 'react'
import styles from './ChangeImageMenuHeader.css'
import i18next from 'i18next'

export const ChangeImageMenuHeader = ({closeClick}) => {
    return (
        <div className={styles.root}>
            <div className={styles.title}>
                {i18next.t('change_image_menu_title')}
            </div>
            <div>
                <button className={styles.close} onClick={closeClick} type="button">
                    {i18next.t('category_selector_close')}
                </button>
            </div>
        </div>
    )
}



