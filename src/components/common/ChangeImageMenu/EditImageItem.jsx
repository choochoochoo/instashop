import React from 'react'
import {Field} from 'redux-form'
import {FileUploadField, EditorButton} from '../../common'
import i18next from 'i18next'
import styles from './EditImageItem.css'

export const EditImageItem = ({
                                     photoAccepted,
                                     name,
                                    click,
                                }) => {

    return (
        <div className={styles.root}>

                <div className={styles.content}>
                    <button className={styles.rootButton} type="button" onClick={click}>
                        <div className={styles.titleButton}>
                            {i18next.t('change_image_menu_edit_image_title')}
                        </div>
                    </button>
                </div>
        </div>
    )
}