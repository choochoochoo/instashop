import React from 'react'
import {Field} from 'redux-form'
import {FileUploadField, EditorButton} from '../../common'
import i18next from 'i18next'
// import {productUpload} from '../../resources'
import styles from './LoadImageItem.css'

export const LoadImageItem = ({
                                     photoAccepted,
                                     name,
                                }) => {

    return (
        <div className={styles.root}>
            <FileUploadField
                photoAccepted={photoAccepted}
                name={name}
                className={styles.dropzone}
            >
                <div className={styles.content}>
                    <button className={styles.rootButton} type="button">
                        <div className={styles.titleButton}>
                            {i18next.t('change_image_menu_new_image_title')}
                        </div>
                    </button>
                </div>
            </FileUploadField>
        </div>
    )
}