import React from 'react'
import {Link} from 'react-router-dom';
import i18next from 'i18next';

import styles from './ChangePosition.css'
import {CategoriesPanel, ClosePanel} from '../index'


import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as actions from '../../../actions/index';
import store from '../../../store.js';

import {ChangePositionGrid} from './ChangePositionGrid.jsx'

export const ChangePositionPure = ({
                                       match,
                                       history,
                                       userId,
                                       accountName,
                                       profile,
                                       visible,
                                       products,
                                       selectedPosition,
                                       productCategories,
                                       selectedCategory,
                                       productId,
                                    }) => {

    const style = {
        display: visible ? 'block' : 'none' ,
        visibility: visible ? 'visible' : 'hidden',
    }

    const click = ()=> {
        store.dispatch(actions.closeChangePositionPage());
    }

    const positionClick = (position) => {
        store.dispatch(actions.changePosition(position));
    }

    const categoryAllClick = () => {
        const allProducts = []
        store.dispatch(actions.selectCategoryChangePosition({products: allProducts, _id: 'all'}));
    }

    const categoryItemClick = (item) => {
         store.dispatch(actions.selectCategoryChangePosition(item));
    }

    return (
        <div className={styles.root} style={style}>
            <ClosePanel
                text={i18next.t('change_position_menu')}
                userId={accountName}
                click={click}
                isLink={false}
                border={false}
            />

            {
                productCategories.length > 0 &&
                <CategoriesPanel
                    categories={productCategories}
                    categoryAllClick={categoryAllClick}
                    categoryItemClick={categoryItemClick}
                    selectedCategory={selectedCategory}
                />
            }

            <ChangePositionGrid
                products={products}
                selectedPosition={selectedPosition}
                click={positionClick}
                productId={productId}
            />
        </div>
    )
}

const mapStateToProps = state => ({
    userId: state.profile._id,
    accountName: state.profile.accountName,
    theme: state.profile.theme,
    profile: state.profile,
    products: state.changePosition.products,
    selectedPosition: state.changePosition.selectedPosition,
    selectedCategory: state.changePosition.selectedCategory,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch),
})

export const ChangePosition = connect(mapStateToProps, mapDispatchToProps)(ChangePositionPure)