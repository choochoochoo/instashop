import React from 'react'
import {Link} from 'react-router-dom';
import i18next from 'i18next';

import styles from './ChangePosition.css'
import {CategoriesPanel, ClosePanel} from '../index'


import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as actions from '../../../actions/index';
import store from '../../../store.js';

import {ChangePositionGrid} from './ChangePositionGrid.jsx'

export const ChangePositionCategoryPure = ({
                                       match,
                                       history,
                                       userId,
                                       accountName,
                                       profile,
                                       visible,
                                       items,
                                       selectedPosition,
                                       itemId,
                                    }) => {

    const style = {
        display: visible ? 'block' : 'none' ,
        visibility: visible ? 'visible' : 'hidden',
    }

    const click = ()=> {
        store.dispatch(actions.closeChangePositionCategoryPage());
    }

    const positionClick = (position) => {
        store.dispatch(actions.changePositionCategory(position));
    }

    return (
        <div className={styles.root} style={style}>
            <ClosePanel
                text={i18next.t('change_position_menu')}
                userId={accountName}
                click={click}
                isLink={false}
                border={false}
            />

            <ChangePositionGrid
                products={items}
                selectedPosition={selectedPosition}
                click={positionClick}
                productId={itemId}
            />
        </div>
    )
}

const mapStateToProps = state => ({
    userId: state.profile._id,
    accountName: state.profile.accountName,
    theme: state.profile.theme,
    profile: state.profile,
    items: state.changePositionCategory.items,
    selectedPosition: state.changePositionCategory.selectedPosition,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch),
})

export const ChangePositionCategory = connect(mapStateToProps, mapDispatchToProps)(ChangePositionCategoryPure)