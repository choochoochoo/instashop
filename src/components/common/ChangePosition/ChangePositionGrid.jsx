import React from 'react'
import {Link} from 'react-router-dom';
import {Field, reduxForm} from 'redux-form'

import styles from './ChangePositionGrid.css'
import {ChangePositionGridItem} from './ChangePositionGridItem.jsx'

const isEven = (elements) => elements % 2 === 0

export const ChangePositionGrid = ({products, selectedPosition, click, productId}) => {

    return (
        <div className={styles.root}>
            {
                products.map((item, index) => {
                    return (
                        <ChangePositionGridItem
                            key={item._id}
                            number={index + 1}
                            title={item.title}
                            isChecked={item._id === productId}
                            click={click}
                        />
                    )
                })
            }
            {
                !isEven(products.length) && <div className={styles.fake}/>
            }
        </div>
    )
}
