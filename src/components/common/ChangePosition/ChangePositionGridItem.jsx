import React from 'react'
import {Link} from 'react-router-dom';
import {SvgIcon, Button} from '../index'
const classNames = require('classnames');

import styles from './ChangePositionGridItem.css'

export const ChangePositionGridItem = ({number, title, isChecked, click}) => {

    const styleobj = { }
    styleobj[`${styles.root}`] = true
    styleobj[`${styles.isChecked}`] = isChecked

    const clickHandler = () => {
        click(number)
    }

    return (
        <button className={classNames(styleobj)} onClick={clickHandler}>
            <div className={styles.wrap}>
                {
                    isChecked &&
                    <div className={styles.checkOk}>
                        <SvgIcon
                            icon="checkOk"
                            width={64}
                            height={64}
                            className={styles.isCheckedIcon}
                        />

                    </div>
                }
                {
                    !isChecked &&
                    <div className={styles.number}>
                        {number}
                    </div>
                }
                <div className={styles.title}>
                    {title}
                </div>
            </div>
        </button>
    )
}



