import React from 'react'
import styles from './ChangePositionButton.css'
import i18next from 'i18next';
import {SvgIcon} from '../common'



export let ChangePositionButton = props => {

    const {
        label,
        click,
        placeholder,
        position = 1,
    } = props

    return (
        <div>
            <button className={styles.editorView} type="button" onClick={click}>
                <div className={styles.label}>
                    {label}
                </div>

                <div className={styles.editorViewTextWrapper}>
                    <div className={styles.value}>
                        {placeholder ? placeholder : 1}
                    </div>
                    <div className={styles.arrow}>
                        <SvgIcon icon="arrow"/>
                    </div>
                </div>
            </button>
        </div>
    )
}