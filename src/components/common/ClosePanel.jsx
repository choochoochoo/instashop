import React from 'react'
import styles from './ClosePanel.css'
import {Link} from 'react-router-dom';
import {SvgIcon} from './SvgIcon/SvgIcon.jsx'
const _ = require('lodash')
const classNames = require('classnames');

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import * as actions from '../../actions';
import store from '../../store.js';

const closeSvgs = {
    'navy-blue': 'closeSvgNavyBlue',
    'cloud': 'closeSvgCloud',
    'steve-jobs': 'closeSvgSteveJobs',
}

export const ClosePanelPure = ({
                                   text,
                                   userId,
                                   click,
                                   theme,
                                   buttons = null,
                                   isLink = true,
                                   type="",
                                   border = true,
                                   link = null,
                                   titlePanel = null,
                                   showClose = true,
                               }) => {

    const onClick =() => {
        click ? click() : _.noop()
    }

    const textStyle = {
        color: theme['common_title_text-color']
    }

    const closeStyle = styles[closeSvgs[theme.key]]

    const newLink = link ? link : `/${userId}`

    return (
        <div className={classNames(styles.root, styles[type])} style={{
            borderBottom: border ? 'solid 1px #d6dce3' : 'none'
        }}>
            <div className={styles.mainPanel}>
                { text && <div className={styles.text} style={textStyle}>
                    {text}
                </div> }
                <div>
                    {titlePanel}
                </div>
                { showClose && <div>
                    <button onClick={onClick} className={styles.close}>
                        {
                            isLink && (
                                <Link to={newLink} >
                                    <SvgIcon
                                        icon="close"
                                        width={64}
                                        height={64}
                                        className={closeStyle}
                                    />
                                </Link>
                            )
                        }
                        {
                            !isLink && (
                                <SvgIcon
                                    icon="close"
                                    width={64}
                                    height={64}
                                    className={closeStyle}
                                />
                            )
                        }

                    </button>
                </div> }

            </div>
            {
                buttons &&
                <div className={styles.extButtons}>
                    {buttons}
                </div>
            }
        </div>
    )
}

const mapStateToProps = state => ({
    theme: state.theme,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch),
})

export const ClosePanel = connect(mapStateToProps, mapDispatchToProps)(ClosePanelPure)

