import React from 'react'
import {Link} from 'react-router-dom';
import {Field, reduxForm} from 'redux-form'

import styles from './ConfirmationButton.css'

export const ConfirmationButton = ({title, click}) => {
    return (
        <div className={styles.root}>
            <button type="button" className={styles.button} onClick={click}>
                <span className={styles.buttonText}>{title}</span>
             </button>
        </div>
    )
}



