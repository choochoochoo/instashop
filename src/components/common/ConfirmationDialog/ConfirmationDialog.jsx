import React from 'react'
import {Link} from 'react-router-dom';
import {Field, reduxForm} from 'redux-form'

import styles from './ConfirmationDialog.css'

import {ConfirmationButton} from './ConfirmationButton.jsx'

export const ConfirmationDialog = ({
                                       text,
                                       okClick,
                                       cancelClick,
                                       okTitle,
                                       cancelTitle,
                                       opened,
                                    }) => {
    return (
        <div className={styles.modal} style={{
            opacity: opened ? 1 : 0,
            visibility: opened ? 'visible' : 'hidden'
        }}>
            <div className={styles.root}>
                <div className={styles.textWrapper}>
                    <div className={styles.text}>
                        {text}
                    </div>
                </div>
                <div className={styles.buttonPanel}>
                    <ConfirmationButton
                        title={okTitle}
                        click={okClick}
                    />

                    <ConfirmationButton
                        title={cancelTitle}
                        click={cancelClick}
                    />
                </div>
            </div>
        </div>
    )
}



