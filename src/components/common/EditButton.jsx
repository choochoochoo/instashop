import React from 'react'
import {Link} from 'react-router-dom';
import {Field, reduxForm} from 'redux-form'
const classNames = require('classnames');
import styles from './EditButton.css'

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import * as actions from '../../actions';
import store from '../../store.js';

export const EditButtonPure = ({title, link, className="", theme}) => {
    return (
        <Link to={link} className={styles.link}>
            <button
                className={classNames(styles.button, className)}
                type="button"
                style={{
                    backgroundColor: theme['common_active-button_background-color'],
                }}
            >
                <span className={styles.text}>
                    {title}
                </span>
            </button>
        </Link>
    )
}

const mapStateToProps = state => ({
    theme: state.theme
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const EditButton = connect(mapStateToProps, mapDispatchToProps)(EditButtonPure)