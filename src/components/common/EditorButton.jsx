import React from 'react'
import {Link} from 'react-router-dom';
import styles from './EditorButton.css'
const classNames = require('classnames');
import _ from 'lodash'

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import * as actions from '../../actions';
import store from '../../store.js';

export const EditorButtonPure = ({text, onClick = _.noop, className = {}, type='', theme}) => {
    return (
        <span className={classNames(styles.root, className)}>
            <button
                type='button'
                onClick={onClick}
                className={classNames(styles.button, styles[type])}
                style={{
                    backgroundColor: theme['common_active-button_background-color'],
                }}
            >
                <span className={styles.buttonText}>{text}</span>
             </button>
        </span>
    )
}

const mapStateToProps = state => ({
    theme: state.theme
})

const mapDispatchToProps = dispatch => ({

})

export const EditorButton = connect(mapStateToProps, mapDispatchToProps)(EditorButtonPure)




