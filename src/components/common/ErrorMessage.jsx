import React from 'react'
import {Link} from 'react-router-dom';
import {Field, reduxForm} from 'redux-form'

import styles from './ErrorMessage.css'

export const ErrorMessage = ({text, visible}) => {

    const style = {
        visibility: visible ? 'visible' : 'hidden',
    }

    return (
        <div className={styles.root} style={style}>
            {text}
        </div>
    )
}



