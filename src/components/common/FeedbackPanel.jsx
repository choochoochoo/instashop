import React from 'react'
import {Link} from 'react-router-dom';
import {Field, reduxForm} from 'redux-form'
import i18next from 'i18next';
import styles from './FeedbackPanel.css'

const feedbackLink = 'https://www.facebook.com/blablabla'
const feedbackLinkText = 'www.facebook.com/blablabla'

export const FeedbackPanel = ({text}) => {
    return (
        <div className={styles.root}>
            <div className={styles.text}>
                {text}
            </div>
            <a href={feedbackLink} className={styles.link}>
                {feedbackLinkText}
            </a>
        </div>
    )
}



