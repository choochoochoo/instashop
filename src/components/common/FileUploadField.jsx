import React from 'react'
import {Field} from 'redux-form'
import Dropzone from 'react-dropzone'
import {
    resizeAndOptimize,
    getImage,
    TARGET_WIDTH,
    TARGET_SIZE,
    getRotationData,
    rotate,
} from '../../utils/image'

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import * as actions from '../../actions';
import store from '../../store.js';

class FileInput extends React.Component {

    state: {
        imgUrl: {},
    }

    constructor(props) {
        super(props)
        // this.onChange = this.onChange.bind(this)
        this.onPhotoAccepted = this.onPhotoAccepted.bind(this)
    }

    onChange(files) {
        store.dispatch(actions.showProgress());

        if (this.props.input) {
            const {input: {onChange}} = this.props
            onChange(files[0])
        }
        else if(this.props.onChange){
            this.props.onChange(files[0])
        }
        else{
            console.warn('redux-form-dropzone => Forgot to pass onChange props ?');
        }
    }

    onPhotoAccepted(files){
        const file = files[0];

        getImage(file.preview).then(image => {
            if(file.size > TARGET_SIZE) {
                resizeAndOptimize(image, file)
                    .then(result => {
                        getImage(result.preview).then(imageNew => {
                            getRotationData(file.preview).then(rotateData => {
                                if(rotateData.rotate !== 0) {
                                    rotate(imageNew, rotateData.rotate, file.name).then(res => {
                                        // this.setState({
                                        //     imgUrl: res.preview
                                        // })

                                        this.props.photoAccepted(res)
                                        store.dispatch(actions.hideProgress());
                                    })
                                }else{
                                    this.props.photoAccepted(result)
                                    store.dispatch(actions.hideProgress());
                                }
                            }).catch((error)=>{
                                console.log(error)
                                result.imageSrc = imageNew.preview;
                                this.props.photoAccepted(result)
                                store.dispatch(actions.hideProgress());
                            })

                        })
                    })
            }else{
                file.imageSrc = image
                this.props.photoAccepted({
                    file,
                    preview: file.preview,
                    name: file.name,
                })
                store.dispatch(actions.hideProgress());
            }
        })
    }

    onDropRejected(){
        store.dispatch(actions.hideProgress());
    }

    render() {
        return (
            <div>
                <Dropzone
                    // onDrop={ this.onChange }
                    onDropAccepted={this.onPhotoAccepted}
                    onDropRejected={this.onDropRejected}
                    accept="image/jpeg,image/png"
                    name={this.props.name}
                    multiple={false}
                    className={this.props.className}
                >
                    {this.props.children}
                </Dropzone>
            </div>
        )
    }
}



const mapStateToProps = state => ({

})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const FileUploadField = connect(mapStateToProps, mapDispatchToProps)(FileInput)