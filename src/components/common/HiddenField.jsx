import React from 'react'
import {Link} from 'react-router-dom';
import i18next from 'i18next';
import {Field, reduxForm} from 'redux-form'

const renderField = ({
                         input,
                         placeholder,
                         type,
                         accept,
                         meta: {touched, error, warning},
                         value,
                     }) => (
        <input
            {...input}
            placeholder={placeholder}
            type={type}
            accept={accept}
            value={value}
        />
)

export const HiddenField = ({name}) => {
    return (
        <Field
            name={name}
            type={"hidden"}
            component={renderField}
        />
    )
}