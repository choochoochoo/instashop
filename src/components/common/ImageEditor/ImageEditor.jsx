import React from 'react'
import ReactDOM from 'react-dom'
import ReactCrop, { makeAspectCrop, getPixelCrop } from '../../../../ext/react-image-crop/index'

import styles from './ImageEditor.css'

import {Button} from '../Button.jsx'
import i18next from "i18next";

import {getCroppedImage, getCropInit} from '../../../utils/image'
import {cropphotowidth, cropphotoheight, cropaim} from '../../../resources/index'
import {SvgIcon} from '../SvgIcon/SvgIcon.jsx'
import {ImageEditorPanel} from './ImageEditorPanel.jsx'

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
 import * as actions from '../../../actions';
import store from '../../../store.js';

export class ImageEditorPure extends React.Component {
    state = {
        src: null,
        imageObj: null,
    }

    onImageLoaded = image => {
        if(this.props.crop){
            this.setState({
                crop: this.props.crop,
                imageObj: image,
            })
        }else{
            this.setState({
                crop: getCropInit(image),
                imageObj: image,
            })
        }
    }

    onCropComplete = crop => {
        console.log('onCropComplete', crop)
    }

    onCropChange = crop => {
        this.setState({ crop })
    }

    onCropPhoto = (crop) => {
        const imageSrc = this.state.imageObj
        const pixelCrop = getPixelCrop(imageSrc, this.state.crop)

        getCroppedImage(imageSrc, pixelCrop, 'croppedImage').then((result) => {
            store.dispatch(actions.cropPhoto(
                {
                    crop: this.state.crop,
                    pixelCrop,
                    croppedImageFile: result,
                },
                {
                    acceptedFile: this.props.acceptedFile,
                    acceptedFileName: this.props.acceptedFileName,
                    acceptedFilePreview: this.props.acceptedFilePreview,
                }
            ));

            store.dispatch(actions.closeImageEditor());
        });
    }

    handleClose = () => {
        store.dispatch(actions.closeImageEditor());
    }

    render() {
        const visible = this.props.visible
        const preview = this.props.acceptedFilePreview

        const style = {
            display: visible ? 'block' : 'none' ,
            visibility: visible ? 'visible' : 'hidden',
        }

        const cropAimStyle = {
            backgroundImage: `url(/${cropaim})`,
        }

        return (
            <div className={styles.root} style={style}>
                <ImageEditorPanel
                    clickCancel={this.handleClose}
                    clickNext={this.onCropPhoto}
                />
                <div className={styles.reactCrop}>
                    <ReactCrop
                        //src={`/${cropphotoheight}`}
                        // src={image && image.preview}
                        src={preview}
                        crop={this.state.crop}
                        onImageLoaded={this.onImageLoaded}
                        onComplete={this.onCropComplete}
                        onChange={this.onCropChange}
                        cropAimStyle={cropAimStyle}
                    />
                </div>
            </div>
        )
    }
}


const mapStateToProps = state => ({
    acceptedFile: state.imageEditor.acceptedFile,
    acceptedFileName: state.imageEditor.acceptedFileName,
    acceptedFilePreview: state.imageEditor.acceptedFilePreview,
    crop: state.imageEditor.crop,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch),
})

export const ImageEditor = connect(mapStateToProps, mapDispatchToProps)(ImageEditorPure)