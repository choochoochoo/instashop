import React from 'react'
import {Link} from 'react-router-dom';
import i18next from 'i18next';
import {Field, reduxForm} from 'redux-form'

import styles from './ImageEditorPanel.css'


export const ImageEditorPanel = ({clickNext, clickCancel}) => {
    return (
        <div className={styles.root}>
            <button className={styles.buttonCancel} onClick={clickCancel}>
                {i18next.t('image_editor_button_cancel')}
            </button>

            <button className={styles.buttonNext} onClick={clickNext}>
                {i18next.t('image_editor_button_next')}
            </button>
        </div>
    )
}