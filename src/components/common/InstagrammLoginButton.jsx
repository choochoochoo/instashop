import React from 'react'
import {SvgIcon} from '../common'
import i18next from 'i18next'
import styles from './InstagrammLoginButton.css'
import {instagrammLogo} from '../../resources'

const LOGIN_URL = "https://api.instagram.com/oauth/authorize/?client_id=98d6fab199c54f3abeb0b3ec1c9c3229&redirect_uri=https://flicky.io/api/users/login/instagramm/&response_type=code"


export const InstagrammLoginButton = ({title}) => {

    return (

        <a className={styles.root} href={LOGIN_URL}>
            <div className={styles.leftCol}>
                <div className={styles.logo}>
                    <img src={instagrammLogo} width="32px" height="32px"/>
                </div>
                <div className={styles.title}>
                    {title}
                </div>
            </div>
            <div>
                <SvgIcon icon="arrow"/>
            </div>
        </a>
    )
}
