import React from 'react'
import {Link} from 'react-router-dom';

import styles from './LinkButton.css'
import i18next from "i18next";
const classNames = require('classnames');

export const LinkButton = ({title, type='standart', link, className}) => {


    return (
        <div className={className}>
            <Link to={link} className={classNames(styles.button, styles[type])}>
                <span className={classNames(styles.buttonText, styles[`${type}-button-text`])}>
                    {title}
                </span>
            </Link>
        </div>
    )
}



