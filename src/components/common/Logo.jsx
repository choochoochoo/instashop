import React from 'react'
import {Link} from 'react-router-dom';
import {Field, reduxForm} from 'redux-form'

import styles from './Logo.css'

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import store from '../../store.js';

export const LogoPure = ({icon, theme}) => {
    return (
        <div className={styles.logoWrapper}>
            <img
                src={icon}
                className={styles.productLogo}
                style={{
                    borderColor: theme['main_header-logo-first-circle_border-color']
                }}
            />
        </div>
    )
}


const mapStateToProps = state => ({
    theme: state.theme
})

const mapDispatchToProps = dispatch => ({

})

export const Logo = connect(mapStateToProps, mapDispatchToProps)(LogoPure)

