import React from 'react'
import styles from './MapField.css'
import i18next from 'i18next';

import {
    MapView,
} from './MapView.jsx'

import {
    SvgIcon,
} from '../SvgIcon/SvgIcon.jsx'

export let MapField = props => {

    const {
        label,
        placeholder,
        latitude,
        longitude,
        buttons,
    } = props

    const hasMap = () => {
        return latitude && longitude
    }

    console.log(buttons)

    return (
        <div className={styles.root}>
            <div className={styles.label}>
                {label}
            </div>

            <div className={styles.mapWrapper}>
                {
                    buttons
                }
                <div className={styles.map}>
                    <MapView
                        lat={parseFloat(latitude)}
                        lng={parseFloat(longitude)}
                        width="100%"
                        height="166px"
                    />
                    {
                        !hasMap() && (
                            <div className={styles.mapCover}>
                                <div className={styles.icon}>
                                    <SvgIcon icon="mapMarker"/>
                                </div>
                                <div className={styles.placeholder}>
                                    {placeholder}
                                </div>
                            </div>
                        )
                    }
                </div>
            </div>
        </div>
    )
}