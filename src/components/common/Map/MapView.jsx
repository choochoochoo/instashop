import React from 'react'
import styles from './MapView.css'
import i18next from 'i18next';
import {loadScript} from './mapUtil.js'

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as actions from '../../../actions';
import store from '../../../store.js';

import * as mapScriptState from '../../../constants/MapScriptState.js';

const API_KEY = 'AIzaSyBBgPWzlHe3-Iu-OCRzMXYjcweoKfZ_7pc';

export const LoadingCircle = () => {
    return <div>Loading</div>
}

export class MapViewPure extends React.Component {
    // state = {
    //     apiLoaded: null,
    // }

    componentDidMount() {
        if(!window.__mapSciptState ){
            store.dispatch(actions.startLoadingMapScript());
            window.__mapSciptState = mapScriptState.LOADING
            this.loadMapScript();
        }

        if(window.__mapSciptState === mapScriptState.LOADED){
            this.renderMap(this.props.lat, this.props.lng)
        }
    }

    componentWillReceiveProps(nextProps) {
        if (window.google && window.google.maps) {
            if (nextProps.lat !== this.props.lat || nextProps.lng !== this.props.lng) {
                this.renderMap(nextProps.lat, nextProps.lng)
            }
        }

        if(nextProps.mapSciptState === mapScriptState.LOADED){
            this.renderMap(nextProps.lat, nextProps.lng)
        }
    }

    renderMap(lat, lng){
        var target = {
            lat,
            lng
        }

        var map = new google.maps.Map(
            this.refs.map1,
            {
                zoom: 15,
                center: target
            }
        );

        var marker = new google.maps.Marker({position: target, map: map});
    }

    loadMapScript() {
        // Load the google maps api script when the component is mounted.


            loadScript(`https://maps.googleapis.com/maps/api/js?key=${API_KEY}`)
                .then((script) => {
                    this.mapScript = script;
                    // this.setState({ apiLoaded: true });
                    store.dispatch(actions.finishLoadingMapScript());
                    window.__mapSciptState = mapScriptState.LOADED
                    this.renderMap(this.props.lat, this.props.lng)
                })
                .catch((err) => {
                    console.error(err.message);
                });



    }


    render() {
        return (
            <div className={this.props.className}>
                {
                    <div ref="map1" style={{
                        width: this.props.width,
                        height: this.props.height,
                    }}></div>
                }
            </div>
        );
    }

}


const mapStateToProps = state => ({
    mapSciptState: state.global.mapSciptState,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const MapView = connect(mapStateToProps, mapDispatchToProps)(MapViewPure)