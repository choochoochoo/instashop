import React from 'react'
import {Link} from 'react-router-dom';
import {Field, reduxForm} from 'redux-form'

import styles from './Notification.css'
import i18next from 'i18next'
const classNames = require('classnames');

export const Notification = ({text, type, visible}) => {

    const messageText = text ? text : i18next.t(`notification_text_${type}`)

    return (
        <div className={styles.modal} style={{
            opacity: visible ? 1 : 0,
            visibility: visible ? 'visible' : 'hidden'
        }}>
            <button className={classNames(styles.button, styles[type])}>
                <span className={styles.buttonText}>
                    {messageText}
                </span>
             </button>
        </div>
    )
}



