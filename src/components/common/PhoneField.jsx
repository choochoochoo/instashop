import React from 'react'
import {Link} from 'react-router-dom';
import i18next from 'i18next';
import {Field, reduxForm} from 'redux-form'

import styles from './PhoneField.css'
import {createNumberMask, createTextMask} from "redux-form-input-masks";


const renderField = ({
                         input,
                         placeholder,
                         type,
                         accept,
                         meta: {touched, error, warning}
                     }) => (
    <div className={styles.inputWrapper}>
        <input
            {...input}
            placeholder={placeholder}
            type={type}
            accept={accept}
            className={styles.input}
        />

        {touched &&
        ((error && <span>{error}</span>) ||
            (warning && <span>{warning}</span>))}
    </div>
)



const phoneMask = createTextMask({
    pattern: '+9 999 999 99 99',
});

export const PhoneField = ({name, type, placeholder}) => {
    return (
        <div className={styles.root}>
            <Field
                name={name}
                type="tel"
                {...phoneMask}
                placeholder={placeholder}
                component={renderField}
            />
        </div>
    )
}



