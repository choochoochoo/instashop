import {clearPhone} from "../../utils/common";
import React from "react";

export const PhoneLink = ({phone, children, className}) => {
    return (
        <a
            href={`tel:${clearPhone(phone)}`}
            target="_self"
            className={className}
        >
            {children}
        </a>
    )
}