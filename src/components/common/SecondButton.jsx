import React from 'react'
import {Link} from 'react-router-dom';
import {Field, reduxForm} from 'redux-form'

import styles from './SecondButton.css'

export const SecondButton = ({title, onClick, type = 'submit'}) => {
    return (
        <div className={styles.root}>
            <button type={type} className={styles.button} onClick={onClick}>
                <span className={styles.buttonText}>{title}</span>
             </button>
        </div>
    )
}



