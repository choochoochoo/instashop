import React from 'react'
import {Link} from 'react-router-dom';
import i18next from 'i18next';
import {Field, reduxForm} from 'redux-form'

import styles from './SelectField.css'


const renderField = ({
                         input,
                         placeholder,
                         type,
                         accept,
                         meta: {touched, error, warning},
                         list,
                         label,
                     }) => (

    <div className={styles.inputWrapper}>
        <label>
            <div className={styles.label}>
                {label}
            </div>
            <div className={styles.inputWrapper2}>
                <select
                    {...input}
                    placeholder={placeholder}
                    type={type}
                    accept={accept}
                    className={styles.select}
                >
                    {
                        list.map(item => {
                            return <option value={item.code}>{item.title}</option>
                        })
                    }
                </select>
            </div>
        </label>
    </div>
)

export const SelectField = ({name, type, placeholder, list, label}) => {
    return (
        <div className={styles.root}>
            <Field
                name={name}
                type={type}
                label={label}
                placeholder={placeholder}
                component={renderField}
                list={list}
            />
        </div>
    )
}