import React from 'react'
import styles from './Status.css'
import {CROPING_PHOTO} from "../../constants/CreateProductSteps";
const classNames = require('classnames');


export const Status = ({text, status, className}) => {
    const styleobj = { }
    styleobj[`${styles.root}`] = true
    styleobj[`${styles.new}`] = status === 'new'
    styleobj[`${styles.accept}`] = status === 'accept'

    let visible =  status === 'new' || status === 'accept'

    if(!visible){
        return <div/>
    }

    return (
        <div className={classNames(styleobj, className)}>
            <div className={styles.text}>
                {text}
            </div>
        </div>
    )
}



