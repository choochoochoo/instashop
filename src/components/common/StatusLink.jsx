import React from 'react'
import {Link} from "react-router-dom";
import {bindActionCreators} from "redux";
import * as actions from "../../actions";
import connect from "react-redux/es/connect/connect";
import {Status} from '../common'
import i18next from 'i18next'


export const StatusLinkPure = ({newOrders, accountName, settingsOrdersSwitchOn}) => {

    return (
        <div>
            {
                settingsOrdersSwitchOn && newOrders > 0 &&
                <Link to={`/${accountName}/orders`}>
                    <Status
                        text={i18next.t('amount_of_orders', {count: newOrders})}
                        status={'new'}
                    />
                </Link>
            }
        </div>
    )
}

const mapStateToProps = state => ({
    newOrders: state.profile.newOrders,
    accountName: state.profile.accountName,
    settingsOrdersSwitchOn: state.profile.settingsOrdersSwitchOn,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const StatusLink = connect(mapStateToProps, mapDispatchToProps)(StatusLinkPure)



