import React from 'react'
import styles from './Subtitle.css'

export const Subtitle = ({text}) => {
    return (
        <div className={styles.title}>
            {text}
        </div>
    )
}



