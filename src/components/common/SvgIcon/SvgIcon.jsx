import React from 'react'
import {
    close,
    reload,
    menu,
    closeMenu,
    email,
    photo,
    phone,
    pdfFile,
    pngFile,
    textEditorHeader,
    textEditorLink,
    mapMarker,
    contactsMap,
    contactsPhone,
    contactsInfo,
    themeCloud,
    themeSteveJobs,
    themeNavyBlue,
    checkOk,
    logo,
    whatsappcall,
    phonecall,
    arrow,
    plus,
    checkboxOn,
    checkboxOff,
    itemMinus,
    itemPlus,
    orderSuccess,
    refresh,
    contactLocation,
    contactMail,
    contactPhone,
} from './icons'


const icons = {
    close,
    reload,
    menu,
    closeMenu,
    email,
    photo,
    phone,
    pdfFile,
    pngFile,
    textEditorHeader,
    textEditorLink,
    mapMarker,
    contactsMap,
    contactsPhone,
    contactsInfo,
    themeCloud,
    themeSteveJobs,
    themeNavyBlue,
    checkOk,
    logo,
    whatsappcall,
    phonecall,
    arrow,
    plus,
    checkboxOn,
    checkboxOff,
    itemMinus,
    itemPlus,
    orderSuccess,
    refresh,
    contactLocation,
    contactMail,
    contactPhone,
}

export const SvgIcon = ({icon, width, height, className}) => {
    const iconStyle = {
        width: `${width}px`,
        height: `${height}px`,
    }

    return (
        <span
            dangerouslySetInnerHTML={{__html: icons[icon]}}
            style={iconStyle}
            className={className}
        />
    )
}



