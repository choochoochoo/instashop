import React from 'react'
import {Link} from 'react-router-dom';
import i18next from 'i18next';
import {Field, reduxForm} from 'redux-form'

import styles from './Switch.css'
const classNames = require('classnames');

export const Switch = ({name, click, value, label}) => {
    const lineStyle = {
        backgroundColor:  '#f8d75c'
    }

    const roundStyle = {
        backgroundColor: '#354b6c',
        border: 'solid 1px #d6dce3',
        transform: 'translateX(20px)',
        transition: 'transform 150ms cubic-bezier(0.4, 0, 0.2, 1) 0ms'
    }

    return (
        <label>
            <span className={styles.label}>
                {label}
            </span>
            <span className={styles.root}>
                <span className={styles.checkboxWrapper}>
                    <span className={styles.round} style={value ? roundStyle : {}}></span>
                    <input
                        name={name}
                        type={"checkbox"}
                        onClick={click}
                        value={value}
                        className={styles.checkbox}
                    />
                </span>
                <span className={styles.line} style={value ? lineStyle : {}}></span>
            </span>
        </label>
    )
}