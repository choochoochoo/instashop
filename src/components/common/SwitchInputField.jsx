import React from 'react'
import {Link} from 'react-router-dom';
import i18next from 'i18next';
import {Field, reduxForm} from 'redux-form'

import styles from './SwitchInputField.css'

const classNames = require('classnames');

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import * as actions from '../../actions';
import store from '../../store.js';


const renderField = ({
                         input,
                         placeholder,
                         type,
                         accept,
                         meta: {touched, error, warning},
                         label,
                         theme,
                         required,
                     }) => {

    const styleError = touched && error ? {
        borderTop: 'solid 1px #f57123',
        borderBottom: 'solid 1px #f57123',
    } : {}

    const styleErrorText = touched && error ? {
        color: '#f57123'
    } : {}

    const labelText = (touched && error) ? `${label} — ${error}` : label

    let req = null

    if(required){
        req = <span className={styles.required}>*</span>
    }

    return (
        <div className={styles.inputWrapper}>
            <label>
                <div className={styles.label} style={styleErrorText}>
                    {labelText}
                    {req}
                </div>
                <div className={styles.inputWrapper2}>
                    <input
                        {...input}
                        placeholder={placeholder}
                        type={type}
                        accept={accept}
                        className={styles.input}
                        style={{
                            color: theme['common_field_text-color']
                        }}
                    />
                </div>
            </label>
        </div>
    )
}

export const SwitchInputFieldPure = ({name, type, placeholder, label, theme, required}) => {
    return (
        <div className={styles.root}>
            <Field
                name={name}
                type={type}
                placeholder={placeholder}
                component={renderField}
                label={label}
                theme={theme}
                required={required}
            />
        </div>
    )
}

const mapStateToProps = state => ({
    theme: state.theme
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const SwitchInputField = connect(mapStateToProps, mapDispatchToProps)(SwitchInputFieldPure)