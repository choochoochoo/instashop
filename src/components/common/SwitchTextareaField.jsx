import React from 'react'
import {Link} from 'react-router-dom';
import i18next from 'i18next';
import {Field, reduxForm} from 'redux-form'

import styles from './SwitchTextareaField.css'

const classNames = require('classnames');

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import * as actions from '../../actions';
import store from '../../store.js';


const renderField = ({
                         input,
                         placeholder,
                         type,
                         accept,
                         meta: {touched, error, warning},
                         label,
                         theme,
                     }) => {

    return (
        <div className={styles.inputWrapper}>
            <label>
                <div className={styles.label}>
                    {label}
                </div>
                <div className={styles.inputWrapper2}>
                    <textarea
                        {...input}
                        placeholder={placeholder}
                        type={type}
                        accept={accept}
                        className={styles.input}
                        style={{
                            color: theme['common_field_text-color']
                        }}
                        rows={4}
                    />
                </div>
            </label>
        </div>
    )
}

export const SwitchTextareaFieldPure = ({name, type, placeholder, label, theme}) => {
    return (
        <div className={styles.root}>
            <Field
                name={name}
                placeholder={placeholder}
                component={renderField}
                label={label}
                theme={theme}
            />
        </div>
    )
}

const mapStateToProps = state => ({
    theme: state.theme
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const SwitchTextareaField = connect(mapStateToProps, mapDispatchToProps)(SwitchTextareaFieldPure)