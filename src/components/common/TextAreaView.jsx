import React from 'react'

import {normalizeTextareaText} from "../../utils/common";

export const TextAreaView = ({content, className}) => {

    return (
        <div
            className={className}
            dangerouslySetInnerHTML={{__html: normalizeTextareaText(content)}}
        />
    )
}



