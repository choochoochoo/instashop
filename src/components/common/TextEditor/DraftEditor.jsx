import React from 'react'
import {
    Editor,
    EditorState,
    RichUtils,
    convertToRaw,
    Modifier,
    CompositeDecorator,
    ContentState,
} from 'draft-js';
var Immutable = require('immutable');
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';

import { getLinkDecorator } from './decorators/index'

import styles from './DraftEditor.css'

import {TextEditorPanel} from './TextEditorPanel.jsx'
import i18next from "i18next";

import {LinkWindow} from './LinkWindow.jsx'

const blockRenderMap = Immutable.Map({
    'unstyled': {
        element: 'div'
    },
    'header-three': {
        element: 'h3'
    }
});

export function hasProperty(obj: Object, property: string) {
    let result = false;
    if (obj) {
        for (const key in obj) { // eslint-disable-line no-restricted-syntax
            if ({}.hasOwnProperty.call(obj, key) && property === key) {
                result = true;
                break;
            }
        }
    }
    return result;
}

function myBlockStyleFn(contentBlock) {
    const type = contentBlock.getType();
    if (type === 'LINK') {
        return 'superFancyBlockquote';
    }

    if (type === 'unstyled') {
        return 'paragraph';
    }
}

export class DraftEditor extends React.Component {
    constructor(props) {
        super(props);
        this.state = {editorState: EditorState.createEmpty()};

        this.focus = () => this.refs.editor.focus();
        this.onChange = (editorState) => this.setState({editorState});

        this.handleKeyCommand = (command) => this._handleKeyCommand(command);
        this.onTab = (e) => this._onTab(e);
        this.toggleBlockType = (type) => this._toggleBlockType(type);
        this.toggleInlineStyle = (style) => this._toggleInlineStyle(style);

        this.handleButtonClick = () => this._handleButtonClick();
        this.insertLink = (link) => this._insertLink(link);

        this.saveClick = (content) => this._saveClick(content);
    }

    getCompositeDecorator = () => {
        const decorators = [
            getLinkDecorator({
                //showOpenOptionOnHover: this.state.toolbar.link.showOpenOptionOnHover
            })
        ];

        return new CompositeDecorator(decorators);
    };

    componentWillMount(): void {
        this.compositeDecorator = this.getCompositeDecorator();
        const editorState = this.createEditorState(this.compositeDecorator);
        //extractInlineStyle(editorState);
        this.setState({
            editorState
        });
    }

    componentDidMount(): void {
        if(this.props.content) {
            this._getInitial(this.props.content)
        }
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.content !== this.props.content && this.props.content){
            this._getInitial(nextProps.content)
        }
    }

    _getInitial(content) {

        console.log(content)

        let editorState;
        const blocksFromHtml = htmlToDraft(content);
        const { contentBlocks, entityMap } = blocksFromHtml;
        const contentState = ContentState.createFromBlockArray(contentBlocks, entityMap);
        editorState = EditorState.createWithContent(contentState, this.compositeDecorator);

        this.setState({
            editorState
        });
    }

    createEditorState = compositeDecorator => {
        let editorState;

        if (!editorState) {
            editorState = EditorState.createEmpty(compositeDecorator);
        }

        return editorState;
    };

    _handleButtonClick() {


    }

    _handleKeyCommand(command) {
        const {editorState} = this.state;
        const newState = RichUtils.handleKeyCommand(editorState, command);
        if (newState) {
            this.onChange(newState);
            return true;
        }
        return false;
    }

    _onTab(e) {
        const maxDepth = 4;
        this.onChange(RichUtils.onTab(e, this.state.editorState, maxDepth));
    }

    _toggleBlockType(blockType) {
        const { editorState } = this.state;

        this.onChange(
            RichUtils.toggleBlockType(
                editorState,
                blockType
            )
        );
    }

    _insertLink(link) {
        const { editorState, onChange } = this.state;
        const { currentEntity } = this.state;
        let selection = editorState.getSelection();

        if (currentEntity) {
            const entityRange = getEntityRange(editorState, currentEntity);
            selection = selection.merge({
                anchorOffset: entityRange.start,
                focusOffset: entityRange.end,
            });
        }
        const entityKey = editorState
            .getCurrentContent()
            .createEntity('LINK', 'MUTABLE', { url: link, targetOption: '_blank' })
            .getLastCreatedEntityKey();

        let contentState = Modifier.replaceText(
            editorState.getCurrentContent(),
            selection,
            `${link}`,
            editorState.getCurrentInlineStyle(),
            entityKey,
        );
        let newEditorState = EditorState.push(editorState, contentState, 'insert-characters');

        // insert a blank space after link
        selection = newEditorState.getSelection().merge({
            anchorOffset: selection.get('anchorOffset') + link.length,
            focusOffset: selection.get('anchorOffset') + link.length,
        });
        newEditorState = EditorState.acceptSelection(newEditorState, selection);
        contentState = Modifier.insertText(
            newEditorState.getCurrentContent(),
            selection,
            ' ',
            newEditorState.getCurrentInlineStyle(),
            undefined,
        );
        this.onChange(EditorState.push(newEditorState, contentState, 'insert-characters'));
    }

    _saveClick(text) {
        const customEntityTransform = (entity, text) => {
            console.log(entity)

            return 'mylink'
        }

        const html = draftToHtml(
            convertToRaw(this.state.editorState.getCurrentContent()),
            null,
            null//,
            //customEntityTransform
        );

        //console.log(convertToRaw(this.state.editorState.getCurrentContent()))


        this.props.onHandleSave(html)
    }

    render() {
        const {editorState} = this.state;

        const {
            onHandleClose,
            onHandleSave,
            closeWindowLink,
            openTextEditorLinkWindow,
        } = this.props

        const pasteLinkSubmit = (values) => {

            this.insertLink(values.link)
            closeWindowLink()
        }

        const keyUp = (e) => {
            const {editorState} = this.state;
            let selection = editorState.getSelection();

            const blockType = editorState
                .getCurrentContent()
                .getBlockForKey(selection.getStartKey())
                .getType();


            if(blockType === 'header-three' && e.key === 'Enter'){
                this._toggleBlockType('unstyled')
            }
        }

        // If the user changes block type before entering any text, we can
        // either style the placeholder or hide it. Let's just hide it now.
        let className = 'RichEditor-editor';
        var contentState = editorState.getCurrentContent();
        if (!contentState.hasText()) {
            if (contentState.getBlockMap().first().getType() !== 'unstyled') {
                className += ' RichEditor-hidePlaceholder';
            }
        }

        return (
            <div className="RichEditor-root">
                {
                    openTextEditorLinkWindow &&
                    <LinkWindow
                        clickCancel={closeWindowLink}
                        clickNext={pasteLinkSubmit}
                    />
                }

                <div className={className} onClick={this.focus} onKeyUp={keyUp}>
                    <div className={styles.backPanel}></div>
                    <TextEditorPanel
                        clickCancel={onHandleClose}
                        clickNext={this.saveClick}
                        handleHeaderButtonClick={this.toggleBlockType}
                        handleLinkButtonClick={this.props.handleLinkButtonClick}
                        editorState={editorState}
                    />
                    <Editor
                        editorState={editorState}
                        handleKeyCommand={this.handleKeyCommand}
                        onChange={this.onChange}
                        onTab={this.onTab}
                        // placeholder={i18next.t('text_editor_draft_editor_placeholder')}
                        ref="editor"
                        spellCheck={true}
                        blockRenderMap={blockRenderMap}
                        blockStyleFn={myBlockStyleFn}
                    />
                </div>
            </div>
        );
    }
}


