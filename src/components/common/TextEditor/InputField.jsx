import React from 'react'
import {Link} from 'react-router-dom';
import i18next from 'i18next';
import {Field, reduxForm} from 'redux-form'

import styles from './InputField.css'


const renderField = ({
                         input,
                         placeholder,
                         type,
                         accept,
                         meta: {touched, error, warning},
                         label,
                     }) => (
    <div className={styles.inputWrapper}>
        <label >

            <div className={styles.label}>
                {label}

            </div>
            <div className={styles.inputWrapper2}>
            <input
                {...input}
                placeholder={placeholder}
                type={type}
                accept={accept}
                className={styles.input}
            />
            </div>
        </label>


        {touched &&
        ((error && <span>{error}</span>) ||
            (warning && <span>{warning}</span>))}
    </div>
)

export const InputField = ({name, type, placeholder, label}) => {
    return (
        <div className={styles.root}>
            <Field
                name={name}
                type={type}
                placeholder={placeholder}
                component={renderField}
                label={label}

            />
        </div>
    )
}