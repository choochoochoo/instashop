import React from 'react'
import styles from './LinkWindow.css'
import {SvgIcon} from '../../common'
import i18next from "i18next";
const classNames = require('classnames');

import {LinkWindowForm} from './LinkWindowForm.jsx'

export const LinkWindow = ({clickCancel, clickNext, visible}) => {
    return (
        <div
            className={styles.modal}
        >
            <LinkWindowForm
                clickCancel={clickCancel}
                onSubmit={clickNext}
            />
        </div>
    );

}
