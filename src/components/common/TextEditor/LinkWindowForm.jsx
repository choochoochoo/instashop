import React from 'react'
import {Field, reduxForm} from 'redux-form'
import {connect} from "react-redux";
import {PanelButton} from './PanelButton.jsx'

import i18next from "i18next";

import {
    InputField,
} from './InputField.jsx'

import styles from './LinkWindow.css'



const validate = values => {
    const errors = {}

    // if (!values.photo) {
    //     errors.photo = 'Required'
    // }

    // if (!values.title) {
    //     errors.title = 'Required'
    // }
    //
    // if (!values.price) {
    //     errors.price = 'Required'
    // }

    return errors
}



let LinkWindowFormPure = props => {
    const {
        handleSubmit,
        clickCancel,
        clickNext,
    } = props

    return (
        <div className={styles.formWrapper}>
            <form onSubmit={handleSubmit} className={styles.form}>
                <div className={styles.panel}>
                    <PanelButton onClick={clickCancel}>
                        {i18next.t('text_editor_link_window_button_cancel')}
                    </PanelButton>
                    <PanelButton type="submit">
                        {i18next.t('text_editor_link_window_button_next')}
                    </PanelButton>
                </div>
                <div className={styles.inputWrapper}>
                    <InputField
                        placeholder={i18next.t('text_editor_link_window_text_field_placeholder')}
                        name="link"
                    />
                </div>
            </form>
        </div>
    )
}

LinkWindowFormPure = reduxForm({
    form: 'LinkWindowForm',
    validate,
    enableReinitialize: true,
})(LinkWindowFormPure)

export const LinkWindowForm = connect(
    (state, ownParam) => {
        return {
             initialValues: {

             }
        }
    }
)(LinkWindowFormPure)
