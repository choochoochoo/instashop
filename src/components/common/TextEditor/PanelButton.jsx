import React from 'react'
import {Link} from 'react-router-dom';
import {Field, reduxForm} from 'redux-form'

import styles from './PanelButton.css'
const classNames = require('classnames');

export const PanelButton = ({onClick, children, className, type="button"}) => {
    return (
        <button
            type={type}
            className={classNames(styles.button, className)}
            onClick={onClick}
        >
            <span className={styles.buttonText}>{children}</span>
        </button>
    )
}
