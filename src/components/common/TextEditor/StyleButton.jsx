import React from 'react'
import styles from './StyleButton.css'
import {SvgIcon} from '../../common'
const classNames = require('classnames');

export const StyleButton = ({click, icon, title, active, style}) => {

        const styleobj = { }
        styleobj[`${styles.button}`] = true
        styleobj[`${styles.active}`] = active

        const clickHandle = (e) => {
            e.preventDefault()
            click(style)
        }

        return (
            <button
                onMouseDown={clickHandle}
                className={classNames(styleobj)}
            >
                <div className={styles.icon}>
                    <SvgIcon icon={icon} />
                </div>
                <div className={styles.title}>
                    {title}
                </div>
            </button>
        );

}

