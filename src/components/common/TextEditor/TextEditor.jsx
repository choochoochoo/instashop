import React from 'react'
import ReactDOM from 'react-dom'
import i18next from "i18next";

import {Button} from '../Button.jsx'
import {SvgIcon} from '../SvgIcon/SvgIcon.jsx'
import styles from './TextEditor.css'


import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import * as actions from '../../../actions';
import store from '../../../store.js';

import {DraftEditor} from './DraftEditor.jsx'


export class TextEditorPure extends React.Component {
    state = {

    }

    componentDidMount(){

    }

    componentWillReceiveProps(nextProps) {

    }

    render() {
        const {
            handleClose,
            handleSave,
            visible,
            content,
            openTextEditorLinkWindow,
        } = this.props


        console.log(content)

        const style = {
            display: visible ? 'block' : 'none' ,
            visibility: visible ? 'visible' : 'hidden',
        }

        const onHandleClose = () => {
             handleClose()
        }

        const onHandleSave = (html) => {
             handleSave(html)
        }

        const linkButtonClick = () => {
            store.dispatch(actions.openTextEditorLinkWindow());
        }

        const closeWindowLink = (text) => {
            store.dispatch(actions.closeTextEditorLinkWindow());
        }

        return (
            <div className={styles.root} style={style}>
                <div className={styles.textEditorWrapper}>
                    <DraftEditor
                        onHandleClose={onHandleClose}
                        onHandleSave={onHandleSave}
                        handleLinkButtonClick={linkButtonClick}
                        closeWindowLink={closeWindowLink}
                        openTextEditorLinkWindow={openTextEditorLinkWindow}
                        content={content}
                    />
                </div>
            </div>
        )
    }
}


const mapStateToProps = state => ({
    openTextEditorLinkWindow: state.global.openTextEditorLinkWindow
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const TextEditor = connect(mapStateToProps, mapDispatchToProps)(TextEditorPure)