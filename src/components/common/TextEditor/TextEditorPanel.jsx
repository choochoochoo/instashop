import React from 'react'
import {Link} from 'react-router-dom';
import i18next from 'i18next';
import {Field, reduxForm} from 'redux-form'

import styles from './TextEditorPanel.css'

import {PanelButton} from './PanelButton.jsx'
import {StyleButton} from './StyleButton.jsx'

const header = {label: 'H3', style: 'header-three'}


export const TextEditorPanel = ({
                                    clickNext,
                                    clickCancel,
                                    setText,
                                    editorState,
                                    handleHeaderButtonClick,
                                    handleLinkButtonClick,
                                }) => {

    const selection = editorState.getSelection();

    const blockType = editorState
        .getCurrentContent()
        .getBlockForKey(selection.getStartKey())
        .getType();

    return (
        <div className={styles.root}>

            <PanelButton onClick={clickCancel}>
                {i18next.t('text_editor_button_cancel')}
            </PanelButton>

            <div className={styles.buttonInsertWrapper}>
                <StyleButton
                    active={header.style === blockType}
                    title={i18next.t('text_editor_panel_button_header')}
                    click={handleHeaderButtonClick}
                    icon="textEditorHeader"
                    style={header.style}
                />
            </div>

            <PanelButton onClick={clickNext}>
                {i18next.t('text_editor_button_next')}
            </PanelButton>
        </div>
    )
}