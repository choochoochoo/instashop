import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './Link.css';

function findLinkEntities(contentBlock, callback, contentState) {
    contentBlock.findEntityRanges(
        (character) => {
            const entityKey = character.getEntity();
            return (
                entityKey !== null &&
                contentState.getEntity(entityKey).getType() === 'LINK'
            );
        },
        callback,
    );
}

function getLinkComponent(config) {
    return class Link extends Component {
        static propTypes = {
            entityKey: PropTypes.string.isRequired,
            children: PropTypes.array,
            contentState: PropTypes.object,
        };

        state: Object = {
            showPopOver: false,
        };

        render() {
            const { children, entityKey, contentState } = this.props;
            const { url, targetOption } = contentState.getEntity(entityKey).getData();
            const { showPopOver } = this.state;
            return (
                <a
                  href={url}
                  target={targetOption}
                  className={styles.link}>
                  {children}
                </a>
            );
        }
    };
}

export const getLinkDecorator = (config) => ({
    strategy: findLinkEntities,
    component: getLinkComponent(config),
});