import React from 'react'
import styles from './TextEditorField.css'
import sectionViewStyles from './TextEditor/SectionView.css'
import i18next from 'i18next';

import {
    HiddenField,
} from './HiddenField.jsx'

import {urlifyWithoutLink} from '../../utils/common'

export let TextEditorField = props => {

    const {
        text,
        openTextEditor,
        value,
        label,
        placeholder,
        fieldName,
        height=200,
    } = props



    return (
        <div>
            <div className={styles.hidden}>
                <HiddenField
                    name={fieldName}
                    value={value}
                />
            </div>

            <button className={styles.editorView} type="button" onClick={openTextEditor} >
                <div className={styles.label}>
                    {label}
                </div>

                <div className={styles.editorViewTextWrapper} style={{
                    height: `${height}px`
                }}>
                    <div className={styles.editorViewText}>
                        {
                            !text && (
                                <div className={styles.placeholder}>
                                    {placeholder}
                                </div>
                            )
                        }

                        {
                            text &&
                            <div
                                dangerouslySetInnerHTML={{__html: urlifyWithoutLink(text)}}
                                className={sectionViewStyles.sectionView}
                            />
                        }
                    </div>
                </div>
            </button>
        </div>
    )
}