import React from 'react'
import {Link} from 'react-router-dom';
import i18next from 'i18next';
import {Field, reduxForm} from 'redux-form'

import styles from './TextareaField.css'

const classNames = require('classnames');

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import * as actions from '../../actions';
import store from '../../store.js';


const renderField = ({
                         input,
                         placeholder,
                         type,
                         accept,
                         meta: {touched, error, warning},
                         label,
                         theme,
                         rows,
                     }) => {

    const styleError = touched && error ? {
        borderTop: 'solid 1px #f57123',
        borderBottom: 'solid 1px #f57123',
    } : {}

    const styleErrorText = touched && error ? {
        color: '#f57123'
    } : {}

    const labelText = (touched && error) ? `${label} (${error})` : label

    return (
        <div className={styles.inputWrapper}>
            <label >
                <div className={styles.label} style={styleErrorText}>
                    {labelText}
                </div>
                <div className={styles.inputWrapper2} style={styleError}>
                    <textarea
                        {...input}
                        placeholder={placeholder}
                        accept={accept}
                        className={styles.input}
                        style={{
                            color: theme['common_field_text-color']
                        }}
                        rows={rows}
                    />
                </div>
            </label>
        </div>
    )
}

export const TextareaFieldPure = ({name, placeholder, label, theme, rows=3}) => {
    return (
        <div className={styles.root}>
            <Field
                name={name}
                rows={rows}
                placeholder={placeholder}
                component={renderField}
                label={label}
                theme={theme}
            />
        </div>
    )
}

const mapStateToProps = state => ({
    theme: state.theme
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const TextareaField = connect(mapStateToProps, mapDispatchToProps)(TextareaFieldPure)