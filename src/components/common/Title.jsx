import React from 'react'
import styles from './Title.css'

export const Title = ({text}) => {
    return (
        <div className={styles.title}>
            {text}
        </div>
    )
}



