import React from 'react'
import {Link} from 'react-router-dom';
import {Field, reduxForm} from 'redux-form'


import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import * as actions from '../../actions';
import store from '../../store.js';
import {Switch} from "./Switch.jsx";

import i18next from 'i18next'


export const ViewModeSwitchPure = ({
                           viewModeEnabled
                        }) => {

    const editModeClick = () => {
        if(viewModeEnabled){
            store.dispatch(actions.viewModeDisabled());
        }else{
            store.dispatch(actions.viewModeEnabled());
        }
    }

    const viewModeText =
        viewModeEnabled ?
            i18next.t('view_mode_enabled') :
            i18next.t('view_mode_disabled')

    return (
        <div>
            <Switch
                click={editModeClick}
                value={viewModeEnabled}
                label={viewModeText}
            />
        </div>
    )
}

const mapStateToProps = state => ({
    viewModeEnabled: state.global.viewModeEnabled,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
})

export const ViewModeSwitch = connect(mapStateToProps, mapDispatchToProps)(ViewModeSwitchPure)



