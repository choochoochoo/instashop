import {clearPhone} from "../../utils/common";
import styles from "../Product/Connection.css";
import React from "react";
import i18next from 'i18next'


export const WhatsappLink = ({phone, text, children, className}) => {
    return (
        <a href={`https://wa.me/${clearPhone(phone)}/?text=${encodeURI(text ? text : i18next.t('whatsapp_default_text'))}`}
           target="_self"
           className={className}
        >
            {children}
        </a>
    )
}