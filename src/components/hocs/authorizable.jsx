import React from 'react';
import * as actions from '../../actions';
import store from '../../store.js';
import {getUser} from '../../api';

export const authorizable = (WrappedComponent) => {

    return class LoadableClassPure extends React.Component {
        constructor(props) {
            super(props);
        }

        componentWillReceiveProps(nextProps) {
            if(nextProps.loginedUser._id){
                this.props.history.push(`/${nextProps.loginedUser.accountName}`)
            }
        }

        render() {
            return(
                <WrappedComponent {...this.props}/>
            )
        }
    }
};

