import React from 'react';
import * as actions from '../../actions';
import store from '../../store.js';
import {getOrders} from '../../api';

export const orderable = (WrappedComponent) => {

    return class LoadableClassPure extends React.Component {
        constructor(props) {
            super(props);
        }

        componentDidMount() {
            const userId = this.props.profile._id

            if(!this.props.profile._id) {
                store.dispatch(actions.showProgress());
                store.dispatch(actions.ordersGetStart());

                // const userId = this.props.match.params.userId;

                if(userId) {
                    getOrders(userId).then(response => {
                        store.dispatch(actions.ordersGetSuccess(response.data));
                        store.dispatch(actions.hideProgress());

                    }).catch(exception => {
                        console.log(exception);
                        store.dispatch(actions.ordersGetError(exception));
                        store.dispatch(actions.hideProgress());
                    });
                }
            }
        }

        render() {
            return(
                <WrappedComponent {...this.props}/>
            )
        }
    }
};

