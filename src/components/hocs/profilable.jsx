import React from 'react';
import * as actions from '../../actions';
import store from '../../store.js';
import {getUserOrders, getUser} from '../../api';
import {getCart} from "../../api/cart";

export const profilable = (WrappedComponent) => {

    return class LoadableClassPure extends React.Component {
        constructor(props) {
            super(props);
        }

        componentWillMount() {

            if(!this.props.profile._id) {
                store.dispatch(actions.showProgress());
                store.dispatch(actions.userGetStart());

                const userId = this.props.match.params.userId;

                if(userId) {
                    getUser(userId).then(response => {
                        store.dispatch(actions.userGetSuccess(response.data));
                        store.dispatch(actions.hideProgress());

                        if(response.data.settingsOrdersSwitchOn){
                            const cart = getCart(this.props.userId)

                            if(cart) {
                                store.dispatch(actions.getCartFromStorage(cart));
                            }

                            store.dispatch(actions.showProgress());
                            store.dispatch(actions.ordersGetStart());

                            getUserOrders(response.data._id).then(orderResponse => {

                                store.dispatch(actions.ordersGetSuccess(orderResponse.data));
                                store.dispatch(actions.hideProgress());

                            }).catch(exception => {
                                console.log(exception);
                                store.dispatch(actions.ordersGetError(exception));
                                store.dispatch(actions.hideProgress());
                            });

                        }

                    }).catch(exception => {
                        console.log(exception);
                        store.dispatch(actions.userGetError(exception));
                        store.dispatch(actions.hideProgress());
                    });
                }
            }
        }

        render() {
            return(
                <WrappedComponent {...this.props}/>
            )
        }
    }
};

