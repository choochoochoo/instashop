export const INFO = 'info';
export const ERROR = 'error';
export const SAVE = 'save';
export const REMOVE = 'remove';

export const TIMEOUT = 500;
