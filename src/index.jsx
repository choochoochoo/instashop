import React from 'react'
import {render} from 'react-dom'
import {BrowserRouter as Router, Switch, Route, browserHistory} from 'react-router-dom';
import {Provider} from 'react-redux';
//import {syncHistoryWithStore} from 'react-router-redux';

// import { createBrowserHistory } from 'history';
// const history = syncHistoryWithStore(createBrowserHistory(), store);
// history.listen(location => console.log(location))

import store from './store.js';
import {
    App,
    Product,
    Profile,
    Theme,
    ClientOrderSuccess,
    Orders,
    GlobalApp,
    Register,
    Login,
    CreateProduct,
    Landing,
    RegisterSuccess,
    QRCode,
    Section,
    CreateSection,
    NewSectionDemo,
    NewContactsDemo,
    Settings,
    Hints,
    Connections,
    Categories,
    EditCategory,
    SettingsOrders,
    ClientCart,
    ClientOrderRegister,
    Order,
    ChangeAccountName,
} from './components'

render(
    <Provider store={store}>
        <GlobalApp>
            <Router>
                <Switch>
                    <Route exact path='/' component={Landing}/>
                    <Route exact path='/register' component={Register}/>
                    <Route exact path='/register-success/' component={RegisterSuccess}/>
                    <Route exact path='/login' component={Login}/>
                    <Route exact path='/:userId' component={App}/>
                    <Route exact path='/:userId/settings/' component={Settings}/>
                    <Route exact path='/:userId/hints/' component={Hints}/>
                    <Route exact path='/:userId/profile/' component={Profile}/>
                    <Route exact path='/:userId/theme/' component={Theme}/>
                    <Route exact path='/:userId/connections/' component={Connections}/>
                    <Route exact path='/:userId/products/create/' component={CreateProduct}/>
                    <Route exact path='/:userId/products/edit/:productId' component={CreateProduct}/>
                    <Route exact path='/:userId/products/:productId' component={Product}/>
                    <Route exact path='/:userId/qrcode' component={QRCode}/>
                    <Route exact path='/:userId/demo/section' component={NewSectionDemo}/>
                    <Route exact path='/:userId/demo/contacts' component={NewContactsDemo}/>
                    <Route exact path='/:userId/sections/create/:type' component={CreateSection}/>
                    <Route exact path='/:userId/sections/edit/:type/:sectionId' component={CreateSection}/>
                    <Route exact path='/:userId/sections/:sectionId' component={Section}/>
                    <Route exact path='/:userId/categories' component={Categories}/>
                    <Route exact path='/:userId/categories/edit/:categoryId' component={EditCategory}/>
                    <Route exact path='/:userId/settings/orders' component={SettingsOrders}/>
                    <Route exact path='/:userId/client-cart' component={ClientCart}/>
                    <Route exact path='/:userId/client-order-register' component={ClientOrderRegister}/>
                    <Route exact path='/:userId/client-order-success' component={ClientOrderSuccess}/>
                    <Route exact path='/:userId/orders' component={Orders}/>
                    <Route exact path='/:userId/orders/:orderId' component={Order}/>
                    <Route exact path='/:userId/profile/account-name' component={ChangeAccountName}/>
                </Switch>
            </Router>
        </GlobalApp>
    </Provider>,
    document.getElementById('root')
)