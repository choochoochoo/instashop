import {
    SHOW_CALLBACK_DIALOG,
    HIDE_CALLBACK_DIALOG,
} from '../constants/ActionTypes.js';


const initialState = {
    opened: false,
    phone: '',
};

export default function comments(state = initialState, action) {
    switch (action.type) {
        case SHOW_CALLBACK_DIALOG:
            return {...state, opened: true};
        case HIDE_CALLBACK_DIALOG:
            return {...state, opened: false};
        default:
            return state;
    }
}