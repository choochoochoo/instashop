import {
    OPEN_CATEGORY_SELECTOR,
    CHECKBOX_ON_CATEGORY_SELECTOR,
    CHECKBOX_OFF_CATEGORY_SELECTOR,
    CLEAR_CATEGORY_SELECTOR,
} from '../constants/ActionTypes.js';

import _ from "lodash";
import {CATEGORY_CREATE_SUCCESS} from "../constants/ActionTypes";

const initialState = {
    categories: [],
    productCategories: null,
    dirty: false,
};

export default function categorySelector(state = initialState, action) {
    switch (action.type) {
        case OPEN_CATEGORY_SELECTOR: {

            const categoriesChecked = _.cloneDeep(action.categories)

            categoriesChecked.forEach(categoryCheck => {

                action.productCategories.forEach(productCategory => {
                    if(categoryCheck._id === productCategory._id){
                        categoryCheck.checked = true
                    }
                })

            })

            return {
                ...state,
                categories: categoriesChecked,
                productCategories: action.productCategories,
            };
        }
        case CHECKBOX_ON_CATEGORY_SELECTOR: {
            const categoriesChecked = _.cloneDeep(state.categories)

            const foundCategory = categoriesChecked.find(item => item._id === action.category._id)
            foundCategory.checked = true

            return {
                ...state,
                categories: categoriesChecked,
                dirty: true
            };
        }
        case CHECKBOX_OFF_CATEGORY_SELECTOR: {
            const categoriesChecked = _.cloneDeep(state.categories)

            const foundCategory = categoriesChecked.find(item => item._id === action.category._id)
            foundCategory.checked = false

            return {
                ...state,
                categories: categoriesChecked,
                dirty: true
            };
        }
        case CLEAR_CATEGORY_SELECTOR: {
            return {...initialState}
        }
        case CATEGORY_CREATE_SUCCESS: {

            const newCategories = [
                ...state.categories,
                {
                    _id: action.category._id,
                    title: action.category.title,
                }
            ]

            return {...state, categories: newCategories}
        }
        default:
            return state;
    }
}