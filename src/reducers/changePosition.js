import {
    OPEN_CHANGE_POSTION_PAGE,
    CLOSE_CHANGE_POSTION_PAGE,
    CHANGE_POSITION,
    CLEAR_CHANGE_POSITION,
    SELECT_CATEGORY_CHANGE_POSITION,
} from '../constants/ActionTypes.js';

import {arrayMove, getOldPosition, fullfillCategories, getProducts} from '../utils/common'
import _ from "lodash";

const initialState = {
    allProducts: [],
    products: [],
    openChangePositionPage: false,
    selectedPosition: null,
    productId: null,
    selectedCategory: null,
    categories: [],
    newPositions: {}
};

export default function changePosition(state = initialState, action) {
    switch (action.type) {
        case OPEN_CHANGE_POSTION_PAGE:
            const products = getProducts(action.allProducts, action.categories, action.selectedCategory)

            let changeProducts = _.cloneDeep(products)
            let selectedCategory = action.selectedCategory
            let categories = _.cloneDeep(action.categories)
            let allProducts = _.cloneDeep(action.allProducts)

            if(state.products.length > 0){
                changeProducts = state.products
                selectedCategory = state.selectedCategory
                categories = state.categories
                allProducts = state.allProducts

                const actionCategories = _.cloneDeep(action.categories)

                const newElements = _.differenceBy(actionCategories, categories , '_id');
                newElements.forEach(newElement => {
                    const foundCategory = actionCategories.find(item => item._id === newElement._id)
                    categories.push(foundCategory)
                })

            }

            if(action.isNew && state.products.length === 0){

                changeProducts.splice(0, 0, action.product)
                allProducts.splice(0, 0, action.product)
                categories.forEach(category => {
                    category.products.splice(0, 0, action.product)
                })

            }

            fullfillCategories(categories, action.product)

            return {
                ...state,
                openChangePositionPage: true,
                products: changeProducts,
                selectedCategory: selectedCategory,
                allProducts: allProducts,
                productId: action.productId,
                categories: categories,
            };
        case CLOSE_CHANGE_POSTION_PAGE:
            return {
                ...state,
                openChangePositionPage: false,
            };
        case CHANGE_POSITION: {
            let copyProducts = null
            let oldPosition = null

            const newPositions = {...state.newPositions, }
            newPositions[state.selectedCategory] = action.newPosition

            if (state.selectedCategory === 'all') {
                copyProducts = [...state.allProducts]

                oldPosition = getOldPosition(copyProducts, state.productId)
                arrayMove(copyProducts, (oldPosition - 1), (action.newPosition - 1))

                return {
                    ...state,
                    products: copyProducts,
                    allProducts: copyProducts,
                    // openChangePositionPage: false,
                    selectedPosition: action.newPosition,
                    newPositions,
                };

            } else {
                const copyCategories = _.cloneDeep(state.categories)
                const foundCategory = copyCategories.find(item => item._id === state.selectedCategory);
                oldPosition = getOldPosition(foundCategory.products, state.productId)

                arrayMove(foundCategory.products, (oldPosition - 1), (action.newPosition - 1))

                return {
                    ...state,
                    categories: copyCategories,
                    // openChangePositionPage: false,
                    selectedPosition: action.newPosition,
                    newPositions,
                    products: foundCategory.products,
                };
            }
        }
        case CLEAR_CHANGE_POSITION:
            return {
                ...initialState,
            };
        case SELECT_CATEGORY_CHANGE_POSITION:

            let newProducts = []

            if(action.category._id === 'all'){
                newProducts = state.allProducts
            }else{
                const foundCategory = state.categories.find(item => item._id === action.category._id);
                newProducts = foundCategory.products
            }

            const foundProduct = newProducts.find(item => item._id === state.productId);

            return {
                ...state,
                selectedCategory: action.category._id,
                products: newProducts,
                selectedPosition: foundProduct.position,
            }
        default:
            return state;
    }
}