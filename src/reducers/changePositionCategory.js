import {
    OPEN_CHANGE_POSTION_CATEGORY_PAGE,
    CLOSE_CHANGE_POSTION_CATEGORY_PAGE,
    CHANGE_POSITION_CATEGORY,
    CLEAR_CHANGE_POSITION_CATEGORY,
} from '../constants/ActionTypes.js';

import {arrayMove, getOldPosition} from "../utils/common";

const initialState = {
    items: [],
    openChangePositionPage: false,
    selectedPosition: null,
    itemId: null,
};

export default function changePosition(state = initialState, action) {
    switch (action.type) {
        case OPEN_CHANGE_POSTION_CATEGORY_PAGE: {

            if (state.items.length > 0) {
                return {
                    ...state,
                    openChangePositionPage: true,
                };
            } else {
                const newItems = _.cloneDeep(action.items)

                return {
                    ...state,
                    openChangePositionPage: true,
                    items: newItems,
                    itemId: action.itemId,
                };
            }
        }
        case CLOSE_CHANGE_POSTION_CATEGORY_PAGE:
            return {
                ...state,
                openChangePositionPage: false,
            };
        case CHANGE_POSITION_CATEGORY: {
            const newItems = _.cloneDeep(state.items)
            const oldPosition = getOldPosition(newItems, state.itemId)

            arrayMove(newItems, (oldPosition - 1), (action.newPosition - 1))

            return {
                ...state,
                selectedPosition: action.newPosition,
                items: newItems,
            };
        }
        case CLEAR_CHANGE_POSITION_CATEGORY:
            return {
                ...initialState,
            };
        default:
            return state;
    }
}