import {
    ADD_PRODUCT_TO_CLIENT_CART,
    REMOVE_PRODUCT_FROM_CLIENT_CART,
    GET_CART_FROM_STORAGE,
    ORDER_CREATE_SUCCESS,
    CLEAR_CLIENT_CART,
} from '../constants/ActionTypes.js';

import {addToCart} from "../utils/common";
import {removeFromCart} from "../utils/common";

const initialState = {
    items: [],
    status: null,
    number: null,
    _id: null,
    phone: null,
};

export default function clientCart(state = initialState, action) {
    switch (action.type) {
        case ADD_PRODUCT_TO_CLIENT_CART: {

            const newCart = addToCart(state, action.product)

            return {
                items: newCart.items
            }
        }
        case REMOVE_PRODUCT_FROM_CLIENT_CART: {

            const newCart = removeFromCart(state, action.product)

            return {
                items: newCart.items
            }
        }
        case GET_CART_FROM_STORAGE: {
            const newCart = action.cart
            return {
                items: newCart ? newCart.items : [],
                _id: newCart._id,
                status: newCart.status,
                number: newCart.number,
                phone: newCart.phone,
            }
        }
        case ORDER_CREATE_SUCCESS: {
            return {
                ...state,
                _id: action.order._id,
                status: action.order.status,
                number: action.order.number,
                phone: action.order.phone,
            }
        }
        case CLEAR_CLIENT_CART: {
            return {...initialState}
        }
        default:
            return state;
    }
}