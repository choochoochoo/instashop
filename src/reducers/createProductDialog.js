import {
    SHOW_CREATE_PRODUCT_DIALOG,
    HIDE_CREATE_PRODUCT_DIALOG,
} from '../constants/ActionTypes.js';


const initialState = {
    opened: false,
    productId: '',
    photoId: '',
};

export default function comments(state = initialState, action) {
    switch (action.type) {
        case SHOW_CREATE_PRODUCT_DIALOG:
            return {...state, opened: true, productId: action.productId, photoId: action.photoId,  };
        case HIDE_CREATE_PRODUCT_DIALOG:
            return {...state, opened: false, productId: '', photoId: ''};
        default:
            return state;
    }
}