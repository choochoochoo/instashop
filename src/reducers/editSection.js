import {
    SECTION_GET_SUCCESS,
    CLEAR_CREATE_SECTION,
    CLOSE_SECTION,
} from '../constants/ActionTypes.js';
import _ from "lodash";

//import {elementsToForm} from '../components/CreateSection/utils.js'
//import {getProduct} from '../utils/data'

const initialState = {
    _id: null,
    title: '',
    type: '',
    elements: {},
};

export default function editSection(state = initialState, action) {
    switch (action.type) {
        case SECTION_GET_SUCCESS:
            return {
                _id: action.section._id,
                type: action.section.type,
                title: action.section.title,
                elements: action.section.elements
            }
        case CLEAR_CREATE_SECTION:
            return {}
        case CLOSE_SECTION:
            return {}
        default:
            return state;
    }
}