import {
    SHOW_PROGRESS,
    HIDE_PROGRESS,
    USER_LOGIN_SUCCESS,
    USER_LOGOUT_SUCCESS,
    USER_LOGIN_NOT_SUCCESS,
    USER_GET_AUTH_SUCCESS,
    OPEN_MENU,
    CLOSE_MENU,
    USER_CREATE_NOT_SUCCESS,
    USER_CREATE_SUCCESS,
    VIEW_MODE_ENABLED,
    VIEW_MODE_DISABLED,
    CHANGE_THEME,
    LOAD_PHOTO_PRODUCT,
    UNLOAD_PHOTO_PRODUCT,
    SET_CROP,
    SET_SRC_QR_CODE,
    OPEN_TEXT_EDITOR,
    NEXT_TEXT_EDITOR,
    CLOSE_TEXT_EDITOR,
    SET_TEXT_TEXT_EDITOR,
    SECTION_GET_SUCCESS,
    OPEN_TEXT_EDITOR_LINK_WINDOW,
    CLOSE_TEXT_EDITOR_LINK_WINDOW,
    SET_TEXT_EDITOR_LINK_WINDOW,
    CLEAR_CREATE_SECTION,
    OPEN_ADD_MAP_PAGE,
    CLOSE_ADD_MAP_PAGE,
    START_LOADING_MAP_SCRIPT,
    FINISH_LOADING_MAP_SCRIPT,
    OPEN_REMOVE_PRODUCT_DIALOG,
    CLOSE_REMOVE_PRODUCT_DIALOG,
    OPEN_REMOVE_SECTION_DIALOG,
    CLOSE_REMOVE_SECTION_DIALOG,
    OPEN_REMOVE_MAP_COORDINATES_DIALOG,
    CLOSE_REMOVE_MAP_COORDINATES_DIALOG,
    SHOW_NOTIFICATION,
    HIDE_NOTIFICATION,
    SELECT_THEME_FORM,
    USER_GET_SUCCESS,
    USER_EDIT_SUCCESS,

    FILE_ACCEPTED,
    CROP_PHOTO,
    CLEAR_LOADED_FILE,

    OPEN_CHANGE_POSTION_PAGE,
    CLOSE_CHANGE_POSTION_PAGE,
    CHANGE_POSITION,

    SWITCH_ON_PHONECALL,
    SWITCH_OFF_PHONECALL,
    SWITCH_OFF_WHATSAPPCALL,
    SWITCH_ON_WHATSAPPCALL,

    OPEN_CONNECTION_CIRCLE,
    CLOSE_CONNECTION_CIRCLE,

    OPEN_REMOVE_CATEGORY_DIALOG,
    CLOSE_REMOVE_CATEGORY_DIALOG,

    HIDE_ADD_CATEGORY_BUTTON,

    OPEN_CATEGORY_SELECTOR,
    CLOSE_CATEGORY_SELECTOR,

    OPEN_CLIENT_ORDER_SUCCESS_ITEMS_PANEL,
    CLOSE_CLIENT_ORDER_SUCCESS_ITEMS_PANEL,
    USER_CHANGE_ACCOUNT_NAME_NOT_SUCCESS,
    OPEN_CHANGE_IMAGE_MENU,
    CLOSE_CHANGE_IMAGE_MENU,
} from '../constants/ActionTypes.js';

import * as notificaitonTypes from '../constants/NotificationTypes'

import {
    EDITING_PHOTO,
    CROPING_PHOTO,
} from '../constants/CreateProductSteps.js';

import * as mapScriptState from '../constants/MapScriptState.js';

const initialState = {
    progressFormOpen: false,
    loginedUser: {},
    menuOpened: false,
    loginCode: null,
    registerCode: null,
    viewModeEnabled: false,
    theme: {},
    loadPhotoProduct: null,
    stepCreateProduct: EDITING_PHOTO,
    srcQRCode: null,
    openTextEditor: false,
    textEditorText: '',
    textEditorFieldName: '',
    openTextEditorLinkWindow: false,
    linkTextTextEditorLinkWindow: '',
    openAddMapPage: false,
    mapSciptState: mapScriptState.NOT_LOADED,
    removeProductDialogOpened: false,
    removeSectionDialogOpened: false,
    removeMapCoordinatesDialogOpened: false,
    notificationType: null,
    notificaitonText: null,
    selectThemeForm: null,

    crop: null,
    pixelCrop: null,
    croppedImageFilePreview: null,

    acceptedFile: null,
    acceptedFileName: null,
    acceptedFilePreview: null,
    phonecallSwitchOn: false,
    whatsappcallSwitchOn: false,

    openConnectionCircle: false,
    selectedCategory: null,
    removeCategoryDialogOpened: null,

    hideAddCategoryButton: null,
    openCategorySelector: false,
    changeAccountNamgeServerError: null,
    openChangeImageMenu: false,
};

export default function comments(state = initialState, action) {
    switch (action.type) {
        case SHOW_PROGRESS:
            return {...state, progressFormOpen: true};
        case HIDE_PROGRESS:
            return {...state, progressFormOpen: false};
        case USER_LOGIN_SUCCESS:
            return {...state, loginedUser: action.user};
        case USER_LOGOUT_SUCCESS:
            return {...initialState};
        case USER_CREATE_SUCCESS:
            return {...state, loginedUser: action.user};
        case USER_LOGIN_NOT_SUCCESS:
            return {...state, loginCode: action.code};
        case USER_CREATE_NOT_SUCCESS:
            return {...state, registerCode: action.code};
        case USER_GET_AUTH_SUCCESS:
            return {...state, loginedUser: action.user};
        case OPEN_MENU:
            return {...state, menuOpened: true};
        case CLOSE_MENU:
            return {...state, menuOpened: false};
        case VIEW_MODE_ENABLED:
            return {...state, viewModeEnabled: true};
        case VIEW_MODE_DISABLED:
            return {...state, viewModeEnabled: false};
        case LOAD_PHOTO_PRODUCT:
            return {...state, loadPhotoProduct: action.photo, stepCreateProduct: CROPING_PHOTO};
        case UNLOAD_PHOTO_PRODUCT:
            return {
                ...state,
                loadPhotoProduct: null,
                stepCreateProduct: EDITING_PHOTO,
                croppedImage: null
            };
        case SET_CROP:
            return {
                ...state,
                croppedImage: {
                    ...state.croppedImage,
                    crop: {
                        x: action.crop.left,
                        y: action.crop.top,
                        width: action.crop.width,
                        height: action.crop.height,
                        aspect: 1,
                    },
                },
            };
        case SET_SRC_QR_CODE:
            return {...state, srcQRCode: action.qrcode};
        case OPEN_TEXT_EDITOR:
            return {
                ...state,
                openTextEditor: true,
                textEditorText: action.text,
                textEditorFieldName: action.fieldName
            };
        case NEXT_TEXT_EDITOR:
            return {...state, openTextEditor: false, textEditorText: action.text};
        case CLOSE_TEXT_EDITOR:
            return {...state, openTextEditor: false};
        case SET_TEXT_TEXT_EDITOR:
            return {...state, textEditorText: action.text};
        case SECTION_GET_SUCCESS: {
            const editorText =
                action.section.type === 'section' ?
                    action.section.elements.text :
                    action.section.elements.additionalInfo
            return {...state, textEditorText: editorText};
        }
        case OPEN_TEXT_EDITOR_LINK_WINDOW:
            return {...state, openTextEditorLinkWindow: true};
        case CLOSE_TEXT_EDITOR_LINK_WINDOW:
            return {...state, openTextEditorLinkWindow: false};
        case SET_TEXT_EDITOR_LINK_WINDOW:
            return {...state, linkTextTextEditorLinkWindow: action.text};
        case CLEAR_CREATE_SECTION:
            return {...state, textEditorText: ''};
        case OPEN_ADD_MAP_PAGE:
            return {...state, openAddMapPage: true};
        case CLOSE_ADD_MAP_PAGE:
            return {...state, openAddMapPage: false};
        case START_LOADING_MAP_SCRIPT:
            return {...state, mapSciptState: mapScriptState.LOADING};
        case FINISH_LOADING_MAP_SCRIPT:
            return {...state, mapSciptState: mapScriptState.LOADED};
        case OPEN_REMOVE_PRODUCT_DIALOG:
            return {...state, removeProductDialogOpened: true};
        case CLOSE_REMOVE_PRODUCT_DIALOG:
            return {...state, removeProductDialogOpened: false};
        case OPEN_REMOVE_SECTION_DIALOG:
            return {...state, removeSectionDialogOpened: true};
        case CLOSE_REMOVE_SECTION_DIALOG:
            return {...state, removeSectionDialogOpened: false};
        case OPEN_REMOVE_MAP_COORDINATES_DIALOG:
            return {...state, removeMapCoordinatesDialogOpened: true};
        case CLOSE_REMOVE_MAP_COORDINATES_DIALOG:
            return {...state, removeMapCoordinatesDialogOpened: false};
        case SHOW_NOTIFICATION:
            return {
                ...state,
                notificationType: action.notificationType,
                notificationText: action.text
            };
        case HIDE_NOTIFICATION:
            return {...state, notificationType: null, notificaitonText: null,};
        case SELECT_THEME_FORM:
            return {...state, selectThemeForm: action.theme};
        case USER_GET_SUCCESS:
        case USER_EDIT_SUCCESS:
            return {
                ...state,
                selectThemeForm: action.user.theme,
                phonecallSwitchOn: action.user.phonecallSwitchOn,
                whatsappcallSwitchOn: action.user.whatsappcallSwitchOn,

            };
        case CROP_PHOTO:
            return {
                ...state,

                crop: action.croppedImage.crop,
                pixelCrop: action.croppedImage.pixelCrop,
                croppedImageFilePreview: action.croppedImage.croppedImageFile.preview,

                acceptedFile: action.acceptedImage.acceptedFile,
                acceptedFileName: action.acceptedImage.acceptedFileName,
                acceptedFilePreview: action.acceptedImage.acceptedFilePreview,
            };
        case CLEAR_LOADED_FILE:
            return {
                ...state,

                crop: null,
                pixelCrop: null,
                croppedImageFilePreview: null,

                acceptedFile: null,
                acceptedFileName: null,
                acceptedFilePreview: null,
            };
        case SWITCH_ON_PHONECALL:
            return {
                ...state,
                phonecallSwitchOn: true
            }
        case SWITCH_OFF_PHONECALL:
            return {
                ...state,
                phonecallSwitchOn: false
            }
        case SWITCH_ON_WHATSAPPCALL:
            return {
                ...state,
                whatsappcallSwitchOn: true
            }
        case SWITCH_OFF_WHATSAPPCALL:
            return {
                ...state,
                whatsappcallSwitchOn: false
            }
        case OPEN_CONNECTION_CIRCLE:
            return {
                ...state,
                openConnectionCircle: true
            }
        case CLOSE_CONNECTION_CIRCLE:
            return {
                ...state,
                openConnectionCircle: false
            }

        case OPEN_REMOVE_CATEGORY_DIALOG:
            return {...state, removeCategoryDialogOpened: true};
        case CLOSE_REMOVE_CATEGORY_DIALOG:
            return {...state, removeCategoryDialogOpened: false};

        case HIDE_ADD_CATEGORY_BUTTON:
            return {...state, hideAddCategoryButton: true};

        case OPEN_CATEGORY_SELECTOR:
            return {...state, openCategorySelector: true};
        case CLOSE_CATEGORY_SELECTOR:
            return {...state, openCategorySelector: false};
        case OPEN_CLIENT_ORDER_SUCCESS_ITEMS_PANEL:
            return {...state, openClientOrderSuccessItemsPanel: true};
        case CLOSE_CLIENT_ORDER_SUCCESS_ITEMS_PANEL:
            return {...state, openClientOrderSuccessItemsPanel: false};
        case USER_CHANGE_ACCOUNT_NAME_NOT_SUCCESS:
            return {...state, changeAccountNamgeServerError: action.code};
        case OPEN_CHANGE_IMAGE_MENU:
            return {...state, openChangeImageMenu: true};
        case CLOSE_CHANGE_IMAGE_MENU:
            return {...state, openChangeImageMenu: false};
        default:
            return state;
    }
}
