import {
    OPEN_IMAGE_EDITOR,
    CLOSE_IMAGE_EDITOR,
    FILE_ACCEPTED,
} from '../constants/ActionTypes.js';


const initialState = {
    opened: false,
    crop: null,

    acceptedFile: null,
    acceptedFileName: null,
    acceptedFilePreview: null,
};

export default function comments(state = initialState, action) {
    switch (action.type) {
        case OPEN_IMAGE_EDITOR:
            return {
                ...state,
                opened: true,
                acceptedFile: action.file,
                acceptedFileName: action.fileName,
                acceptedFilePreview: action.filePreview,
                crop: action.crop,
            };
        case CLOSE_IMAGE_EDITOR:
            return {...initialState};
        default:
            return state;
    }
}