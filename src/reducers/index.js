import { combineReducers } from 'redux';
//import { syncHistoryWithStore, routerReducer } from 'react-router-redux'

import callbackDialog from './callbackDialog.js';
import createProductDialog from './createProductDialog.js';
import global from './global.js';
import profile from './profile.js';
import editSection from './editSection.js';
import theme from './theme.js';
import imageEditor from './imageEditor.js';
import changePosition from './changePosition.js';
import changePositionCategory from './changePositionCategory.js';
import categorySelector from './categorySelector.js';
import clientCart from './clientCart.js';
import orders from './orders.js';
import order from './order.js';
import { reducer as formReducer } from 'redux-form'

const rootReducer = combineReducers({
    global,
    callbackDialog,
    createProductDialog,
    profile,
    editSection,
    theme,
    imageEditor,
    changePosition,
    changePositionCategory,
    categorySelector,
    clientCart,
    orders,
    order,
    form: formReducer,
   // routing: routerReducer,
});

export default rootReducer;