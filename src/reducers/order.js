import {
    ORDER_GET_SUCCESS,
    ORDER_EDIT_SUCCESS,
} from '../constants/ActionTypes.js';

const initialState = {
    _id: null,
    phone: null,
    comment: null,
    userId: null,
    createDate: null,
    number: null,
    status: null,
    products: [],
};

export default function editSection(state = initialState, action) {
    switch (action.type) {
        case ORDER_GET_SUCCESS:
            return {
                ...state,
                ...action.order
            }
        case ORDER_EDIT_SUCCESS:
            return {
                ...state,
                ...action.order.updatedOrder
            }
        default:
            return state;
    }
}