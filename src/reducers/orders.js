import {
    ORDER_EDIT_SUCCESS,
    ORDERS_GET_SUCCESS,
} from '../constants/ActionTypes.js';

import {getOrder} from "../utils/data";

const initialState = {
    items: [],
    skip: null,
    limit: null,
    count: null,
};

export default function orders(state = initialState, action) {
    switch (action.type) {
        case ORDERS_GET_SUCCESS:
            return {
                items: _.concat(state.items, action.orders.items),
                skip: action.orders.skip,
                limit: action.orders.limit,
                count: action.orders.count,
            }
        case ORDER_EDIT_SUCCESS:

            const newItems = _.cloneDeep(state.items)

            const order = getOrder(newItems, action.order.updatedOrder._id)
            if(order){
                order.status = action.order.updatedOrder.status
            }

            return {
                ...state,
                items: newItems
            }
        default:
            return state;
    }
}