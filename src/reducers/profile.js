import {
    USER_GET_SUCCESS,
    USER_EDIT_SUCCESS,
    CHANGE_LANGUAGE,
    CHANGE_THEME,
    SECTION_CREATE_SUCCESS,
    SECTION_EDIT_SUCCESS,
    SECTION_REMOVE_SUCCESS,
    USER_LOGOUT_SUCCESS,
    SWITCH_OFF_HIDE_ALL_CATEGORY,
    SWITCH_ON_HIDE_ALL_CATEGORY,
    CATEGORY_CREATE_SUCCESS,
    CATEGORY_EDIT_SUCCESS,
    CATEGORY_REMOVE_SUCCESS,
    ALL_CATEGORY_START,
    ALL_CATEGORY_END,
    SELECT_CATEGORY,
    PRODUCT_EDIT_SUCCESS,
    PRODUCT_CREATE_SUCCESS,
    PRODUCT_REMOVE_SUCCESS,
    SWITCH_ON_SETTINGS_ORDERS,
    SWITCH_OFF_SETTINGS_ORDERS,
    ORDER_EDIT_SUCCESS,
    USER_CHANGE_ACCOUNT_NAME_SUCCESS,
} from '../constants/ActionTypes.js';

import {
    arrayMove,
    getSelectedCategory,
    editCategoryDb,
    changeProductEverywhere,
    changePosition,
    getOldPosition,
} from '../utils/common'

import {getProduct} from "../utils/data";
import _ from "lodash";

const initialState = {
    _id: null,
    accountName: null,
    logo: null,
    title: null,
    description: null,
    phone: null,
    email: null,
    language: 'en',
    theme: 'cloud',
    sections: [],
    categories: [],
    contactsSection: null,
    hasSeenSectionDemoPage: false,
    hasSeenContactsDemoPage: false,
    phonecallPhone: null,
    phonecallSwitchOn: false,
    whatsappcallPhone: null,
    whatsappcallText: null,
    whatsappcallSwitchOn: false,
    hideAllCategorySwitchOn: false,
    allCategoryStart: false,
    allCategoryEnd: false,
    allProducts: [],
    selectedCategory: null,
    selectedCategoryTitle: null,
    settingsOrdersSwitchOn: false,
    newOrders: null,
    categoriesProductsSwitchOn: false,
};



export default function comments(state = initialState, action) {
    switch (action.type) {
        case CHANGE_LANGUAGE:
            return { ...state, language: action.language }
        case USER_CHANGE_ACCOUNT_NAME_SUCCESS:
            return { ...state, accountName: action.user.accountName }
        case USER_GET_SUCCESS: {

            return {
                _id: action.user._id,
                accountName: action.user.accountName,
                logo: action.user.companyLogo,
                title: action.user.companyName,
                description: action.user.companyDescription,
                phone: action.user.companyPhone,
                email: action.user.email,
                language: action.user.language,
                theme: action.user.theme,
                sections: action.user.sections,
                categories: action.user.categories,
                contactsSection: action.user.contactsSection,
                allProducts: action.user.products,
                hasSeenSectionDemoPage: action.user.hasSeenSectionDemoPage,
                hasSeenContactsDemoPage: action.user.hasSeenContactsDemoPage,
                phonecallPhone: action.user.phonecallPhone,
                phonecallSwitchOn: action.user.phonecallSwitchOn,
                whatsappcallPhone: action.user.whatsappcallPhone,
                whatsappcallText: action.user.whatsappcallText,
                whatsappcallSwitchOn: action.user.whatsappcallSwitchOn,
                hideAllCategorySwitchOn: action.user.hideAllCategorySwitchOn,
                allCategoryStart: action.user.allCategoryStart,
                allCategoryEnd: action.user.allCategoryEnd,
                selectedCategory: getSelectedCategory(action.user)._id,
                selectedCategoryTitle: getSelectedCategory(action.user).title,
                settingsOrdersSwitchOn: action.user.settingsOrdersSwitchOn,
                orderFeedback: action.user.orderFeedback,
                orderEmail: action.user.orderEmail,
                newOrders: action.user.newOrders,
                categoriesProductsSwitchOn: action.user.categoriesProductsSwitchOn,
            };
        }
        case USER_EDIT_SUCCESS:
            return {
                _id: action.user._id,
                accountName: action.user.accountName,
                logo: action.user.companyLogo,
                title: action.user.companyName,
                description: action.user.companyDescription,
                phone: action.user.companyPhone,
                email: action.user.email,
                language: action.user.language,
                theme: action.user.theme,
                sections: action.user.sections,
                categories: action.user.categories,
                allProducts: action.user.products,
                contactsSection: action.user.contactsSection,
                hasSeenSectionDemoPage: action.user.hasSeenSectionDemoPage,
                hasSeenContactsDemoPage: action.user.hasSeenContactsDemoPage,
                phonecallPhone: action.user.phonecallPhone,
                phonecallSwitchOn: action.user.phonecallSwitchOn,
                whatsappcallPhone: action.user.whatsappcallPhone,
                whatsappcallText: action.user.whatsappcallText,
                whatsappcallSwitchOn: action.user.whatsappcallSwitchOn,
                hideAllCategorySwitchOn: action.user.hideAllCategorySwitchOn,
                allCategoryStart: action.user.allCategoryStart,
                allCategoryEnd: action.user.allCategoryEnd,
                selectedCategory: getSelectedCategory(action.user)._id,
                selectedCategoryTitle: getSelectedCategory(action.user).title,
                settingsOrdersSwitchOn: action.user.settingsOrdersSwitchOn,
                orderFeedback: action.user.orderFeedback,
                orderEmail: action.user.orderEmail,
                newOrders: action.user.newOrders,
                categoriesProductsSwitchOn: action.user.categoriesProductsSwitchOn,
            };
        case CHANGE_THEME:
            return { ...state, theme: action.theme }
        case SECTION_CREATE_SUCCESS:
            return { ...state, sections:
                [
                    ...state.sections, {
                        _id: action.section._id,
                        title: action.section.title,
                        type: action.section.type
                    }
                ],
                contactsSection:
                    action.section.type === 'contacts' ?
                        action.section._id : state.contactsSection,
            }
        case SECTION_EDIT_SUCCESS: {
            const newSections = [...state.sections]
            const editUserSection = newSections.find(item => item._id === action.section._id);
            editUserSection.title = action.section.title

            return {...state, sections: newSections}
        }
        case SECTION_REMOVE_SUCCESS: {
            const newSections = state.sections.filter(item => item._id !== action.section._id);

            const contactsSection =
                action.section.type === 'contacts' ?
                    null :
                    state.contactsSection

            return {...state, sections: newSections, contactsSection }
        }
        case USER_LOGOUT_SUCCESS:
            return {...initialState};
        case SWITCH_ON_HIDE_ALL_CATEGORY:
            return {
                ...state,
                hideAllCategorySwitchOn: true,
            }
        case SWITCH_OFF_HIDE_ALL_CATEGORY:
            return {
                ...state,
                hideAllCategorySwitchOn: false,
            }
        case ALL_CATEGORY_START:
            return {
                ...state,
                allCategoryStart: true,
                allCategoryEnd: false,
            }
        case ALL_CATEGORY_END:
            return {
                ...state,
                allCategoryStart: false,
                allCategoryEnd: true,
            }
        case CATEGORY_CREATE_SUCCESS: {

            const newCategories = [
                ...state.categories,
                {
                    _id: action.category._id,
                    title: action.category.title,
                    products: [],
                }
            ]

            return {...state, categories: newCategories}
        }
        case CATEGORY_EDIT_SUCCESS: {
            const newCategories = [...state.categories]

            const {
                category,
                newPosition,
            } = action.category

            const categoryId = category._id

            const editUserCategory = newCategories.find(item => item._id === categoryId);
            editUserCategory.title = category.title

            const oldPosition = getOldPosition(newCategories, categoryId)
            if(oldPosition !== newPosition){
                arrayMove(newCategories, oldPosition - 1, newPosition - 1)
            }

            // отредактируем категории в продуктах
            state.categories.forEach(categoryItem => {
                categoryItem.products.forEach(product => {
                    let foundCategory = product.categories.find(item => item._id.toString() === categoryId)
                    if(foundCategory) {
                        foundCategory.title = category.title
                    }
                })
            })

            state.allProducts.forEach(product => {
                let foundCategory = product.categories.find(item => item._id.toString() === categoryId)
                if(foundCategory) {
                    foundCategory.title = category.title
                }
            })

            return {...state, categories: newCategories}
        }
        case CATEGORY_REMOVE_SUCCESS: {
            const categoryId = action.category._id

            const newCategories = state.categories.filter(item => item._id !== categoryId);

            // почистим категории в продуктах
            state.categories.forEach(category => {
                category.products.forEach(product => {
                    product.categories = product.categories.filter(item => item._id.toString() !== categoryId)
                })
            })

            state.allProducts.forEach(product => {
                product.categories = product.categories.filter(item => item._id.toString() !== categoryId)
            })

            return {...state, categories: newCategories }
        }
        case SELECT_CATEGORY:
            return {
                ...state,
                selectedCategory: action.category._id,
                selectedCategoryTitle: action.category.title,
            }
        case PRODUCT_EDIT_SUCCESS: {

            const { editedProduct, newPositions } = action.product

            const productId = editedProduct._id
            const oldProduct = getProduct(state.allProducts, productId)

            const newCategories = _.cloneDeep(state.categories)
            const newAllProducts = _.cloneDeep(state.allProducts)

            editCategoryDb(
                editedProduct.categories,
                oldProduct.categories,
                editedProduct,
                newCategories,
                productId,
            )

            changeProductEverywhere(newCategories, editedProduct, productId, newAllProducts)

            changePosition(newCategories, newAllProducts, productId, newPositions)

            return {
                ...state,
                categories: newCategories,
                allProducts: newAllProducts
            };
        }
        case PRODUCT_REMOVE_SUCCESS: {
            const editedCategories = _.cloneDeep(state.categories)

            editedCategories.forEach(category => {
                category.products = category.products.filter(item =>
                    item._id.toString() !== action.product.productId
                )
            })

            let editedAllProducts = state.allProducts.filter(item =>
                item._id !== action.product.productId
            )

            return {
                ...state,
                allProducts: editedAllProducts,
                categories: editedCategories,
            };
        }
        case PRODUCT_CREATE_SUCCESS: {
            const { newProduct, newPositions } = action.product

            const editedCategories = _.cloneDeep(state.categories)
            const editedAllProducts = _.cloneDeep(state.allProducts)

            editedAllProducts.splice(0, 0, newProduct)

            const categories = newProduct.categories
            if(categories && categories.length > 0) {
                categories.forEach(item => {
                    const foundCategory = editedCategories.find(category => category._id === item._id)
                    foundCategory.products.splice(0, 0, newProduct)
                })
            }

            newPositions.forEach(positionItem => {
                if(positionItem._id === 'all'){
                    arrayMove(editedAllProducts, 0, positionItem.position - 1)
                }else{
                    const foundCategory = editedCategories.find(category => category._id === positionItem._id)
                    arrayMove(foundCategory.products, 0, positionItem.position - 1)
                }
            })

            return {
                ...state,
                allProducts: editedAllProducts,
                categories: editedCategories,
            };
        }
        case SWITCH_ON_SETTINGS_ORDERS:
            return {
                ...state,
                settingsOrdersSwitchOn: true,
            }
        case SWITCH_OFF_SETTINGS_ORDERS:
            return {
                ...state,
                settingsOrdersSwitchOn: false,
            }
        case ORDER_EDIT_SUCCESS:
            return {
                ...state,
                newOrders: action.order.newOrdersCount,
            }

        default:
            return state;
    }
}
