import {
    USER_GET_SUCCESS,
    USER_EDIT_SUCCESS,
    CHANGE_THEME,
} from '../constants/ActionTypes.js';

import themes from '../components/Theme/themes.json'

const initialState = {
    key: "",
    name: "",
    common_font: "",
    'common_title_text-color': "",
    'common_field_text-color': "",
    'common_active-button_background-color': "",
    'common_close-icon_text-color': "",

    'main_background_color': "",
    'main_header-title_text-color': "",
    'main_header-description_text-color': "",
    'main_header-section-button_background-color': "",
    'main_header-section-button_text-color': "",
    'main_header-section-button_text-size': "",
    'main_header-section-button_text-weight': "",
    'main_header-section-button_border-radius': "",
    'main_header-logo-first-circle_border-color': "",
    'main_header-logo-second-circle_border-color': "",

    'menu_background-color': "",
    'menu_item_text-color': "",
    'menu_divider_color': "",
    'menu_close-icon_text-color': ""
};

export default function comments(state = initialState, action) {
    switch (action.type) {
        case USER_GET_SUCCESS:
            return { ...state,  ...themes[action.user.theme], key: action.user.theme}
        case USER_EDIT_SUCCESS:
            return { ...state,  ...themes[action.user.theme], key: action.user.theme}
        case CHANGE_THEME:
            return { ...state,  ...themes[action.theme], key: action.theme}
        default:
            return state;
    }
}