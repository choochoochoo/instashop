import { applyMiddleware, createStore, combineReducers } from 'redux';
import reducers from './reducers';
import { createLogger } from 'redux-logger'
import thunk from 'redux-thunk';
import promise from 'redux-promise';
import { composeWithDevTools } from 'redux-devtools-extension';


const logger = createLogger();


// const store = createStore(
//     combineReducers({
//         ...reducers,
//         routing: routerReducer,
//     }),
//     composeWithDevTools(applyMiddleware(thunk, promise, logger))
// )


const store = createStore(
    reducers,
    composeWithDevTools(applyMiddleware(thunk, promise, logger))
);

export default store;