import i18next from "i18next";
var cookie = require('cookie');
import {getFile} from '../api';
import _ from "lodash";
var pad = require('pad-number');

export const getSectionTitle = (title, type) => {
    const newTitle = type === 'contacts' ?
        i18next.t('section_title_contacts') :
        title

    return newTitle
}

export const getLanguage = () => {
    const cookies = cookie.parse(document.cookie);
    const lng = cookies.lng ? cookies.lng : 'en'

    return 'en'
}

export const getPreview = (photoSize1080, croppedImageFilePreview) => {
    let preview = photoSize1080 ? getFile(photoSize1080) : null

    if(croppedImageFilePreview){
        preview = croppedImageFilePreview
    }

    return preview
}


export const getPreviewForm = (photoSize1080, croppedImageFilePreview) => {
    let preview = photoSize1080 ? photoSize1080 : null

    if(croppedImageFilePreview){
        preview = croppedImageFilePreview
    }

    return preview
}


export const getOriginalPreview = (photoOriginal, acceptedFilePreview) => {
    let preview = photoOriginal ? getFile(photoOriginal) : null

    if(acceptedFilePreview){
        preview = acceptedFilePreview
    }

    return preview
}

export const getCrop = (originalCrop, acceptedFileCrop) => {
    let crop = convertToUICrop(originalCrop)

    if(acceptedFileCrop){
        crop = acceptedFileCrop
    }

    return crop
}

export const convertToUICrop = (crop) => {

    if(!crop){
        return null
    }

    return {
        x: crop.left,
        y: crop.top,
        width: crop.width,
        height: crop.height,
        aspect: 1
    }
}

export const sendErrorToAnaliticsExc = (exception) => {
    gtag('event', 'exception', {
        'description': `message: ${exception.message}, stack: ${exception.stack}`,
        'fatal': false
    });
}


export const normalizeTextareaText = (text) => {

    if (!text) {
        return null
    }

    return text.replace(/\r\n/g, '<br/>').replace(/\n/g, '<br/>')
}

export const urlify = (text) => {
    if(!text){
        return null
    }

    var urlRegex = /([^\s]+((\.com)|(\.ru)|(\.org)|(\.net)|(\.io)))/g;

    return text.replace(urlRegex, function(url,b,c) {
        var url2 = (c == '.com' || c == '.ru' || c == '.org' || c == '.net' || c == '.io' )
            ?  'http://' + url : url;
        return '<a href="' +url2+ '" target="_blank">' + url + '</a>';
    })
}

export const urlifyWithoutLink = (text) => {
    if(!text){
        return null
    }

    var urlRegex = /([^\s]+((\.com)|(\.ru)|(\.org)|(\.net)|(\.io)))/g;

    return text.replace(urlRegex, function(url,b,c) {
        var url2 = (c == '.com' || c == '.ru' || c == '.org' || c == '.net' || c == '.io' )
            ?  'http://' + url : url;
        return '<a target="_blank">' + url + '</a>';
    })
}

export const arrayMove = (arr, old_index, new_index) => {
    if (new_index >= arr.length) {
        var k = new_index - arr.length + 1;
        while (k--) {
            arr.push(undefined);
        }
    }
    arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
    return arr; // for testing
};

export const clearPhone = (phone) => {
    if(!phone) return null

    return phone.replace(/\D+/g, '');
}

export const getSelectedCategory = (user) => {
    if(!user) return

    let selectedCategory = null

    if (user.hideAllCategorySwitchOn && user.allCategoryStart){
        selectedCategory = {_id: 'all' }
    }else{
        const firstCategory = user.categories[0]

        if(firstCategory){
            selectedCategory = firstCategory
        }
        else{
            selectedCategory = {_id: 'all' }
        }
    }

    return selectedCategory
}


export const editCategoryDb = (newCategories, oldCategories, editedProduct, categories, productId) => {
    const newElements = _.differenceBy(newCategories, oldCategories , '_id');
    const removeElements = _.differenceBy(oldCategories, newCategories  , '_id');

    if(newElements.length > 0 ){
        newElements.forEach(item => {
            const foundCategory = categories.find(category => category._id.toString() === item._id)

            foundCategory.products.splice(0, 0, editedProduct)
        })
    }

    if(removeElements.length > 0 ){
        removeElements.forEach(item => {
            const foundCategory = categories.find(category => category._id.toString() === item._id)

            foundCategory.products = foundCategory.products.filter(item => item._id.toString() !== productId)
        })
    }

};


export const changePosition = (newCategories, newAllProducts, productId, newPositions) => {
    let oldPosition = null

    newPositions.forEach(itemPosition => {

        if(itemPosition._id === 'all'){
            oldPosition = getOldPosition(newAllProducts, productId)
            arrayMove(newAllProducts, oldPosition - 1, itemPosition.position - 1)
        }else{
            const foundCategory = newCategories.find(category => category._id === itemPosition._id)
            if(foundCategory){
                const foundProduct = foundCategory.products.find(product => product._id === productId)
                if(foundProduct){
                    oldPosition = getOldPosition(foundCategory.products, productId)
                    arrayMove(foundCategory.products, oldPosition - 1, itemPosition.position - 1)
                }
            }
        }
    })
}

export const changeProductEverywhere = (categories, product, productId, newAllProducts) => {
    categories.forEach(category => {
        const foundProduct = category.products.find(item => item._id === productId);
        changeProduct(foundProduct, product)
    })

    const foundProduct = newAllProducts.find(item => item._id === productId);
    changeProduct(foundProduct, product)
}

export const getSerializedCategories = (productCategories) => {
    return JSON.stringify(productCategories.map(item => { return {_id: item._id, title: item.title} }))
}

export const getSerializedPositions = (positions) => {

    const positionArray = Object.keys(positions).map(key=>{
        return {_id: key, position: positions[key]}
    })

    return JSON.stringify(positionArray)
}

export const changeProduct = (foundProduct, newProduct) => {

    if(foundProduct) {
        foundProduct.photo = newProduct.photo
        foundProduct.position = newProduct.position
        foundProduct.title = newProduct.title
        foundProduct.description = newProduct.description
        foundProduct.price = newProduct.price
        foundProduct.categories = newProduct.categories
    }
}

export const getFullCategories = (fullCategories, smallCategories) => {
   return _.intersectionBy(fullCategories, smallCategories , '_id');
}

export const getOldPosition = (products, productId) => {
    let index = products.findIndex( el => el._id === productId )
    return index + 1
}
export const fullfillCategories = (categories, product) => {
    categories.forEach(category => {
        let foundProduct = category.products.find(productItem => productItem._id === product._id)
        if(!foundProduct){
            category.products.splice(0, 0, product)
        }
    })

}


export const getOrderButtonVisible = (state) => {

    // if(state.global && state.global.loginedUser && state.global.loginedUser._id === state.profile._id){
    //     return false
    // }

    if(state.profile.settingsOrdersSwitchOn){
        return true
    }

    return false
}

export const addToCart = (cart, product) => {

    const newCart = _.cloneDeep(cart)
    const newProduct = _.cloneDeep(product)

    const foundProduct = newCart.items.find(cartItem => cartItem._id === newProduct._id)
    if(foundProduct){
        foundProduct.count = foundProduct.count + 1
    }else{
        newProduct.count = 1
        newCart.items.push(newProduct)
    }

    return newCart
}

export const removeFromCart = (cart, product) => {

    const newCart = _.cloneDeep(cart);

    const foundProduct = newCart.items.find(cartItem => cartItem._id === product._id)
    if(foundProduct){
        foundProduct.count = foundProduct.count - 1

        if(foundProduct.count === 0){
            newCart.items = newCart.items.filter(item => item._id !== product._id)
        }
    }

    return newCart
}


export const editCart = (oldCart, newCart) => {

    const oldCartCopy = _.cloneDeep(oldCart);

    oldCartCopy.status = newCart.status
    oldCartCopy.phone = newCart.phone
    oldCartCopy.number = newCart.number
    oldCartCopy._id = newCart._id

    return oldCartCopy
}

export const getNumber = (number) => {

    if(!number) return

    return `#${pad(number, 3)}`
}

export const getProducts = (allProducts, categories, selectedCategory) => {
    let products = []

    if(selectedCategory === 'all'){
        products = allProducts
    }else{
        const foundCategory = categories.find(item => item._id === selectedCategory)
        if(foundCategory){
            products = foundCategory.products
        }
    }

    return products.filter(item => item !== null)
}

export const getProfileLink = (accountName) => {
    return `${location.host}/${accountName}`
}
