export const getProduct = (products, productId) => products.find(item => item._id === productId);

export const getCategory = (categories, categoryId) => categories.find(item => item._id === categoryId);

export const getOrder = (orders, orderId) => orders.find(item => item._id === orderId);