export const TARGET_WIDTH = 1080;
export const TARGET_SIZE = 200000;
const pica = require('pica/dist/pica')
import _ from 'lodash'

export const getCropInit = (image) => {
    if(!image){
        return
    }

    const width = image.naturalWidth
    const height = image.naturalHeight

    const squareSize = 80

    const maxSide = Math.max(width, height)
    const minSide = Math.min(width, height)

    const onePercent = maxSide * 1 / 100;
    const partFromMaxSide = minSide / onePercent;

    const partWidth = partFromMaxSide / 100 * squareSize
    const partHeight = 100 / 100 * squareSize

    const shiftFromMinSide = (100 - squareSize) / 2
    const shiftFromMaxSide = (100 - partWidth) / 2


    if(width > height) {
        return {
            aspect: 1 / 1,
            x: shiftFromMaxSide,
            y: shiftFromMinSide,
            width: partWidth,
            height: partHeight,
        }
    }else{
        return {
            aspect: 1 / 1,
            x: shiftFromMinSide,
            y: shiftFromMaxSide,
            width: partHeight,
            height: partWidth,
        }
    }
}

export const getImage = (url) => {
    return new Promise((resolve, reject) => {
        const image = new Image()
        image.src = url;
        image.addEventListener('load', ()=>{
            resolve(image)
        }, false);

    })
}

export const getCroppedImage = (image, pixelCrop, fileName) => {
    const canvas = document.createElement('canvas');
    canvas.width = pixelCrop.width;
    canvas.height = pixelCrop.height;
    const ctx = canvas.getContext('2d');

    ctx.drawImage(
        image,
        pixelCrop.x,
        pixelCrop.y,
        pixelCrop.width,
        pixelCrop.height,
        0,
        0,
        pixelCrop.width,
        pixelCrop.height
    );

    return new Promise((resolve, reject) => {
        canvas.toBlob(file => {
            file.name = fileName;

            const URL = window.URL;
            const url = URL.createObjectURL(file);

            resolve({
                file,
                preview: url,
            });
        }, 'image/jpeg');
    });
}

export const getTargetDimension = (image) => {
    const {
        naturalWidth,
        naturalHeight,
    }  = image;

    if(naturalWidth < TARGET_WIDTH){
        return {
            width: naturalWidth,
            height: naturalHeight,
        }
    }

    const onePercent = naturalWidth * 1 / 100;
    const targetWidthFromNaturalPercent = TARGET_WIDTH / onePercent / 100;

    return {
        width: Math.round(naturalWidth * targetWidthFromNaturalPercent),
        height: Math.round(naturalHeight * targetWidthFromNaturalPercent),
    }
}

export const resizeAndOptimize = (image, fileImage) => {
    return new Promise((resolve, reject) => {
        const offScreenCanvas = document.createElement('canvas')
        const targetDimension = getTargetDimension(image)

        offScreenCanvas.width  = targetDimension.width;
        offScreenCanvas.height = targetDimension.height;

        const pica = require('pica/dist/pica')();

        pica.resize(image, offScreenCanvas, {
            unsharpAmount: 80,
            unsharpRadius: 0.6,
            unsharpThreshold: 2
        }).then(result => {

            pica.toBlob(result, 'image/jpeg', 0.80).then((file) => {
                const URL = window.URL;
                const url = URL.createObjectURL(file);
                resolve({
                    file,
                    preview: url,
                    name: fileImage.name,
                });
            })

        });
    })
}

/**
 * Check if the given value is not a number.
 */
var isNaN = Number.isNaN || WINDOW.isNaN;

/**
 * Check if the given value is a number.
 * @param {*} value - The value to check.
 * @returns {boolean} Returns `true` if the given value is a number, else `false`.
 */
function isNumber(value) {
    return typeof value === 'number' && !isNaN(value);
}

/**
 * Check if the given value is a function.
 * @param {*} value - The value to check.
 * @returns {boolean} Returns `true` if the given value is a function, else `false`.
 */
function isFunction(value) {
    return typeof value === 'function';
}


/**
 * Iterate the given data.
 * @param {*} data - The data to iterate.
 * @param {Function} callback - The process function for each element.
 * @returns {*} The original data.
 */
function forEach(data, callback) {
    if (data && isFunction(callback)) {
        if (Array.isArray(data) || isNumber(data.length) /* array-like */) {
            var length = data.length;

            var i = void 0;

            for (i = 0; i < length; i += 1) {
                if (callback.call(data, data[i], i, data) === false) {
                    break;
                }
            }
        } else if (isObject(data)) {
            Object.keys(data).forEach(function (key) {
                callback.call(data, data[key], key, data);
            });
        }
    }

    return data;
}

var REGEXP_DATA_URL_HEAD = /^data:.*,/;

/**
 * Transform Data URL to array buffer.
 * @param {string} dataURL - The Data URL to transform.
 * @returns {ArrayBuffer} The result array buffer.
 */
export function dataURLToArrayBuffer(dataURL) {
    var base64 = dataURL.replace(REGEXP_DATA_URL_HEAD, '');
    var binary = atob(base64);
    var arrayBuffer = new ArrayBuffer(binary.length);
    var uint8 = new Uint8Array(arrayBuffer);

    forEach(uint8, function (value, i) {
        uint8[i] = binary.charCodeAt(i);
    });

    return arrayBuffer;
}

/**
 * Transform array buffer to Data URL.
 * @param {ArrayBuffer} arrayBuffer - The array buffer to transform.
 * @param {string} mimeType - The mime type of the Data URL.
 * @returns {string} The result Data URL.
 */
export function arrayBufferToDataURL(arrayBuffer, mimeType) {
    var uint8 = new Uint8Array(arrayBuffer);
    var data = '';

    // TypedArray.prototype.forEach is not supported in some browsers.
    forEach(uint8, function (value) {
        data += fromCharCode(value);
    });

    return 'data:' + mimeType + ';base64,' + btoa(data);
}

/**
 * Get orientation value from given array buffer.
 * @param {ArrayBuffer} arrayBuffer - The array buffer to read.
 * @returns {number} The read orientation value.
 */
export function getOrientation(arrayBuffer) {
    var dataView = new DataView(arrayBuffer);
    var orientation = void 0;
    var littleEndian = void 0;
    var app1Start = void 0;
    var ifdStart = void 0;

    // Only handle JPEG image (start by 0xFFD8)
    if (dataView.getUint8(0) === 0xFF && dataView.getUint8(1) === 0xD8) {
        var length = dataView.byteLength;
        var offset = 2;

        while (offset < length) {
            if (dataView.getUint8(offset) === 0xFF && dataView.getUint8(offset + 1) === 0xE1) {
                app1Start = offset;
                break;
            }

            offset += 1;
        }
    }

    if (app1Start) {
        var exifIDCode = app1Start + 4;
        var tiffOffset = app1Start + 10;

        if (getStringFromCharCode(dataView, exifIDCode, 4) === 'Exif') {
            var endianness = dataView.getUint16(tiffOffset);

            littleEndian = endianness === 0x4949;

            if (littleEndian || endianness === 0x4D4D /* bigEndian */) {
                if (dataView.getUint16(tiffOffset + 2, littleEndian) === 0x002A) {
                    var firstIFDOffset = dataView.getUint32(tiffOffset + 4, littleEndian);

                    if (firstIFDOffset >= 0x00000008) {
                        ifdStart = tiffOffset + firstIFDOffset;
                    }
                }
            }
        }
    }

    if (ifdStart) {
        var _length = dataView.getUint16(ifdStart, littleEndian);
        var _offset = void 0;
        var i = void 0;

        for (i = 0; i < _length; i += 1) {
            _offset = ifdStart + i * 12 + 2;

            if (dataView.getUint16(_offset, littleEndian) === 0x0112 /* Orientation */) {
                // 8 is the offset of the current tag's value
                _offset += 8;

                // Get the original orientation value
                orientation = dataView.getUint16(_offset, littleEndian);

                // Override the orientation with its default value
                dataView.setUint16(_offset, 1, littleEndian);
                break;
            }
        }
    }

    return orientation;
}

/**
 * Parse Exif Orientation value.
 * @param {number} orientation - The orientation to parse.
 * @returns {Object} The parsed result.
 */
export function parseOrientation(orientation) {
    var rotate = 0;
    var scaleX = 1;
    var scaleY = 1;

    switch (orientation) {
        // Flip horizontal
        // case 2:
        //     scaleX = -1;
        //     break;

        // Rotate left 180°
        case 3:
            rotate = -180;
            break;

        // Flip vertical
        // case 4:
        //     scaleY = -1;
        //     break;

        // Flip vertical and rotate right 90°
        // case 5:
        //     rotate = 90;
        //     scaleY = -1;
        //     break;

        // Rotate right 90°
        case 6:
            rotate = 90;
            break;

        // Flip horizontal and rotate right 90°
        // case 7:
        //     rotate = 90;
        //     scaleX = -1;
        //     break;

        // Rotate left 90°
        case 8:
            rotate = -90;
            break;

        default:
    }

    return {
        rotate: rotate,
        scaleX: scaleX,
        scaleY: scaleY,
    };
}


export function getRotation(arrayBuffer) {
    var  imageData = {};
    var orientation = getOrientation(arrayBuffer);
    var rotate = 0;
    var scaleX = 1;
    var scaleY = 1;

    if (orientation > 1) {
       // this.url = arrayBufferToDataURL(arrayBuffer, 'image/jpeg');

        var _parseOrientation = parseOrientation(orientation);

        rotate = _parseOrientation.rotate;
        scaleX = _parseOrientation.scaleX;
        scaleY = _parseOrientation.scaleY;
    }


    imageData.rotate = rotate;
    imageData.scaleX = scaleX;
    imageData.scaleY = scaleY;

    return imageData

}


var fromCharCode = String.fromCharCode;

/**
 * Get string from char code in data view.
 * @param {DataView} dataView - The data view for read.
 * @param {number} start - The start index.
 * @param {number} length - The read length.
 * @returns {string} The read result.
 */

function getStringFromCharCode(dataView, start, length) {
    var str = '';
    var i = void 0;

    length += start;

    for (i = start; i < length; i += 1) {
        str += fromCharCode(dataView.getUint8(i));
    }

    return str;
}

    export function getRotationData(url) {

        // XMLHttpRequest disallows to open a Data URL in some browsers like IE11 and Safari
        // if (REGEXP_DATA_URL.test(url)) {
        //     if (REGEXP_DATA_URL_JPEG.test(url)) {
        //         this.read(dataURLToArrayBuffer(url));
        //     } else {
        //         this.clone();
        //     }
        //
        //     return;
        // }

        return new Promise((resolve, reject) => {

            var xhr = new XMLHttpRequest();

            var reloading = true;


            var done = function done() {
                reloading = false;
                xhr = null;
            };

            xhr.ontimeout = done;
            xhr.onabort = done;
            xhr.onerror = function () {
                done();
            };

            xhr.onload = function (res) {
                done();

                resolve({
                    ...getRotation(res.currentTarget.response)
                });


            };

            // Bust cache when there is a "crossOrigin" property
            // if (options.checkCrossOrigin && isCrossOriginURL(url) && element.crossOrigin) {
            //     url = addTimestamp(url);
            // }

            xhr.open('get', url);
            xhr.responseType = 'arraybuffer';
            //xhr.withCredentials = element.crossOrigin === 'use-credentials';
            xhr.send();
        });
    }


export function rotate(image, rotate, fileName) {
    const canvas = document.createElement('canvas');
    const context = canvas.getContext('2d');

    if(rotate === -180){
        canvas.height = image.naturalHeight
        canvas.width = image.naturalWidth
    }else{
        canvas.height = image.naturalWidth
        canvas.width = image.naturalHeight
    }

    const cache = image

    context.save(); //saves the state of canvas
    context.clearRect(0, 0, canvas.width, canvas.height); //clear the canvas

    context.translate(canvas.width/2,canvas.height/2);
    context.rotate(Math.PI / 180 * rotate);
    context.drawImage(image,-cache.width/2,-cache.height/2);

    context.restore(); //restore the state of canvas

    return new Promise((resolve, reject) => {
        canvas.toBlob(file => {
            file.name = fileName;

            const URL = window.URL;
            const url = URL.createObjectURL(file);

            resolve({
                file,
                preview: url,
            });
        }, 'image/jpeg');
    });
}